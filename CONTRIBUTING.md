# Contributing to paas toolbox

We'd love for you to contribute to our source code and to make **pass toolbox** even better than it is today! Here are the guidelines we'd like you to follow:

- [Commit Message Guidelines](#git-commit-guidelines)

## Git Commit Guidelines

We have very precise rules over how our git commit messages can be formatted.  This leads to **more
readable messages** that are easy to follow when looking through the **project history**.

The commit message formatting have to be added using a typical git workflow or through the use of a CLI
wizard ([Commitizen](https://github.com/commitizen/cz-cli)).

To use the wizard, run `npm run commit` in your terminal after staging your changes in git.

A detailed explanation can be found in [this document](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit#).


You can process you JIRA Software issues using special command, called Smart Commits, in your commit messages.

You can:
- comment on issues : `#comment`
- record time tracking information against issues `#time`
- transition issues to any status defined in the JIRA Software porject's workflow `#close`

See documentation at [Processing JIRA Software issues with Smart Commit messages](https://confluence.atlassian.com/bitbucket/processing-jira-software-issues-with-smart-commit-messages-298979931.html)
