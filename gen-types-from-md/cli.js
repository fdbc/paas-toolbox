#!/usr/bin/env node
'use strict';

var meow = require('meow'),
    log = require('../logger/logger.js'),
    app = require('./app.js');

var cli = meow({
    help: [
        'Usage',
        '  node cli.js <path>',
        '',
        'Example',
        '  node cli.js "../../cards-v1/v0/cards/"',
        '',
    ]
});

var path = cli.input[0];

if (!path) {
    log.error('path is not specified');
    cli.showHelp();
    process.exit(1);
}

app.generate(path);
