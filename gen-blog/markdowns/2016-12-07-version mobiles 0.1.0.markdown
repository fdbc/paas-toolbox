## The new API Mobiles 0.1.0 is available.

This API includes all the needed functionalities for performing top ups over prepaid mobile phones.

### New methods
* POST /top-ups
* GET /service-providers
* GET /service-providers/{service-provider-id}
<br>

See below for bugs solved, user histories and version details.

### Feature

* [[GABI1-1080](https://globaldevtools.bbva.com/jira/browse/GABI1-1080)] - Compra de tiempo aire (recarga de móvil)

## CHANGELOG



### Bug Fixes

* **api.json:** Deleted api.json ([b2c21f7](https://bitbucket.org/apisbbva/global-apis-mobile-mobiles-v0.git/commits/b2c21f7))
* **Mobiles:** Changed annotation to required on date field. ([d9c55ce](https://bitbucket.org/apisbbva/global-apis-mobile-mobiles-v0.git/commits/d9c55ce))
* **name:** api ([fcbfdc8](https://bitbucket.org/apisbbva/global-apis-mobile-mobiles-v0.git/commits/fcbfdc8))
* **raml:** delete views ([3ee2e90](https://bitbucket.org/apisbbva/global-apis-mobile-mobiles-v0.git/commits/3ee2e90))
* **raml:** examples ([1cf2c5d](https://bitbucket.org/apisbbva/global-apis-mobile-mobiles-v0.git/commits/1cf2c5d))
* **raml:** optionals ([ce4e9fd](https://bitbucket.org/apisbbva/global-apis-mobile-mobiles-v0.git/commits/ce4e9fd))

### Features

* **mobiles:** Updated API description ([57f9af8](https://bitbucket.org/apisbbva/global-apis-mobile-mobiles-v0.git/commits/57f9af8))
* **providers:** Renamed top-up-amounts to service-providers ([180ab91](https://bitbucket.org/apisbbva/global-apis-mobile-mobiles-v0.git/commits/180ab91))
