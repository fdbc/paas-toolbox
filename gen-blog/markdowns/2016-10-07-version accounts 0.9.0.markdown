## The new version of API accounts 0.9.0 is available.

### Changes in:
* GET /accounts/{account-id}
* POST /accounts
* PATCH /accounts/{account-id}/participants

### New methods added
* GET /accounts/{account-id}/customized-formats
* GET /accounts/{account-id}/customized-formats/{customized-format-id}
<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-1070](https://globaldevtools.bbva.com/jira/browse/GABI1-1070)] - Establecer cuenta SPEI
*   [[GABI1-1253](https://globaldevtools.bbva.com/jira/browse/GABI1-1253)] - modificaiones creación de una cuenta

## CHANGELOG

### Bug Fixes

* **account:** add point ([258329b](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/258329b))
* **api:** reminderCode deleted GABI1-1070 ([58e2d80](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/58e2d80))
* **currency:** delete type currency ([4a34e11](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/4a34e11))
* **customizedFormat:** add get ([7acb039](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/7acb039))
* **example:** indicators ([10e8614](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/10e8614))
* **put:** customizedFormats ([393bd24](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/393bd24))
* **put:** customizedFormats ([7ad7883](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/7ad7883))
* **put:** customizedFormats ([7c4f5bc](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/7c4f5bc))
* **put:** http 201 created ([76ad7aa](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/76ad7aa))
* **revert:** headers ([e6ecf65](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/e6ecf65))

### Features

* **api:** GABI1-1070 create digital account ([cabf2f4](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/cabf2f4))
* **customizedFormat:**  customizedFormat type created  and  types associated to the Account updated GAB ([52906dd](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/52906dd))
