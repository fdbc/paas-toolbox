## The new version of API accounts 0.6.0 is available.

Adds a new service to create a digital account and a new service to manage 'Saldo salvo buen cobro' as a hold.

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-715](https://globaldevtools.bbva.com/jira/browse/GABI1-715)] - Alta de cuenta digital
*   [[GABI1-1109](https://globaldevtools.bbva.com/jira/browse/GABI1-1109)] - Soporte Saldo salvo buen cobro del día de hoy en Accounts

## CHANGELOG

### Bug Fixes

* **account:** method POST GABI1-503 ([f4cfbcd](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/f4cfbcd))
* **account:** method POST GABI1-503 ([f6234a7](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/f6234a7))
* **account:** method POST GABI1-503 ([6f63231](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/6f63231))
* **account:** method POST GABI1-503. ([94b0a9d](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/94b0a9d))
* **accounts:** review GABI1-503 ([09be909](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/09be909))
* **accounts:** review GABI1-503 ([e4cae98](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/e4cae98))
* **accounts:** review GABI1-566 ([21aeb3c](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/21aeb3c))
* **delete:** remove enum ([084f7b4](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/084f7b4))
* **expand:** Removed queryParams related to expandable subresources. Fixed typo in expand exa ([dd74132](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/dd74132))
* **refactor:** changes mandatories ([25f7422](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/25f7422))
* **rename:** rename facT ([5eecc0f](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/5eecc0f))
* **rename:** rename queryParam expand ([13a7a4c](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/13a7a4c))
* **revision:** add modifications of post account ([123891c](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/123891c))
* **sortable:** change name ([3b447d6](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/3b447d6))
* **sortable:** change name ([64c89f7](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/64c89f7))

### Features

* **fields:** add fiels parameter ([95cc8f2](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/95cc8f2))
* **fields:** add fiels parameter ([829b051](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/829b051))
* **merge:** Merge branch 'develop' into GABI1-556 ([5997128](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/5997128))
