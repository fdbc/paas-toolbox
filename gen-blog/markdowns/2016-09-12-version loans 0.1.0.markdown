## The new API loans 0.1.0 is available.

An API to check the customer loans, mortgages and consumer lending.

<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-801](https://globaldevtools.bbva.com/jira/browse/GABI1-801)] - Consulta de los participantes de un préstamo
*   [[GABI1-802](https://globaldevtools.bbva.com/jira/browse/GABI1-802)] - Consulta de las condiciones de un préstamo
*   [[GABI1-803](https://globaldevtools.bbva.com/jira/browse/GABI1-803)] - Modificación del alias de un préstamo
*   [[GABI1-804](https://globaldevtools.bbva.com/jira/browse/GABI1-804)] - Consulta listado de préstamos
*   [[GABI1-805](https://globaldevtools.bbva.com/jira/browse/GABI1-805)] - Consulta de indicadores de un prestamo
*   [[GABI1-806](https://globaldevtools.bbva.com/jira/browse/GABI1-806)] - Consulta de contratos relacionados de un préstamo
*   [[GABI1-807](https://globaldevtools.bbva.com/jira/browse/GABI1-807)] - Consulta detalle de un préstamo
*   [[GABI1-809](https://globaldevtools.bbva.com/jira/browse/GABI1-809)] - Consulta al plan de amortización de un préstamo
*   [[GABI1-810](https://globaldevtools.bbva.com/jira/browse/GABI1-810)] - Consulta de los importes agregados de un préstamo

## CHANGELOG

### Bug Fixes

* **change:** change orderby ([244b7f2](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/244b7f2))
* **comissions:** add new comissions ([f0d22f2](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/f0d22f2))
* **comissions:** remove interest ([ec18173](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/ec18173))
* **format:** format and delete types ([faadb5f](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/faadb5f))
* **loan:** review model ([465861c](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/465861c))
* **loans:** AMM: Migration to RAML 1.0 ([86aba56](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/86aba56))
* **loans:** AMM: Migration to RAML 1.0 ([ff26c63](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/ff26c63))
* **loans:** AMM: Migration to RAML 1.0 ([5f837ec](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/5f837ec))
* **loans:** AMM: Migration to RAML 1.0 ([3eee3dc](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/3eee3dc))
* **loans:** AMM: Migration to RAML 1.0 ([125da0b](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/125da0b))
* **loans:** Change word deposit for loan ([62443e4](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/62443e4))
* **loans:** review ([58b9282](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/58b9282))
* **merge:** merge develop ([8a8d1fc](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/8a8d1fc))
* **raml:** Change types on api.raml and descriptions ([4e3aa2d](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/4e3aa2d))
* **raml:** Changed loan-id type from collection to collection-item ([4825fcd](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/4825fcd))
* **raml:** changes due HQ of GABI1-209. RelatedContracts is now added ([fe3fdad](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/fe3fdad))
* **raml:** changes resourceTypes, added relatedContracts with alias ([b11fe83](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/b11fe83))
* **renames:** renames ([a27a7db](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/a27a7db))
* **review:** last review ([374b47a](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/374b47a))
* **revision:** api revision ([642ecf9](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/642ecf9))
* **traits:** add traits ([cea62e6](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/cea62e6))
* **version:** number version ([5cc46d6](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/5cc46d6))

### Features

* **alias:** Added method PATCH for update alias of a loan. ([7b65d9c](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/7b65d9c))
* **detail:** add participants and detail loan ([ab21ed2](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/ab21ed2))
* **loans:** 404 Error Added ([fffe295](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/fffe295))
* **loans:** Added attributes in the lastInstalment structure ([35e8177](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/35e8177))
* **loans:** Amortization created and added to the endpoint. ([b1db978](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/b1db978))
* **loans:** Amortizations pagination addded ([547031c](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/547031c))
* **loans:** Analyzed the conditions of a loan and added to the detail of a loan GABI1-817 ([bef3e8c](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/bef3e8c))
* **loans:** Balances with example added according to the GLOMO document. ([8e1a798](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/8e1a798))
* **loans:** Change optional for mandatory value of the require attribute in currentInstalmen ([70273c3](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/70273c3))
* **loans:** Comissions modified and new added. ([6d75317](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/6d75317))
* **loans:** Commissions for a loan added ([11f0c52](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/11f0c52))
* **loans:** Commissions refined ([f3c920f](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/f3c920f))
* **loans:** Example JSON for commissions added ([cd6b18f](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/cd6b18f))
* **loans:** Indent balances endpoint to root endpoint. ([a7070d6](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/a7070d6))
* **loans:** Refined and analized all the loan and loans and its types. ([325b583](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/325b583))
* **loans:** Refined detail of loan ([a2702a8](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/a2702a8))
* **loans:** Refined loans examples for consume and mortgage ([ff73d03](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/ff73d03))
* **loans:** Refining deatils in the loan and loans raml ([6198315](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/6198315))
* **loans:** Review all endpoints for loans and write new examples for each htttp method. ([f22842d](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/f22842d))
* **loans:** Types, Examples and Endpoints for manage of loans added ([7932d23](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/7932d23))
* **loans:** Unifiying loan's detail according to the review of endpoints: conditions and bal ([fb2affa](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/fb2affa))
* **refactor:** add examples related-contract ([4a3b666](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/4a3b666))
* **relatedContract:** change related contracto liked with ([78aad7a](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/78aad7a))
