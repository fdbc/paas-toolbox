## The new version of API customers 0.5.0 is available.

Adds a few services to creates, searches, gets and updates customers info.

### New methods added
* PATCH /customers/{customer-id}/economic-data

### Modified methods
* GET /customers
* POST /customers
* GET /customers/{customer-id}
* GET /customers/{customer-id}/contact-details
* GET /customers/{customer-id}/contact-details/{contact-detail-id}
* GET /customers/{customer-id}/indicators

See below for bugs solved, user histories and version details.

### Story

* [[GABI1-1293](https://globaldevtools.bbva.com/jira/browse/GABI1-1293)] - Increase or improve some attributes related to Customers
* [[GABI1-1313](https://globaldevtools.bbva.com/jira/browse/GABI1-1313)] - Actualización de datos básicos Cliente
* [[GABI1-1314](https://globaldevtools.bbva.com/jira/browse/GABI1-1314)] - Unificar /Users en /customers

## CHANGELOG

### Bug Fixes

* **customers:** new document types added ([de94a54](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/de94a54))
* **json:** examples ([f681680](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/f681680))
* **json:** examples ([b6f0893](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/b6f0893))
* **raml:** add last access date ([d81ad5c](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/d81ad5c))
