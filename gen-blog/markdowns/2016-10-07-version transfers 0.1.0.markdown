## The new version of API transfers 0.1.0 is available.

### New methods added
* GET /mobile-transfers
* POST /mobile-transfers
* GET /mobile-transfers/{mobile-transfer-id}
* DELETE /mobile-transfers/{mobile-transfer-id}
* GET /interbank-transfers
* POST /interbank-transfers
* GET /interbank-transfers/{interbank-transfer-id}
* DELETE /interbank-transfers/{interbank-transfer-id}
* GET /internal-transfers
* POST /internal-transfers
* GET /internal-transfers/{internal-transfer-id}
* DELETE /internal-transfers/{internal-transfer-id}
* GET /international-transfers
* POST /international-transfers
* GET /international-transfers/{international-transfer-id}
* DELETE /international-transfers/{international-transfer-id}
* GET /own-transfers
* POST /own-transfers
* GET /own-transfers/{own-transfer-id}
* DELETE /own-transfers/{own-transfer-id}
<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-989](https://globaldevtools.bbva.com/jira/browse/GABI1-989)] - Transferencia de cuenta a un dispositivo móvil (retirada en cajero, efectivo móvil)
*   [[GABI1-990](https://globaldevtools.bbva.com/jira/browse/GABI1-990)] - Transferencias entre productos de diferente divisa
*   [[GABI1-1031](https://globaldevtools.bbva.com/jira/browse/GABI1-1031)] - Transferencias de cuenta a cuentas propias
*   [[GABI1-1032](https://globaldevtools.bbva.com/jira/browse/GABI1-1032)] - Transferencias de cuenta a cuentas de terceros
*   [[GABI1-1033](https://globaldevtools.bbva.com/jira/browse/GABI1-1033)] - Transferencias de cuenta a cuentas interbancarias
*   [[GABI1-1034](https://globaldevtools.bbva.com/jira/browse/GABI1-1034)] - Transferencias de cuenta a cuentas internacionales
*   [[GABI1-1035](https://globaldevtools.bbva.com/jira/browse/GABI1-1035)] - Transferencia de cuenta a tarjetas propias
*   [[GABI1-1036](https://globaldevtools.bbva.com/jira/browse/GABI1-1036)] - Transferencia de cuenta a tarjetas de terceros
*   [[GABI1-1037](https://globaldevtools.bbva.com/jira/browse/GABI1-1037)] - Transferencia de cuenta a tarjetas internacionales
*   [[GABI1-1038](https://globaldevtools.bbva.com/jira/browse/GABI1-1038)] - Transferencia de tarjeta a cuentas propias
*   [[GABI1-1249](https://globaldevtools.bbva.com/jira/browse/GABI1-1249)] - Revisión API transfers

## CHANGELOG

### Bug Fixes

* **exchange:** add exchange information ([3ef2328](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/3ef2328))
* **exchange:** add exchange information ([357c425](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/357c425))
* **international:** revision of GloMo interbank transfers ([3383266](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/3383266))
* **international:** revision of GloMo international transfers ([dc2eca0](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/dc2eca0))
* **numberContract:** add number contract ([b7328a5](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/b7328a5))
* **simulated:** add behavior simulated ([44eaa96](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/44eaa96))
* **simulated:** add simulated transfers ([51a65b4](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/51a65b4))
* **tracking:** add trackings numbers ([e57c1f0](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/e57c1f0))
* **transfers:** menor fixes ([9a85123](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/9a85123))
* **transferType:** delete unnecesari transfer type ([52859d9](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/52859d9))
* **update:** mobile and own transfers ([86b5316](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/86b5316))

### Features

* **traits:** add traits ([17631e9](https://bitbucket.org/apisbbva/global-apis-paymentmethods-transfers-v0.git/commits/17631e9))
