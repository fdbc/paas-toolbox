## The new version of API cards 0.4.0 is available. 

See below for bugs solved, user histories and version details.

### Story

* [[GABI1-140](https://globaldevtools.bbva.com/jira/browse/GABI1-140)] - Información de tarjeta de crédito
- [[GABI1-210](https://globaldevtools.bbva.com/jira/browse/GABI1-210)] - Rewards (Puntos Bancomer)
+ [[GABI1-375](https://globaldevtools.bbva.com/jira/browse/GABI1-375)] - Consultar Comisión / Mantenimiento anual Cards



## CHANGELOG

### Bug Fixes

* **card:** Changes in card type and get and patch response examples due to review.
* **conditions:** Change conditions service description to a better one.
* **raml:** Changes on comments GABI1-140
* **raml:** Holding comments applied GABI1-140
* **raml:** Holding comments applied GABI1-140
* **raml:** Unused reward types removed GABI1-210




