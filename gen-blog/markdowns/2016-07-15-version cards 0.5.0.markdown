## The new version of API cards 0.5.0 is available. 

See below for bugs solved, user histories and version details.

### Story

* [[GABI1-343](https://globaldevtools.bbva.com/jira/browse/GABI1-343)] - Documentación de enumerados en Cards
- [[GABI1-544](https://globaldevtools.bbva.com/jira/browse/GABI1-544)] - Añadir el estado BLOCKED a la lista de estados disponibles de una tarjeta
+ [[GABI1-545](https://globaldevtools.bbva.com/jira/browse/GABI1-545)] - Permitir la consulta de una activación concreta

## CHANGELOG

### Bug Fixes

* Added the structure to the status and substatus cards. GABI1-485 ([857b95c](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/857b95c))
* **descriptions:** Changed descriptions in transactions ([a2e7623](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/a2e7623))
* **descriptions:** Fixed some descriptions in blocks ([f6d03ff](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/f6d03ff))
* **descriptions:** Fixed some descriptions in cards and blocks ([deb89a7](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/deb89a7))
* **values:** Added internal as one of the possible kind of blocks. ([73de2b5](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/73de2b5))

### Features

* **repositories:** renames repositories ([b553c44](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/b553c44))
* **view:** Dynamic routing ([d264534](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/d264534))       

