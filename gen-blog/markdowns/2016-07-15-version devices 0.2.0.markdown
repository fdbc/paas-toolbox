## The new version of API devices 0.2.0 is available. 

See below for bugs solved, user histories and version details.

### Story

* [[GABI1-282](https://globaldevtools.bbva.com/jira/browse/GABI1-282)] - Descripción del POST /mobile-devices como string
- [[GABI1-143](https://globaldevtools.bbva.com/jira/browse/GABI1-143)] - Registro de un nuevo dispositivo
+ [[GABI1-144](https://globaldevtools.bbva.com/jira/browse/GABI1-144)] - Borrado de un dispositivos y aplicaciones
+ [[GABI1-198](https://globaldevtools.bbva.com/jira/browse/GABI1-198)] - Actualizar información de un dispositivo
+ [[GABI1-206](https://globaldevtools.bbva.com/jira/browse/GABI1-206)] - Obtener y actualizar información de las aplicaciones de un dispositivo
+ [[GABI1-207](https://globaldevtools.bbva.com/jira/browse/GABI1-207)] - Obtener y actualizar información de los servicios de las aplicaciones de un dispositivo
+ [[GABI1-387](https://globaldevtools.bbva.com/jira/browse/GABI1-387)] - Migración nueva candidate release RAML MOBILE-DEVICES

## CHANGELOG

### Bug Fixes

* **devices:** changed POST method by PUT method ([c41bd27](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/c41bd27))
* **devices:** Changes in all the services ([19554af](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/19554af))
* **devices:** Changes to the whole API ([39ab50a](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/39ab50a))
* **example:** Renamed example file. ([3c30841](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/3c30841))
* **examples:** Changed gt-i9500 to Galaxy S6 Edge as indicated in model description. ([0173772](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/0173772))
* **mobile-devices:** change description for model and brand attributes ([de3ce45](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/de3ce45))
* **mobile-devices:** HQ GABI1-207 ([4b8fda6](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/4b8fda6))
* **mobile-devies:** HQ errors resolved ([3ce0b89](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/3ce0b89))
* **mobile-devies:** HQ GABI1-143 ([ced266c](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/ced266c))
* **notifications:** Added 404 description ([d35792b](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/d35792b))
* **raml:** Changes due to review ([c8bea28](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/c8bea28))
* **raml:** new enumDescriptions for serviceId are added ([2b7619f](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/2b7619f))
* **refactor:** devices to RC2 ([d9aeb32](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/d9aeb32))
* **types:** Changed path to commons-v0 in new data types. ([552c10c](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/552c10c))
* **verision:** change version ([140959a](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/140959a))

### Features

* **raml:** Added extended description to POST mobile-device ([f47aa61](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/f47aa61))
* **raml:** Added extended description to POST mobile-device ([adcb519](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/adcb519))
* **raml:** enumDescription changed ([a0da72a](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/a0da72a))
* **raml:** Merge branch 'GABI1-143' into GABI1-207 ([6bf30bc](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/6bf30bc))
* **repositories:** renames repositories ([a5c5576](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/a5c5576))
* **view:** Dynamic routing ([cd6eec1](https://bitbucket.org/apisbbva/global-apis-technical-devices-v0.git/commits/cd6eec1))

