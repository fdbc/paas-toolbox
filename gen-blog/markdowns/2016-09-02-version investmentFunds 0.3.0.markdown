## The new version of API investmentFunds 0.3.0 is available.

### Changes in GET /investment-funds
* alias attribute added.

### New methods added
* GET /investment-funds/{investment-fund-id}

*	PATCH /investment-funds/{investment-fund-id}

*	GET /investment-funds/{investment-fund-id}/funds

*	GET /investment-funds/{investment-fund-id}/funds/{fund-id}

*	PATCH /investment-funds/{investment-fund-id}/funds/{fund-id}

*	GET /investment-funds/{investment-fund-id}/funds/{fund-id}/conditions

*	GET /investment-funds/{investment-fund-id}/funds/{fund-id}/indicators

*	GET /investment-funds/{investment-fund-id}/limits

*	GET /investment-funds/{investment-fund-id}/participants

*	GET /investment-funds/{investment-fund-id}/related-contracts

*	POST /investment-funds/{investment-fund-id}/related-contracts

*	PATCH /investment-funds/{investment-fund-id}/related-contracts/{related-contract-id}

*	DELETE /investment-funds/{investment-fund-id}/related-contracts/{related-contract-id}

* GET /balances

<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-772](https://globaldevtools.bbva.com/jira/browse/GABI1-772)] - Consulta de los participantes de un fondo
*   [[GABI1-773](https://globaldevtools.bbva.com/jira/browse/GABI1-773)] - Consulta de las condiciones de un fondo
*   [[GABI1-774](https://globaldevtools.bbva.com/jira/browse/GABI1-774)] - Consulta de los contratos relacionados de un fondo
*   [[GABI1-776](https://globaldevtools.bbva.com/jira/browse/GABI1-776)] - Consulta listado de fondos
*   [[GABI1-777](https://globaldevtools.bbva.com/jira/browse/GABI1-777)] - Consulta detalle de un fondo
*   [[GABI1-778](https://globaldevtools.bbva.com/jira/browse/GABI1-778)] - Modificación del alias de un fondo
*   [[GABI1-779](https://globaldevtools.bbva.com/jira/browse/GABI1-779)] - Consulta de indicadores de un fondo
*   [[GABI1-780](https://globaldevtools.bbva.com/jira/browse/GABI1-780)] - Consulta de los importes agregados de un fondo
*   [[GABI1-914](https://globaldevtools.bbva.com/jira/browse/GABI1-914)] - Consulta de límites de un fondo

## CHANGELOG

### Bug Fixes

* **api:** Changes due to new fund requirements ([314e396](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/314e396))
* **character:** scape ([e8c928c](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/e8c928c))
* **funds:** Added conditions and balances structure. Changes in fund and funds (alias and cl ([b6e6ea0](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/b6e6ea0))
* **funds:** Changed baseFundCurrency attribute name to currency ([0773b02](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/0773b02))
* **investmentFunds:** Changed alias from required to optional in GET output ([88716ef](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/88716ef))
* **limits:** Removed PUT limit-id. Added more info about how limits work in fund. ([2fb8bcc](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/2fb8bcc))
* **traits:** add traits ([51174b6](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/51174b6))

### Features

* **api:** update patch fund ([4ab9dbd](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/4ab9dbd))
* **api:** update patch fund ([dc89151](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/dc89151))
* **api:** update patch fund ([f0b3bc2](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/f0b3bc2))
* **conditions:** Added a few conditions to the enum ([d76d152](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/d76d152))
* **funds:** Added patch method for changing the alias attribute. ([fa7b0a5](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/fa7b0a5))
* **funds:** Changes in fund structure ([5c7ff43](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/5c7ff43))
* **funds:** Changes in structure for funds, related contracts, limits and participants. ([9d85dc6](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/9d85dc6))
* **funds:** Examples, queryParameters and other minor changes ([b19dcc4](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/b19dcc4))
* **funds:** Refactor and cleaning tasks. ([d4671f4](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/d4671f4))
* **indicators:** Added indicators.raml ([1e3fb7b](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/1e3fb7b))
* **refactor:** add examples related-contract ([49a85d6](https://bitbucket.org/apisbbva/global-apis-products-investmentfunds-v0.git/commits/49a85d6))
