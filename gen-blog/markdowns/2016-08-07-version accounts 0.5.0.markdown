## The new version of API accounts 0.5.0 is available. 

See below for bugs solved, user histories and version details.

### Story

* [[GABI1-525](https://globaldevtools.bbva.com/jira/browse/GABI1-525)] - Comisión de membersía
- [[GABI1-547](https://globaldevtools.bbva.com/jira/browse/GABI1-547)] - Saldo salvo buen cobro
+ [[GABI1-561](https://globaldevtools.bbva.com/jira/browse/GABI1-561)] - Filtrado de movimientos por divisa
+ [[GABI1-664](https://globaldevtools.bbva.com/jira/browse/GABI1-198)] - Incluir gasto realizado a la fecha de corte
+ [[GABI1-667](https://globaldevtools.bbva.com/jira/browse/GABI1-206)] - Añadir indicadores
+ [[GABI1-669](https://globaldevtools.bbva.com/jira/browse/GABI1-669)] -  Descripción de grantedCredits incorrecta

## CHANGELOG

### Bug Fixes

* **accounts:** add expands and indicators GABI1-667 ([9e55e41](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/9e55e41))
* **accounts:** convention types ([bb7d208](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/bb7d208))
* **accounts:** delete related contact from resource ([be69f6c](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/be69f6c))
* **api:** Changes due to validation ([63640a4](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/63640a4))
* **refactor:** types ([bb73521](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/bb73521))

### Features

* **accounts:** membership fee #GABI1-525 ([50beb0d](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/50beb0d))
* **accounts:** new indicators for account ([2099a15](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/2099a15))
* **accounts:** refactor types ([28550ee](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/28550ee))
* **holds:** Added holds as an expanble subresource in accounts list ([d507b00](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/d507b00))
* **holds:** Added holds subresource to account-id ([2fc0d3e](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/2fc0d3e))
* **paymentMethod:** Added LAST_PERIOD_AMOUNT to paymentAmounts type GABI1-664 ([e402276](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/e402276))
* **raml:** new local types are added ([07d0e06](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/07d0e06))
* **versions:** CL versions ([3b5c91e](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/3b5c91e))
* **versions:** CL versions ([c589fbf](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/c589fbf))
