## The new version of API catalogs 0.1.0 is available.

An API to get global catalogs info.

See below for bugs solved, user histories and version details.

### Story

*   [[GABI1-530](https://globaldevtools.bbva.com/jira/browse/GABI1-530)] - Consulta tablas corporativas - Ocupaciones-Oficios

## CHANGELOG

### Bug Fixes

* **examples:** add example ([901ca3f](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/901ca3f))
* **merge:** merge ([7b9ab5c](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/7b9ab5c))
* **rename:** trait ([7ca502d](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/7ca502d))
* **revision:** revision pull request GABI1-530 ([a0fded5](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/a0fded5))
* **trait:** rename trait ([6643ee8](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/6643ee8))

### Features

* **all:** First version for catalogs.raml ([be6cb71](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/be6cb71))
* **all:** First version for catalogs.raml ([6a548b3](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/6a548b3))
* **all:** First version for catalogs.raml ([e2c93f0](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/e2c93f0))
* **all:** First version for catalogs.raml ([6ae61da](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/6ae61da))
