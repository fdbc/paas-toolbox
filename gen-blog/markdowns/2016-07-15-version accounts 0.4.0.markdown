## The new version of API accounts 0.4.0 is available. 

See below for bugs solved, user histories and version details.

### Story

* [[GABI1-483](https://globaldevtools.bbva.com/jira/browse/GABI1-483)] - Soporte de Bono fácil Chile
- [[GABI1-354](https://globaldevtools.bbva.com/jira/browse/GABI1-354)] - Incorporar mecanismo de filtrado en Accounts
+ [[GABI1-475](https://globaldevtools.bbva.com/jira/browse/GABI1-475)] - Añadir balances de crédito
+ [[GABI1-479](https://globaldevtools.bbva.com/jira/browse/GABI1-479)] - Nuevo formato de número de cuenta Local Internal Code (LIC) en Accounts
+ [[GABI1-493](https://globaldevtools.bbva.com/jira/browse/GABI1-493)] - Información de pago de deuda de crédito
+ [[GABI1-521](https://globaldevtools.bbva.com/jira/browse/GABI1-521)] - Nuevos indicadores de comportamiento


## CHANGELOG


### Bug Fixes

* **accounts:** added example "BONO FACIL" ([f04bc40](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/f04bc40))
* **accounts:** added lic in numertype ([aabd4fd](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/aabd4fd))
* **accounts:** descriptions changed for enumDescription in numberType.id ([23ac083](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/23ac083))
* **acounts:** PAN removed ([41b7c7b](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/41b7c7b))
* Added the new attributes  to GET /balances. GABI1-475 ([498591a](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/498591a))
* **balances:** New balances are mandatory. ([eed916f](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/eed916f))
* **indicators:** new indicators added ([239a224](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/239a224))
* **limits:** Fixed path to commons repo. ([b5dad45](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/b5dad45))
* **raml:** added expands limits ([20860d8](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/20860d8))

### Features

* **account-v0:** Credit account modifications ([0b9fc75](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/0b9fc75))
* **accounts-v0:** Bono facil (Chile) ([64714df](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/64714df))
* **accounts-v0:** countrySpecific included ([ece194e](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/ece194e))
* **accounts-v0:** countrySpecific test ([084a812](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/084a812))
* **countries:** Added countrySpecific for CLABE and CCC account number types ([810a49b](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/810a49b))
* **reminderCode:** Added Chile specification for reminderCode ([cf6db57](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/cf6db57))
* Corrected the example to /balances. GABI1-475 ([1d1b70a](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/1d1b70a))
* **versions:** Added JSON for version ([7a8b76a](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/7a8b76a))
* **view:** Dynamic routing ([8b005bc](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/8b005bc))

