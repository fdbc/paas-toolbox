## The new version of API cards 0.3.0 is available. 

See below for bugs solved, user histories and version details.

## Bug

*   [[GABI1-472](https://globaldevtools.bbva.com/jira/browse/GABI1-472)] - Filtro por estado de indicador en listado de cards

## Story

*   [[GABI1-93](https://globaldevtools.bbva.com/jira/browse/GABI1-93)] - Activaciones en el extranjero
*   [[GABI1-358](https://globaldevtools.bbva.com/jira/browse/GABI1-358)] - Incorporar mecanismo de filtrado en Cards
*   [[GABI1-435](https://globaldevtools.bbva.com/jira/browse/GABI1-435)] - Fechas de una tarjeta
*   [[GABI1-465](https://globaldevtools.bbva.com/jira/browse/GABI1-465)] - crear PATCH /cards/card-id/related-contracts/related-contract-id
*   [[GABI1-476](https://globaldevtools.bbva.com/jira/browse/GABI1-476)] - Creación endpoint participants

## CHANGELOG

### Bug Fixes

* **cards:** Added the new date "issueDate" to GET /cards/card-id. GABI1-435
* **examples:** Fixed get_200.json participants indentation
* **participants:** Created the new endpoint. GABI1-476 
* **raml:** Added a new filter of indicator
* **raml:** Added new params to cards GABI1-140 #ready-to-verify
* **raml:** Added sorting queryParams to participants. ParticipantType to mandatory in GET r 
* **raml:** Card conditions handling added GABI1-375 #ready-to-verify
* **raml:** Card rewards handling added GABI1-210 #ready-to-verify
* **raml:** Change moneyFlow description 
* **related-contract:** Added product.id in PATCH method
* **related-contracts:** Change description in header
* **transactions:** Changes due to GABI1-385

### Features

* **raml:** Added new queryParams 
* **relatedContract:** Added method PATCH



