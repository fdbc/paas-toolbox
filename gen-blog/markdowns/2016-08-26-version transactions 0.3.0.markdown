## The new version of API transactions 0.3.0 is available.

### Changes in GET /transactions
* New attribute additionalInformation: shows additional information related to this transaction.

### Changes in GET /transactions/transactions-id
* New attribute additionalInformation: shows additional information related to this transaction.

See below for bugs solved, user histories and version details.

### Feature
* [[GABI1-655](https://globaldevtools.bbva.com/jira/browse/GABI1-655)] - Consulta de Saldo y Movimientos de cuentas

## CHANGELOG

### Bug Fixes

* **character:** scape ([583a677](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/583a677))
* **enum:** add pan ([a42d91c](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/a42d91c))
* **refactor:** delete queryparams from to amount ([1fd6815](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/1fd6815))
* **rename:** rename contractID ([377c28f](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/377c28f))
* **traits:** add traits ([e7dc6e6](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/e7dc6e6))
