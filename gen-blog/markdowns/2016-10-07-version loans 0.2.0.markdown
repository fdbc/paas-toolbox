## The new version of API accounts 0.9.0 is available.

### Changes in:
* GET /loans/{loan-id}
* GET /loans/{loan-id}/related-contracts
* POST /loans/{loan-id}/related-contracts
* PATCH /loans/{loan-id}/related-contracts/{related-contract-id}

<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-1070](https://globaldevtools.bbva.com/jira/browse/GABI1-1070)] - Establecer cuenta SPEI
*   [[GABI1-1262](https://globaldevtools.bbva.com/jira/browse/GABI1-1262)] - Cambio opcionales y currencies

## CHANGELOG

### Bug Fixes

* **array:** array commercialValue ([121bd2e](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/121bd2e))
* **currency:** delete type currency ([7ab038f](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/7ab038f))
* **format:** raml ([65fb2e8](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/65fb2e8))
* **loan-id:** add new atribute ([a08f079](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/a08f079))
* **optional:** change attributes to optional ([a410b31](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/a410b31))

### Features

* **relatedContract:** added SIMPLE_ACCOUNT to enum  GABI1-1070 ([8cebe89](https://bitbucket.org/apisbbva/global-apis-products-loans-v0.git/commits/8cebe89))
