## The new version of API accounts 0.7.0 is available.

### Changes in GET /accounts
* New attribute balanceSummary: shows the expenses and incomes done for the current day by the customer.
* New filter accountId.

### Changes in GET /accounts/account-id
* New attribute balanceSummary: shows the expenses and incomes done for  the current day by the customer.

See below for bugs solved, user histories and version details.

### Feature
* [[GABI1-655](https://globaldevtools.bbva.com/jira/browse/GABI1-655)] - Consulta de Saldo y Movimientos de cuentas

## CHANGELOG

### Bug Fixes

* **descriptions:** Fixed some types ([44ede4f](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/44ede4f))
* **summary:** list ([ac11c6e](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/ac11c6e))

### Features

* **accounts:** Added accountId queryParam ([b9bf1ef](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/b9bf1ef))
