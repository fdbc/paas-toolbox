## The new version of API deposits 0.2.0 is available.

### Changes in:
* GET /deposits/{deposit-id}/related-contracts
* POST /deposits/{deposit-id}/related-contracts
* PATCH /deposits/{deposit-id}/related-contracts/{related-contract-id}
<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-1070](https://globaldevtools.bbva.com/jira/browse/GABI1-1070)] - Establecer cuenta SPEI
## CHANGELOG

### Bug Fixes

* **currency:** delete type currency ([5984338](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/5984338))
* **refactor:** error name attribute ([ff81fd4](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/ff81fd4))
