## The new version of API accounts 0.8.0 is available.

### Changes in GET /accounts/accounts-id/indicators
* Remove unnecessary indicatorId.

### Changes in GET /balances
* Add filter balances by account type.

<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-1240](https://globaldevtools.bbva.com/jira/browse/GABI1-1240)] - Eliminar indicador innecesario
*   [[GABI1-1241](https://globaldevtools.bbva.com/jira/browse/GABI1-1241)] - Agregar filtro a los balances por el tipo de cuenta

## CHANGELOG

### Bug Fixes

* **delete:** delete indicator ([6f76172](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/6f76172))
* **filter:** add balance filter ([93ca34b](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/93ca34b))
* **json:** minor changes ([17978d7](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/17978d7))
* **warnings:** remove warnings api.raml ([420d1ce](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/420d1ce))

### Features

* **raml:** added PARTICIPANT role to accounts GABI1-1154 ([d3ca2f0](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/d3ca2f0))
