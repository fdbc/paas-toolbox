## The new version of API cards 0.9.0 is available.

### New methods
* GET /cards/{card-id}/payment-methods


### Modified methods
* GET /cards
* GET /cards/{card-id}
* GET /balances
* GET /cards/{card-id}/blocks/{block-id}


<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-1325](https://globaldevtools.bbva.com/jira/browse/GABI1-1325)] - Eliminar DEBIT_CARD de los posibles valores de filtro de balances
*   [[GABI1-1272](https://globaldevtools.bbva.com/jira/browse/GABI1-1272)] - Añadir atributos a bloqueos de Tarjetas
*   [[GABI1-1330](https://globaldevtools.bbva.com/jira/browse/GABI1-1330)] - Actualización del endpoint GET cards/{card-id}
*   [[GABI1-1338](https://globaldevtools.bbva.com/jira/browse/GABI1-1338)] - Cambios de los filtros

## CHANGELOG

### Bug Fixes

* **balances:** Removed DEBIT_CARD as possible values of cardType.id queryParam ([7a2a423](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/7a2a423))
* **raml:** extract payment methods new endpoint ([4a7940d](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/4a7940d))
* **raml:** minor errors warnings ([343d106](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/343d106))

### Features

* **cards:** Modifiying block.raml for method PUT, into GABI1-1272. ([6c36977](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/6c36977))
