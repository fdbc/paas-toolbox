## The new version of API transactions 0.2.0 is available.

See below for bugs solved, user histories and version details.

### Story

* [[GABI1-563](https://globaldevtools.bbva.com/jira/browse/GABI1-563)] - Filtrado de movimientos por divisa

## CHANGELOG

### Bug Fixes

* **api:** Changes due to validation ([bedb9af](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/bedb9af))
* **api:** minor changes ([36c2c24](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/36c2c24))
