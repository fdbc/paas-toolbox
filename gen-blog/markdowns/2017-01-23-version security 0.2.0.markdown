## The new version of API Security 0.2.0 is available.

This API includes any service related to security management.

### New methods
* GET /software-tokens
* POST /software-tokens
* GET /software-tokens/{software-token-id}
* PATCH /software-tokens/{software-token-id}
* GET /software-tokens/{software-token-id}/opts/{opt-id}
* PUT /software-tokens/software-token-id/activation-data/{activation-data-id}
<br>

See below for bugs solved, user histories and version details.

### Feature

* [[GABI1-1247](https://globaldevtools.bbva.com/jira/browse/GABI1-1247)] - Análisis de los servicios necesarios para la funcionalidad de TokenSoftware

## CHANGELOG


### Bug Fixes

* **delete:** unused methods ([08919b2](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/08919b2))
* **raml:** (changes requested by session review with security team | fixes accorded with pa ([9e34909](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/9e34909))
* **raml:** (changes requested by session review with security team) for GABI1-1247 ([9a4d619](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/9a4d619))
* **raml:** (changes requested by session review with security team) for GABI1-1247 ([21b7612](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/21b7612))
* **raml:** (changes requested by session review with security team) for GABI1-1247 ([08ce32b](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/08ce32b))
* **raml:** (changes requested by session review with security team) for GABI1-1247 ([5b73584](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/5b73584))
* **raml:** (changes requested by session review with security team) for GABI1-1247 ([81b4ce8](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/81b4ce8))
* **raml:** (changes requested by session review with security team) for GABI1-1247 ([556be80](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/556be80))
* **raml:** add endpoint activationData ([b47e822](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/b47e822))
* **raml:** fixes to raml and examples post draft team review ([bd8149c](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/bd8149c))
* **traits:** add traits ([cc276cd](https://bitbucket.org/apisbbva/global-apis-security-security-v0.git/commits/cc276cd))
