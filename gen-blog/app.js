'use strict';

const fs = require('fs'),
    path = require('path'),
    _ = require('lodash'),
    marked = require('marked'),
    logger = require('../logger/logger.js');

function createRouter(markdowns) {
    let routes = "",
        header = "(function () {" +
            "'use strict';" +
            "function routeConfig($routeProvider) {" +
            "$routeProvider",
        footer = ".otherwise({" +
            "redirectTo: '/'" +
            "});" +
            "}" +
            "angular" +
            ".module('passbeta')" +
            ".config(routeConfig);" +
            "})();";
    _.forEach(markdowns, function (markdown) {
        if (markdown.search('markdown') >= 0) {
            let basicRoute = ".when('/blog/" + markdown.replace('.markdown', '') + "', {" +
                "templateUrl: 'app/blog/views/" + markdown.replace('markdown', 'html') + "'" +
                "})";
            routes = routes.concat(basicRoute);
        }
    });

    fs.writeFileSync(path.resolve(__dirname, '../gen-site/src/app/blog/blog.route.js'), header + routes + footer);
}

function createJSONPost(markdowns) {
    let jsons = {
        "posts": []
    };
    _.forEach(markdowns, function (markdown) {
        if (markdown.search('markdown') >= 0) {
            jsons.posts.push({
                "year": markdown.substring(0, markdown.indexOf('-')),
                "month": markdown.substring(markdown.indexOf('-')).substring(1, markdown.indexOf('-') - 1),
                "day": markdown.substring(markdown.indexOf('-')).substring(markdown.indexOf('-')).substring(0, markdown.indexOf('-') - 2),
                "title": markdown.substring(markdown.indexOf('-')).substring(markdown.indexOf('-')).substring(markdown.indexOf('-') - 1).replace('.markdown', '')
            })
        }
    });

    fs.writeFileSync(path.resolve(__dirname, '../gen-site/src/app/blog/post.json'), JSON.stringify(jsons, null, 4));
}

function createPosts(markdowns) {
    _.forEach(markdowns, function (markdown) {
        if (markdown.search('markdown') >= 0) {
            let markdownPath = path.resolve(__dirname, 'markdowns/', markdown),
                htmlPath = path.resolve(__dirname, '../gen-site/src/app/blog/views/', markdown.replace('markdown', 'html')),
                markdownContent = fs.readFileSync(markdownPath, 'utf8'),
                markdownToHtml = marked(markdownContent);
            fs.writeFileSync(htmlPath, markdownToHtml);
        }
    });
}

function createBlog() {
    let markdowns = fs.readdirSync(path.resolve(__dirname, 'markdowns/'));
    createPosts(markdowns);
    createRouter(markdowns)
    createJSONPost(markdowns);
}

createBlog();