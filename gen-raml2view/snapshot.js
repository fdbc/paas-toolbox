#!/usr/bin/env node
'use strict';

var parser = require('./app.js'),
    repos = require('../get-repositories/app'),
    logger = require('../logger/logger.js'),
    meow = require('meow'),
    fs = require('fs-extra');

var cli = meow({
    help: [
        'Description',
        '   RAML2VIEW client tool to generate a HTML from a RAML. It uses Jade, Angular and Material design',
        '   By default it generates the HTML in same folder as RAML, in html/api.html',
        '',
        'Usage',
        '   node snapshot.js -r <raml-repository-name> -p <raml-repository-local-path>',
        '',
        ' Arguments:',
        '   -r <raml-repository-name>: only generate HTML from <raml-repository-name> bitbucket RAML repo',
        '   -p <raml-repository-local-path>: overwrite previous repo with <raml-repository-local-path>',
        '   -d debug mode (true - false): writes json to output folder',
        '   -b <branch name>: default "develop"',
        '   -t gitRepoTags: git repo tags',
        '   -down gitRepoDown: git repo down',
        'Example',
        '   node snapshot.js',
        '   node snapshot.js -r glapi-global-apis-products-accounts',
        '   node snapshot.js -r glapi-global-apis-products-accounts -p ../../glapi-global-apis-products-accounts',
        '',
    ]
});

var repository = cli.flags.r;
var localPath = cli.flags.p;
var debug = cli.flags.d ? true : false;
var gitRepoTags = cli.flags.t;
const gitRepoDown = cli.flags.down;
const branch = cli.flags.b ? cli.flags.b : 'develop';

function obtainDomain(repo) {
    return repo.split('/')[2];
}

repos.createRepos('./raml', branch, repository).then(function(repositories) {
        if (localPath) {
          fs.copySync(localPath, './raml/' + repository)
        }
        return Promise.all(
            repositories.map(function (repo) {
                var domain = obtainDomain(repo)
                if (domain.search('commons') === -1 && domain.search('types') === -1) {
                    logger.highlight(domain);
                    fs.outputFileSync('../gen-site/src/app/api/' + domain + '/' + domain + '.jade', "ng-include(src=\"'/app/api/" + domain + "/api.html'\")");
                    return parser(repo, '../gen-site/src/app/api/' + domain + '/api.html', domain, gitRepoTags, gitRepoDown, debug);
                }
            })
        );
    }
).catch(function(err){
    console.log(err);
    process.exit(1);
});
