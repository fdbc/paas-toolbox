#!/usr/bin/env node
'use strict';

var app = require('./app.js'),
    meow = require('meow'),
    log = require('../logger/logger.js'),
    path = require('path');

    var cli = meow({
        help: [
            'Description',
            '   RAML2VIEW client tool to generate a HTML from a RAML. It uses Jade, Angular and Material design',
            '   By default it generates the HTML in same folder as RAML, in html/api.html',
            '',
            'Usage',
            '   node cli.js -r <path-to-raml-repository> -o <path_to_html_generated>',
            '',
            'Example',
            '   node cli.js -r ../../cards-v1/',
            '   node cli.js -r ../../cards-v1/ -o ../../html/cards.html',
            '',
        ]
    });

    var repositoryPath = cli.flags.r,
        outputPath = cli.flags.o,
        gitRepo = outputPath.replace(outputPath.substring(0, outputPath.indexOf('/api/')), '').replace('/api/', '').replace('/api.html', '');

    var gitRepoTags = gitRepo;
    gitRepo = "/bitbucket/projects/" + gitRepo.substring(0,5) + "/repos/" + gitRepo;
    var gitRepoDown = "/bitbucket/rest/archive/latest/projects/" + gitRepoTags.substring(0,5) + "/repos/" + gitRepoTags;


    if (!repositoryPath) {
        log.error('Path is not specified');
        cli.showHelp();
        process.exit(1);
    }

    if (outputPath) {
      app(path.resolve(repositoryPath), outputPath, gitRepo, gitRepoTags, gitRepoDown);
    } else {
      app(path.resolve(repositoryPath));
    }
