# raml2html-v2

![npm](https://img.shields.io/npm/v/raml2html-v2.svg) ![license](https://img.shields.io/npm/l/raml2html-v2.svg)

A RAML to HTML documentation generator, written for Node.js. This version use the new version of RAML Parser https://github.com/raml-org/raml-js-parser-2

![nodei.co](https://nodei.co/npm/raml2html-v2.png?downloads=true&downloadRank=true&stars=true)

## Features

Generates html from raml.

You can also generate a report with changes in html files, deleted and added repos.

For more info about scripts (npm will pass all the arguments after the -- directly to the script):

`$ npm run snapshot -- --help`

`$ npm run report -- --help`

## Install

`npm install --save raml2html-v2`


## Scripts

 - **npm run snapshot** : `node snapshot`
 - **npm run report** : `cd test && node html-compare.js`
 - **npm run readme** : `node ./node_modules/.bin/node-readme`

## Dependencies

Package | Version | Dev
--- |:---:|:---:
[colors](https://www.npmjs.com/package/colors) | ^1.1.2 | ✖
[fs-extra](https://www.npmjs.com/package/fs-extra) | ^0.26.5 | ✖
[jade](https://www.npmjs.com/package/jade) | ^1.11.0 | ✖
[lodash](https://www.npmjs.com/package/lodash) | ^4.6.1 | ✖
[meow](https://www.npmjs.com/package/meow) | ^3.3.0 | ✖
[q](https://www.npmjs.com/package/q) | ^1.4.1 | ✖
[raml-1-parser](https://www.npmjs.com/package/raml-1-parser) | 0.2.26 | ✖
[walk-sync](https://www.npmjs.com/package/walk-sync) | ^0.2.7 | ✖
[diff](https://www.npmjs.com/package/diff) | ^3.3.0 | ✔
[node-readme](https://www.npmjs.com/package/node-readme) | ^0.1.9 | ✔


## Contributing

Contributions welcome; Please submit all pull requests against the master branch. If your pull request contains JavaScript patches or features, you should include relevant unit tests. Please check the [Contributing Guidelines](contributng.md) for more details. Thanks!

## Author

Miguel María Martínez Echeverría

## License

 - **MIT** : http://opensource.org/licenses/MIT
