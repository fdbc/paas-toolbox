#!/usr/bin/env node
/* global __dirname */

'use strict';

var ramlParser = require('raml-1-parser'),
    fs = require('fs-extra'),
    jade = require('jade'),
    log = require('../logger/logger.js'),
    _ = require('lodash'),
    Q = require('q'),
    filePath = require('path'),
    walkSync = require('walk-sync');

let verbose = false;

function verboseTrace(msg) {
    if(verbose) {
        log.info(msg);
    }
}

function obtainInfo(api) {
    return _.pickBy(api, function (value, key) {
        return key.search('/') !== 0 && key !== 'get' && key !== 'post' && key !== 'put' && key !== 'patch' && key !== 'delete' && key !== 'uriParameters';
    });
}

function arrangeMethods(resource) {
    var methods = resource.methods(),
        result = [];

    _.forEach(methods, function (method) {
        result.push(_.assignIn(method.toJSON({ serializeMetadata: false }), { name: method.method() }));
    });
    return result;
}

function arrangeUriParameters(uriParameters) {
    var result = {};

    _.forEach(uriParameters, function (uriParam) {
        var name = uriParam.name();
        result[name] = {};
        if (uriParam.description()) {
            result[name].description = uriParam.description().value();
        }
        result[name].type = uriParam.type()[0];
        if (uriParam.example()) {
            result[name].example = uriParam.example().value();
        }
    });
    return result;
}

function resourcesToObject(raml) {
    var resources = raml.allResources(),
        baseUri = raml.baseUri().value(),
        result = {
            resources: []
        };
    _.forEach(resources, function (resource) {
        var auxResource = {
            uriResource: resource.absoluteUri().replace(baseUri, ''),
            methods: arrangeMethods(resource)
        };
        var uriParameters = resource.absoluteUriParameters();
        if (!_.isEmpty(uriParameters)) {
            auxResource.uriParameters = arrangeUriParameters(uriParameters);
        }
        result.resources.push(_.assignIn(obtainInfo(resource.toJSON({ serializeMetadata: false })), auxResource));
    });
    return result;
}

function processLibraries(ramlPath, api) {
    log.info('Processing libraries...');
    function processLibrary(library) {
        _.forIn(library, function (fragment, key) {
            switch (key) {
                case 'annotationTypes':
                    _.forIn(fragment, function (annotation, key) {
                        if (!api.annotationTypes) api.annotationTypes = {};
                        api.annotationTypes[key] = annotation;
                    });
                    break;
                case 'traits':
                    _.forIn(fragment, function (trait, key) {
                        if (!api.traits) api.traits = {};
                        api.traits[key] = trait;
                    });
                    break;
                case 'resourceTypes':
                    _.forIn(fragment, function (resourceType, key) {
                        if (!api.resourceTypes) api.resourceTypes = {};
                        api.resourceTypes[key] = resourceType;
                    });
                    break;
                case 'types':
                    _.forIn(fragment[0], function (dataType, key) {
                        if (!api.types) api.types = {};
                        api.types[key] = dataType;
                    });
                    break;
                case 'uses':
                    _.forIn(fragment, function (library) {
                        var libraryPath = filePath.resolve(ramlPath, './types', library.value),
                            libraryProcessed = ramlParser.loadRAMLSync(libraryPath);

                        processLibrary(libraryProcessed.toJSON({ serializeMetadata: false }));
                    });
                    break;
                default:
                    break;
            }
        });
    }
    if (api.uses) {
        _.forIn(api.uses, function (library) {
            var libraryPath = filePath.resolve(ramlPath, pathToLowerCase(library.value)),
                libraryProcessed = ramlParser.loadRAMLSync(libraryPath),
                libraryJSON = libraryProcessed.toJSON();

            processLibrary(libraryProcessed.toJSON({ serializeMetadata: false }));
        });
        delete api.uses;
    }
}

function pathToLowerCase (path){
  return path.split('/').map(folder => folder.includes('.') ? folder : folder.toLowerCase()).join('/')
}

function expandTypes(raml, path) {
    var commonsPath = filePath.resolve(__dirname, 'raml/global-apis-commons-types-v0'),
        commonsTypes = walkSync(commonsPath, { globs: ['**/*.raml'] }),
        ramlTypes = walkSync(filePath.resolve(path, 'types'), { globs: ['**/*.raml'] }),
        types = [],
        ramlJSON = raml.toJSON();

    _.forEach(commonsTypes, function (type) {
        types.push(type.replace('.raml', '').split('/')[type.split('/').length - 1]);
    })

    _.forEach(ramlTypes, function (type) {
        types.push(type.replace('.raml', '').split('/')[type.split('/').length - 1]);
    })

    return types;
}

function cleanTypes(types, typeName) {
    var resultTypes = {};

    _.forIn(types, function (type, key) {
        if(key.split('.')[0] === typeName) resultTypes[key] = _.cloneDeep(type);
    })

    return resultTypes;
}

function processResources(api, raml, path) {
    var resultArray = [],
        auxSubProp = [],
        commonsTypes = expandTypes(raml, path);

    log.info('Processing Resources...');
    api = _.assignIn(obtainInfo(api), resourcesToObject(raml));

    function fixObtainFromLibraries(type) {
        return type.substring(type.indexOf('.') + 1);
    }

    function isACommonType(commonsTypes, useLibrary) {
        return commonsTypes.indexOf(useLibrary) !== -1 ? true : false;
    }

    function isACommonType(propertyType){
      // One property have a common type when its root type is one of the commons types
      return commonsTypes.indexOf(propertyType.split('.')[0]) >= 0
    }

    function isAUnionType(propertyType){
      return /\|/.test(propertyType);
    }

    function getType(propertyType){
      return propertyType.split('.')[1].replace(' []',' [ ]').replace('[]', ' [ ]');
    }

    function getDisplayType(propertyType){
      let displayType = propertyType;
      if (isACommonType(propertyType)){
        displayType = getType(propertyType)
      }
      if (isAUnionType(propertyType)){
        displayType = ""
        propertyType.split('|').forEach( unitProperty => displayType += (getDisplayType(unitProperty.trim()) + " "))
      }
      return displayType;
    }

    function isChild(property){
      return property.parent !== "";
    }

    function haveSameType(propName, typeName){
      return propName.split('.').indexOf(typeName) >= 0;
    }

    function getDisplayName(name, propName, parent){
      // If has parent, remove parents duplicates from name
      return parent === undefined ? propName : _.uniq(name.split('.')).join('.');
    }

    function deepProperties(properties, typeName, method, io, parent, previousType) {
        _.forEach(properties, function (property, propName) {
            if (typeof (propName) !== 'number') {
                if (isACommonType(property.type[0]) && haveSameType(propName, typeName) && isChild(property)) {
                    let name = parent + '.' + propName;
                    verboseTrace("A Before name|"+name);
                    property.displayName = getDisplayName(name, propName, parent);
                    property.displayType = getDisplayType(property.type[0]);
                    verboseTrace("A After display name|"+property.displayName+"|display type|"+property.displayType);
                    if (property.annotations) {
                        var bindings = property.annotations['annotations.bindingDefinition'];
                        if (bindings) {
                            bindings.structuredValue.forEach(function (bind) {
                                if (bind.method === method.toUpperCase() && bind[io] !== 'NONE') {
                                    property.required = (bind[io] === 'REQUIRED');
                                    delete property.annotations['annotations.bindingDefinition'];
                                    delete property.properties;
                                    delete property.items;
                                    resultArray.push(property);
                                }
                            });
                        }else{
                            resultArray.push(property);
                        }
                    }else{
                        resultArray.push(property);
                    }
                    auxSubProp.push(propName);
                    if (!isAUnionType(property.type[0])) {
                      deepProperties(cleanTypes(api.types, property.type[0].split('.')[0]), property.type[0].split('.')[0], method, io, property.displayName, property.displayType);
                    } else {
                      verboseTrace("Processing union type: " + property.type[0]);
                      property.type[0].split('|').forEach(propertyTypeUnit => {
                        deepProperties(cleanTypes(api.types, propertyTypeUnit.trim().split('.')[0]), propertyTypeUnit.trim().split('.')[0], method, io, property.displayName);
                      })
                    }
                } else if (haveSameType(propName, typeName) && isChild(property)) {
                    var name;
                    if(previousType) {
                        verboseTrace("B parent with previous type|"+parent+"|propName|"+propName);

                        var parentArray = parent.split('.');
                        var propNameArray = propName.split('.');

                        if(propNameArray[0] === previousType) {
                          propNameArray.splice(0,1);
                        }

                        parentArray = parentArray.concat(propNameArray);
                        name = parentArray.join('.');

                    } else {
                        verboseTrace("B parent default |"+parent+"|propName|"+propName);
                        name = parent + '.' + propName;
                    }
                    property.displayName = getDisplayName(name, propName, parent);
                    property.displayType = getDisplayType(property.type[0]);
                    verboseTrace("B After display name|"+property.displayName+"|display type|"+property.displayType);
                    if (property.annotations) {
                        var bindings = property.annotations['annotations.bindingDefinition'];
                        if (bindings) {
                            bindings.structuredValue.forEach(function (bind) {
                                if (bind.method === method.toUpperCase() && bind[io] !== 'NONE') {
                                    property.required = (bind[io] === 'REQUIRED');
                                    delete property.annotations['annotations.bindingDefinition'];
                                    delete property.properties;
                                    delete property.items;
                                    resultArray.push(property);
                                }
                            });
                        }else{
                            resultArray.push(property);
                        }
                    }else{
                        resultArray.push(property);
                    }
                }
            }
        });
    }

    function obtainTypeFiltered(type, method, io) {
        var result = {},
            expandResult = [];
        // Only generate properties for input if property has binding definition and is not none, no binding definition is like NONE
        _.forIn(api.types, function (datatype, name) {
            if (name.split('.')[0] === type) {
                if (datatype.annotations) {
                    var bindings = datatype.annotations['annotations.bindingDefinition'];
                    if (bindings) {
                        bindings.structuredValue.forEach(function (bind) {
                            if (bind.method === method.toUpperCase() && bind[io] !== 'NONE') {
                                result[name] = _.cloneDeep(datatype);
                                result[name].required = (bind[io] === 'REQUIRED');
                                verboseTrace("C Before");
                                result[name].displayName = name;
                                verboseTrace("C After displayName|"+result[name].displayName);
                                delete result[name].annotations['annotations.bindingDefinition'];
                                delete result[name].properties;
                                delete result[name].items;
                            }
                        });
                    }
                }
            }
        });

        resultArray = [];
        auxSubProp = [];
        deepProperties(result, type, method, io);
        return resultArray;
    }

    function obtainTypeFilteredUnion(type) {
      var typeName = "";
        _.forIn(api.types, function (datatype, name) {
            if (name.split('.')[0] === type) {
              if (isAUnionType(datatype.type)){
                typeName = datatype.type;
              }
            }
        });
        return typeName;
    }

    function processRequestTypes(method) {
        if (method.body) {
            var key;
            if (method.body['application/json']) {
                key = 'application/json';
            } else if (method.body['multipart/form-data']) {
                key = 'multipart/form-data';
            } else {
                console.log("error")
            }
            delete method.body[key].structuredExample;
            const typeName = method.body[key].type[0];
            if (isAUnionType(typeName)) {

              method.body[key].properties = []
              method.body[key].type.toString().split('|').forEach(function (typeName) {
                var typeFixedName = fixObtainFromLibraries(typeName.trim());
                method.body[key].properties =  method.body[key].properties.concat(obtainTypeFiltered(typeFixedName, method.name, 'input'));
              })
            }else{
              var typeFixedName = fixObtainFromLibraries(typeName);
              method.body[key].properties = obtainTypeFiltered(typeFixedName, method.name, 'input');

              if (method.body[key].properties.length === 0){
                method.body[key].properties = proccessUnionAncestorNode(typeFixedName, method.name, 'input')
              }
            }

            if (method.body[key].example) {
                method.body[key].example = JSON.stringify(method.body[key].example, null, 4);
            } else if (method.body[key].examples) {
                for (var counter = 0; counter < method.body[key].examples.length; counter++) {
                    method.body[key].examples[counter].value = JSON.stringify(method.body[key].examples[counter].value, null, 4);
                }
            }
        }
    }

    function processResponseTypes(method) {
        var responses = method.responses;
        var typeFixedName,
            resp,
            pagination,
            body;
        _.forEach(responses, function (response, index) {
            if (responses[response.code] && responses[response.code].body && responses[response.code].body['application/json']) {
                body = responses[response.code].body['application/json'];
            }
            if (body && (response.code === '200' || response.code === '201' || response.code === '202' || response.code === '206' || response.code === '207' || response.code === '215')) {
                delete body.structuredExample;
                if (body.example) {
                    if (typeof (body.example) !== 'string') {
                        body.example = JSON.stringify(body.example, null, 4);
                    }
                } else if (body.examples) {
                    for (var counter = 0; counter < body.examples.length; counter++) {
                        if (typeof (body.examples[counter].value) !== "string") {
                            body.examples[counter].value = JSON.stringify(body.examples[counter].value, null, 4);
                        }
                    }
                }
                if (body.properties && body.properties.data) {
                    resp = body.properties.data;
                } else {
                    resp = body;
                }

                if (resp.type[0] === 'array') {
                    typeFixedName = fixObtainFromLibraries(resp.items.type[0]);
                    resp.items.properties = obtainTypeFiltered(typeFixedName, method.name, 'output');
                } else {
                    typeFixedName = fixObtainFromLibraries(resp.type[0]);
                    resp.properties = obtainTypeFiltered(typeFixedName, method.name, 'output');
                    if (resp.properties.length === 0){
                        resp.properties = proccessUnionAncestorNode(typeFixedName, method.name, 'output')
                    }
                }
                if (response.code === '206' && body.properties.pagination) {
                    pagination = body.properties.pagination;
                    typeFixedName = fixObtainFromLibraries(pagination.type[0]);
                    pagination.properties = obtainTypeFiltered(typeFixedName, method.name, 'output');
                }
                if (response.code === '200' && body.properties.pagination) {
                    pagination = body.properties.pagination;
                    typeFixedName = fixObtainFromLibraries(pagination.type[0]);
                    pagination.properties = obtainTypeFiltered(typeFixedName, method.name, 'output');
                }
            }
        });
    }

    function proccessUnionAncestorNode(typeFixedName, methodName, methodIO) {
        var propertiesNodeList = []
        var subTypeFixedName = obtainTypeFilteredUnion(typeFixedName);
        if(isAUnionType(subTypeFixedName.toString())) {

            propertiesNodeList = [{
                annotations: {},
                name: 'union',
                displayName: 'union.',
                type: subTypeFixedName,
                repeat: false,
                required: true,
                description: '',
                parent: 'union',
                displayType: getDisplayType(subTypeFixedName.toString())
            }]
            subTypeFixedName.toString().split('|').forEach(function (typeName) {
                var subTypeFixedUnionName = fixObtainFromLibraries(typeName.trim());
                var unionNodeSubType = obtainTypeFiltered(subTypeFixedUnionName, methodName, methodIO)
                unionNodeSubType.forEach(function (unionNodeSubTypeProperty) {
                    // union.. allows the expansion of the union and also remove useless child displayName items
                    unionNodeSubTypeProperty.displayName = 'union..' + unionNodeSubTypeProperty.displayName;
                })
                propertiesNodeList = propertiesNodeList.concat(unionNodeSubType);
            })
        }
        return propertiesNodeList;
    }

    function transformResource(resource) {
        resource.methods.forEach(function (method) {
            processRequestTypes(method);
            processResponseTypes(method);
        });
    }

    if (api.resources) {
        api.resources.forEach(function (resource) {
            delete resource.resources;
            transformResource(resource);
        });
    }
    return api;
}

function processDataTypes(api) {
    log.info('Processing Data types...');
    function processDataType(dataType, name) {
        api.types[name] = dataType;
        api.types[name].parent = name.substring(0, name.lastIndexOf('.'));
        if (dataType.type && dataType.type[0] === 'array' && dataType.items) {
            _.forIn(dataType.items.properties, function (dataType, propertyname) {
                processDataType(dataType, name + '.' + propertyname);
            });
        } else if (dataType.type === 'object' || dataType.properties) {
            _.forIn(dataType.properties, function (dataType, propertyname) {
                processDataType(dataType, name + '.' + propertyname);
            });
        }
    }
    if (api.types) {
        _.forIn(api.types, function (dataType, name) {
            processDataType(dataType, name);
        });
    }
}

function cleanApi(apiProcessed) {
    delete apiProcessed.traits;
    delete apiProcessed.resourceTypes;
    delete apiProcessed.annotationTypes;
    return apiProcessed;
}

function ramlToObject(path) {
    var ramlPath = filePath.resolve(path, './api.raml');
    var raml = ramlParser.loadApiSync(ramlPath);
    if (!raml) return null;
    raml = raml.expand();
    var api = raml.toJSON({ serializeMetadata: false });
    log.info('Working with ' + api.title + ' RAML');
    processLibraries(path, api);
    processDataTypes(api);
    return cleanApi(processResources(api, raml, path));
}

function renderViewSync(path, outputPath, gitRepo, gitRepoTags, gitRepoDown, debug) {
    for (var module in require.cache) {
        if (Object.prototype.hasOwnProperty.call(require.cache, module) && module.indexOf('raml-1-parser\\dist\\index.js') > -1) {
            ramlParser = require(module);
        }
    }
    if (!ramlParser) ramlParser = require('raml-1-parser');
    var deferred = Q.defer();
    var jadePath = __dirname + '/templates/apiView.jade',
        destPath;
    if (outputPath) destPath = outputPath;
    else destPath = filePath.resolve(path, './html/api.html');
    var raml = ramlToObject(path);
    if (!gitRepo){
      var aux = path.split('/')[path.split('/').length - 1];
      gitRepo = "/bitbucket/projects/" + aux.substring(0,5) + "/repos/" + aux;
    }else if(gitRepo.indexOf("/bitbucket") == -1){
      var aux = path.split('/')[path.split('/').length - 1];
      gitRepo = "/bitbucket/projects/" + aux.substring(0,5) + "/repos/" + aux;
    }
    if (!gitRepoTags) {
      gitRepoTags = path.split('/')[path.split('/').length - 1];
    }
    if (!gitRepoDown) {
      var aux = path.split('/')[path.split('/').length - 1];
      gitRepoDown = "/bitbucket/rest/archive/latest/projects/" + aux.substring(0,5) + "/repos/" + aux;
    }
    if (raml) {
        raml.gitRepo = gitRepo;
        raml.gitRepoTags = gitRepoTags;
        raml.gitRepoDown = gitRepoDown;

        if (debug) {
            log.warning('DEBUG is activated. JSON used will be written to ' + path + '/html/api.json');
            fs.outputFileSync(filePath.resolve(path, './html/api.json'), JSON.stringify(raml, null, 4));
        }
        jade.renderFile(jadePath, Object.assign({ pretty: true }, raml), function (error, data) {
            if (error) {
                log.error('An error has occurred parsing a file', error);
                deferred.reject(error);
            } else {
                fs.outputFileSync(destPath, data);
                log.info('Complete parse success, new file created in:', destPath);
                deferred.resolve();
            }
        });
    } else {
        deferred.reject('Raml path doesn\'t exits: ' + path + '/api.raml');
    }
    return deferred.promise;
}

module.exports = renderViewSync;
