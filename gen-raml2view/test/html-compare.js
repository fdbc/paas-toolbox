#!/usr/bin/env node

require('colors');
const meow = require('meow');
const jsdiff = require('diff');
const fs = require('fs');
const builder = require('junit-report-builder');

const cli = meow(
  `
    Compares generated html from raml with previously generated and pushed to each repo

    Usage
      $ html-compare <input>

    Options
      -d, --debug  Detailed output
      -f, --flatten Flatten output (only changed apis)

    Examples
      $ html-compare clapi-cl-apis-data-catalogs
      Compares only clapi-cl-apis-data-catalogs

      $ html-compare -f
      Compares all apis and shows only changed ones`,
  {
    alias: { d: 'debug', f: 'flatten', h: 'help', }
});

const bitbucketFolder = '../raml';
const generatedFolder = '../../gen-site/src/app/api/';

function getApiName(bbFolder) {
  return bbFolder.split('/')[bbFolder.split('/').length - 1];
}

function getApis(bbFolder, genFolder) {
  let apis = [];
  if (cli.input.length > 0) {
    apis = cli.input;
  } else if (fs.lstatSync(bbFolder).isDirectory()) {
    apis = fs.readdirSync(bbFolder).map(apiFolder => getApiName(apiFolder));

    if (fs.lstatSync(genFolder).isDirectory()) {
      const generatedApis = fs.readdirSync(genFolder);
      generatedApis.concat(apis.filter(api => generatedApis.includes(api)));
    }
  }
  return apis;
}

const apis = getApis(bitbucketFolder, generatedFolder);

const suite = builder.testSuite().name('APIS status');

apis.forEach((api) => {
  const htmlBitbucketApiFile = `${bitbucketFolder}/${api}/html/api.html`;
  const htmlGeneratedApiFile = `${generatedFolder}${api}/api.html`;

  if (fs.existsSync(htmlBitbucketApiFile) && fs.existsSync(htmlGeneratedApiFile)) {
    const htmlBitbucketApi = fs.readFileSync(htmlBitbucketApiFile, { encoding: 'utf8' });
    const htmlGeneratedApi = fs.readFileSync(htmlGeneratedApiFile, { encoding: 'utf8' });

    const diff = jsdiff.diffLines(htmlBitbucketApi, htmlGeneratedApi, { ignoreWhitespace: true });
    let equals = true;
    let detail = '';
    diff.forEach((part) => {
      // green for additions, red for deletions
      // grey for common parts
      let color;
      if (part.added) {
        color = 'green';
      } else if (part.removed) {
        color = 'red';
      } else {
        color = 'grey';
      }
      if (part.added || part.removed) {
        equals = false;
        detail += `${part.value[color]}`;
      }
    });
    const color = equals ? 'white' : 'yellow';
    const state = equals ? '=' : 'M';
    if (!cli.flags.flatten || !equals) {
      process.stderr.write(`${state[color]} - ${api[color]}\n`);
    }
    if (cli.flags.debug && detail !== '') {
      process.stderr.write(`${detail}\n`);
    }
    if (!equals) {
      suite.testCase().className(api).name('Compare with published api').failure(detail);
    } else {
      suite.testCase().className(api).name('Compare with published api');
    }
  } else {
    const state =
      fs.existsSync(htmlBitbucketApiFile) && !fs.existsSync(htmlGeneratedApiFile) ? '-' : '+';
    const color = state === '-' ? 'red' : 'magenta';
    process.stderr.write(`${state[color]} - ${api[color]}\n`);
  }
});

builder.writeTo('./test/test-report.xml');
