#!/usr/bin/env node
'use strict';

var genChangelog = require('./app.js'),
  meow = require('meow'),
  sys = require('util'),
  fs = require('fs'),
  path = require('path'),
  log = require('../logger/logger.js'),
  exec = require('child_process').exec;

var cli = meow({
    help: [
        'Usage',
        '  node cli.js -r <path-to-repository> -v <version>',
        '',
        'Example',
        '  node cli.js -r ../../cards-v1/ -v 0.1.0',
        '',
    ]
});

var repositoryPath = cli.flags.r,
    version = cli.flags.v;

if (!repositoryPath) {
  log.error('Path is not specified');
  cli.showHelp();
  process.exit(1);
}

if (!version) {
  log.error('Version is not specified');
  cli.showHelp();
  process.exit(1);
}
var versionRegexOutput = new RegExp(/^(\d+)\.(\d+)\.(\d+)$/).exec(version);
if (versionRegexOutput === null) {
  log.error('Invalid version format.');
  process.exit(1);
}

var config = {path: path.resolve(repositoryPath), version: version};
genChangelog(config);
