#!/usr/bin/env node
'use strict';

var conventionalChangelog = require('conventional-changelog'),
  sys = require('util'),
  fs = require('fs'),
  exec = require('child_process').exec,
  log = require('../logger/logger.js'),
  q = require('q');


function generateChangelog(options, context, path, deferred){
  var currentPath = process.cwd();
  process.chdir(path);
  var changelogStream = conventionalChangelog(options, context)
    .on('error', function(err) {
      log.error(err.toString());
      deferred.reject(err);
    });
  var outStream = fs.createWriteStream(path + '/CHANGELOG.MD');
  changelogStream.pipe(outStream).on('close', function(){
    process.chdir(currentPath);
    deferred.resolve();
  });
}
function obtainRepositoryInfo(path,version,deferred) {
  return function(error, stdout, stderr){
    if (error !== null) {
      log.error('No repository found in: ' + path);
      deferred.reject('No repository found in: ' + path);
    }
    var result = new RegExp(/@?([a-zA-Z-.0-9]+)\/([a-zA-Z-.0-9]+)\/([a-zA-Z-.0-9]+)?/g).exec(stdout);
    var context = {
      host:'https://'+result[1],
      owner:result[2],
      repository:result[3],
      version: version
    };
    var options = {preset: 'angular', releaseCount: 0};
    generateChangelog(options, context, path, deferred);
  };
}

function generate(config){
  var deferred = q.defer();
  var options = { cwd: config.path };
  exec("git config --get remote.origin.url", options, obtainRepositoryInfo(config.path, config.version, deferred));
  return deferred.promise;
}
module.exports = generate;
