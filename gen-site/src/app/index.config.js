(function() {
  'use strict';
  /** @ngInject */
  function config() {}

  angular
    .module('passbeta')
    .config(config);
})();
