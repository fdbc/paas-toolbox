(function () {
    'use strict';

    /** @ngInject */
    function IndexService($http, $q) {
        var branch = ''
        if (window.location.hostname.includes('-dot-')) {
          // When uploads new versions to google apps and doesn't deploy it, it provides an url to
          // test the new version, it has the -dot- prefix, for testing purposes it will points to
          // develop version of each api.html
          branch = "?at=refs%2Fheads%2Fdevelop"
        }
        function getHtml(apiName) {
            var deferred = $q.defer();
            $http({
                method: "GET",
                url: apiName + "/raw/html/api.html" + branch
            }).then(function (data) {
                deferred.resolve(data.data);
            });
            return deferred.promise;
        }

        function getTagHtml(apiName, tag) {
            var deferred = $q.defer();
            $http({
              method: "GET",
              url: apiName + "/raw/html/api.html?at=refs%2Ftags%2F" + tag
            }).then(function (data) {
                deferred.resolve(data.data);
            });
            return deferred.promise;
        }

        return {
            getHtml: getHtml,
            getTagHtml: getTagHtml
        }
    }

    angular
        .module('passbeta')
        .factory('IndexService', IndexService);
})();
