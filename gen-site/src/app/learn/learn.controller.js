(function() {
  'use strict';

  /** @ngInject */
  function LearnController() {}

  angular
    .module('passbeta')
    .controller('LearnController', LearnController);
})();
