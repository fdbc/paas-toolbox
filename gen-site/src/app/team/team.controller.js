(function() {
  'use strict';

  /** @ngInject */
  function TeamController() {
    var vm = this;

    vm.selectTab = function(tab) {
      var lis = $("#tabsMenu li");
      var articles = $("#Articles article");
      var size = 0;
      //Compruba que haya el mismo numero de tabs que de articulos
      if(lis.size() == articles.size()){
        size = lis.size();
      }
      //recorremos el array para ocultar todos menos el seleccionado
      for(var i = 0; i < size; i++){
        if(lis[i].getAttribute("data-show") == tab){
          lis[i].classList.add('activo');
          articles[i].style.display = 'inline';
        }else{
          lis[i].classList.remove('activo');
          articles[i].style.display = 'none';
        }
      }
    }

    vm.flipTab = function($event) {
      var id = $event.target.parentElement.id;
      var idImgs = "#" + id + " img";
      var imgs = $(idImgs);
      var idDivs = "#" + id + " div";
      var divs = $(idDivs);
      if(imgs[0].style.display !== "none") {
        imgs[0].style.display = 'none';
        divs[3].style.display = 'block';
      }else {
        imgs[0].style.display = 'inline';
        divs[3].style.display = 'none';
      }
    }
  }

  angular
    .module('passbeta')
    .controller('TeamController', TeamController);
})();
