(function() {
    'use strict';

    /** @ngInject */
    function HomeController($location, $anchorScroll, $interval, HomeService, Structure) {
        var vm = this;

        vm.index = 0;
        vm.scrollTo = function(id) {
            $location.hash(id);
            $anchorScroll();
        }

        var m = Structure;

        function initCarousel(news) {
            if ( news.length !== 0) {
                $interval(function() {
                    vm.nextNew();
                }, 5000)
            }
        }

        HomeService.getNews()
            .then(function(data) {
                vm.news = data.news;
                initCarousel(data.news)
            });

        vm.nextNew = function() {
            if (vm.index === (vm.news.length - 1)) {
                vm.index = 0;
            } else {
                ++vm.index;
            }
        }

        vm.previousNew = function() {
            if (vm.index === 0) {
                vm.index = vm.news.length - 1;
            } else {
                --vm.index;
            }
        }

        vm.selectNew = function(index) {
            vm.index = index;
        }
    }
    angular
        .module('passbeta')
        .controller('HomeController', HomeController);
})();
