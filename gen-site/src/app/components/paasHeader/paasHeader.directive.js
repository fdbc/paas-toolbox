(function () {
    'use strict';

    /** @ngInject */
    function paasHeader(HeaderService) {
        return {
            restrict: 'E',
            templateUrl: 'app/components/paasHeader/paasHeader.html',
            link: function (scope, element) {
                var apiHeader = angular.element(document.querySelector('.api-header')),
                    buttonMenu = angular.element(document.querySelector('.menu-button')),
                    menuContainer = angular.element(document.querySelector('.menu-container')),
                    listenerForMenuButton,
                    offStateChangeStart;

                function unlisten(element, event, handler) {
                    element.off(event, handler);
                    element = null;
                }

                function listen(element, event, handler) {
                    element.on(event, handler);
                    return function () {
                        unlisten(element, event, handler);
                    }
                }

                function showMenu() {
                    menuContainer.toggleClass('is-visible');
                }

                function hiddeMenu() {
                    menuContainer.removeClass('is-visible');
                }

                scope.openMenu = function ($mdOpenMenu, ev) {
                    $mdOpenMenu(ev);
                };

                scope.logOut = function () {
                    localStorage.removeItem('pth');
                    window.location.reload();
                };

                scope.$on('$routeChangeSuccess', function () {
                    if (location.hash === '#/' || location.hash === '#/#banner-info') {
                        apiHeader.removeClass('app');
                    } else {
                        apiHeader.addClass('app');
                    }
                    if (location.hash.search('#/blog') >= 0) {
                        angular.element('div[ng-view]').addClass('padding-blog');
                    } else {
                        angular.element('div[ng-view]').removeClass('paddingpadding-blog');
                    }
                });

                function onStateChangeStart() {
                    return scope.$on('$routeChangeStart', hiddeMenu);
                }

                listenerForMenuButton = listen(buttonMenu, 'click', showMenu);
                offStateChangeStart = onStateChangeStart();

                element.on('$destroy', function () {
                    listenerForMenuButton();
                    offStateChangeStart();
                });
            }
        };
    }

    angular
        .module('passbeta')
        .directive('paasHeader', paasHeader);
})();
