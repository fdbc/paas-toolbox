(function() {
  'use strict';

  /** @ngInject */
  function HeaderService($http, $q) {

    return {
      userInfo: ""
    }
  }

  angular
    .module('passbeta')
    .factory('HeaderService', HeaderService);
})();
