(function() {
  'use strict';

  /** @ngInject */
  function InvocationsController($location, $anchorScroll) {

    var vm = this;

    vm.selectTab = function(tab) {
      var lis = $("#tabsMenu li");
      var articles = $("#Articles article");
      var size = 0;
      //Compruba que haya el mismo numero de tabs que de articulos
      if(lis.size() == articles.size()){
        size = lis.size();
      }
      //recorremos el array para ocultar todos menos el seleccionado
      for(var i = 0; i < size; i++){
        if(lis[i].getAttribute("data-show") == tab){
          lis[i].classList.add('activo');
          articles[i].style.display = 'inline';
        }else{
          lis[i].classList.remove('activo');
          articles[i].style.display = 'none';
        }
      }
    }
    // Load the Visualization API and the corechart package.
     google.charts.load('current', {'packages':['corechart', 'bar']});

     // Set a callback to run when the Google Visualization API is loaded.
     google.charts.setOnLoadCallback(drawChart);

     // Callback that creates and populates a data table,
     // instantiates the pie chart, passes in the data and
     // draws it.
     function drawChart() {

       // Create the data table.
       // -----------------------------  Datos de los gráficos de Global Catalog  -----------------------------

       var dataInvocationsGlobalCatalog3 = google.visualization.arrayToDataTable([
        ["Country", "Invocations", { role: "style" } ],
        ["Spain", 311254168, "#89D1F3"],
        ["Chile", 52685351, "#009EE5"],
        ["Peru", 6905118, "#094FA4"],
        ["Peru", 5772521, "#86C82D"],
        ["Colombia", 0, "#89D1F3"]
      ]);

      var viewInvocationsGlobalCatalog3 = new google.visualization.DataView(dataInvocationsGlobalCatalog3);
      viewInvocationsGlobalCatalog3.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

       // Set chart options
       var optionsInvocationsGlobalCatalog3 = {
                      'width':600,
                      'height':450,
                      legend: {position: 'none'}};

       // Instantiate and draw our chart, passing in some options.
       var chartInvocationsGlobalCatalog3 = new google.visualization.BarChart(document.getElementById('invocationsGlobalCatalog3'));
       chartInvocationsGlobalCatalog3.draw(viewInvocationsGlobalCatalog3, optionsInvocationsGlobalCatalog3);

       var dataInvocationsSpain2 = google.visualization.arrayToDataTable([
        ["Month", "Invocations", { role: "style" } ],
        ["September", 38.6, "#89D1F3"],
        ["October", 31.1, "#009EE5"]
      ]);

      var viewInvocationsSpain2 = new google.visualization.DataView(dataInvocationsSpain2);
      viewInvocationsSpain2.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var optionsInvocationsSpain2 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        vAxis: { viewWindow:{min:0}},
        'titleTextStyle': {'fontSize': 20},
        'showColorCode': true
      };
      var chartInvocationsSpain2 = new google.visualization.ColumnChart(document.getElementById("invocationsSpain2"));
      chartInvocationsSpain2.draw(viewInvocationsSpain2, optionsInvocationsSpain2);

       var dataSpainConsumptions1 = google.visualization.arrayToDataTable([
        ["APP", "Invocations", { role: "style" } ],
        ["Internet(personal)", 154.5, "#89D1F3"],
        ["Mobile Android", 60.5, "#009EE5"],
        ["Mobile iOS", 27.7, "#094FA4"],
        ["Branches", 25.7, "#86C82D"],
        ["Internet(enterprise)", 2.5, "#009EE5"],
        ["Wallet Android", 2.4, "#094FA4"],
        ["Wallet iOS", 2, "#86C82D"]
      ]);

      var viewSpainConsumptions1 = new google.visualization.DataView(dataSpainConsumptions1);
      viewSpainConsumptions1.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var optionsSpainConsumptions1 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        'titleTextStyle': {'fontSize': 20},
        hAxis: {slantedText: true, slantedTextAngle: 45},
        chartArea: {bottom:125},
        'showColorCode': true
      };
      var chartSpainConsumptions1 = new google.visualization.ColumnChart(document.getElementById("spainConsumptions1"));
      chartSpainConsumptions1.draw(viewSpainConsumptions1, optionsSpainConsumptions1);

      var dataSpainConsumptions2 = google.visualization.arrayToDataTable([
       ["Services", "Invocations", { role: "style" } ],
       ["getSecurityToken", 45.2, "#89D1F3"],
       ["getFinancialDashboard", 34, "#009EE5"],
       ["getTransactionsByCategorization", 27.3, "#094FA4"],
       ["getCustomerProductLimits", 17.5, "#86C82D"],
       ["getTransactionsAccounts", 15.8, "#89D1F3"],
       ["getAnnouncements", 9.3, "#009EE5"],
       ["getBranchVirtualtickets", 8.6, "#094FA4"],
       ["getExternalApplications", 8.5, "#86C82D"],
       ["getCustomerSurveys", 8.3, "#89D1F3"],
       ["getCustomerManagers", 8.2, "#009EE5"]
      ]);

      var viewSpainConsumptions2 = new google.visualization.DataView(dataSpainConsumptions2);
      viewSpainConsumptions2.setColumns([0, 1,
                      { calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation" },
                      2]);

      var optionsSpainConsumptions2 = {
       width: 600,
       height: 450,
       bar: {groupWidth: "95%"},
       legend: { position: "none" },
       hAxis: {slantedText: true, slantedTextAngle: 45},
       chartArea: {bottom:150},
       'titleTextStyle': {'fontSize': 20},
       'showColorCode': true
      };
      var chartSpainConsumptions2 = new google.visualization.ColumnChart(document.getElementById("spainConsumptions2"));
      chartSpainConsumptions2.draw(viewSpainConsumptions2, optionsSpainConsumptions2);





     }
  }

  angular
    .module('passbeta')
    .controller('InvocationsController', InvocationsController);
})();
