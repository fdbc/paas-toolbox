(function () {
    'use strict';

    /** @ngInject */
    function ApiService($http, $q, $templateCache) {

        function repoTags(apiName) {
            var deferred = $q.defer();
            var domainRegex = /(\w*)-/;
            $http({
                method: "GET",
                url: "/bitbucket/rest/api/1.0/projects/" + domainRegex.exec(apiName)[1] + "/repos/" + apiName + "/tags"
            }).then(function (data) {
                deferred.resolve(data.data);
            });
            return deferred.promise;
        }

        function obtainDomainName(domain) {
            var domainWithoutVersion = domain.substring(0, domain.lastIndexOf('-')),
                domainName = domainWithoutVersion.substring(domainWithoutVersion.lastIndexOf('-') + 1, domainWithoutVersion.length)

            return domainName;
        }

        function trimRepoProperties(repository) {
            var domainRegex = /(\w*)-/,
                newRepoProperties = {
                    'display-name': obtainDomainName(repository.name),
                    'repository-url': repository.links.html.href,
                    'tag': repository.links.tags.href,
                    'country': domainRegex.exec(repository.name)[1]
                };
            return newRepoProperties;
        }

        function addedRepo(domains, domainName, groupName, repository) {
            var groupsRegex = /-(\w*)$/;
            angular.forEach(domains, function (domain) {
                if (domain.title === domainName) {
                    angular.forEach(domain.sections, function (section) {
                        if (section.name === groupsRegex.exec(groupName)[1]) {
                            section.domains.push(repository)
                        }
                    });
                }
            })
        }

        function obtainTags(tagsResponse) {
            var tags = [];

            angular.forEach(tagsResponse.values, function (tag) {
                tags.push(tag.name);
            });
            return tags;
        }

        function addedTags(structure) {
            var promises = [];

            angular.forEach(structure, function (domain) {
                angular.forEach(domain.sections, function (section) {
                    angular.forEach(section.domains, function (repo) {
                        var deferred = $q.defer();
                        $http({
                            method: "GET",
                            url: repo.tag,
                            headers: {
                                "Authorization": "Basic Ym90LWNhdGFsb2ctcmVhZDpHZHRabkVMWA=="
                            }
                        }).then(function (data) {
                            deferred.resolve(repo.tag = obtainTags(data.data));
                        })
                        promises.push(deferred.promise)
                    });
                });
            });
            $q.all(promises).then(function () {
                return structure;
            })
        }

        function createStructure(repositories) {
            var domainRegex = /(\w*)-/,
                groupsRegex = /-(\w*)$/,
                localName = /(-)(\w*)(-)/,
                structure = [],
                auxDomains = [],
                auxGroups = [],
                localGroups = [];

            angular.forEach(repositories, function (repository) {
                var domainName = repository.project.name;
                if (domainName.search('Site & Tools') === -1) domainName = domainName.search('global') !== -1 ? domainName : domainName.replace(domainRegex.exec(domainName)[1], 'local');

                if (domainName.search('Site & Tools') === -1 && auxDomains.indexOf(domainName.replace(groupsRegex.exec(domainName)[0], '')) === -1) {
                    structure.push({
                        "title": domainName.replace(groupsRegex.exec(domainName)[0], ''),
                        "sections": [{
                            "name": groupsRegex.exec(domainName)[1],
                            "icon": repository.project.links.avatar.href,
                            "domains": [trimRepoProperties(repository)]
                        }]
                    });
                    auxDomains.push(domainName.replace(groupsRegex.exec(domainName)[0], ''));
                    domainName.search('global') !== -1 ? auxGroups.push(domainName) : localGroups.push(domainName)
                } else if (domainName.search('Site & Tools') === -1 && (domainName.search('global') !== -1 ? auxGroups.indexOf(domainName) === -1 : localGroups.indexOf(domainName) === -1)) {
                    angular.forEach(structure, function (domain, index) {
                        if (domainName.replace(groupsRegex.exec(domainName)[0], '') === domain.title) {
                            structure[index].sections.push({
                                "name": groupsRegex.exec(domainName)[1],
                                "icon": repository.project.links.avatar.href,
                                "domains": [trimRepoProperties(repository)]
                            })
                        }
                    })
                    domainName.search('global') !== -1 ? auxGroups.push(domainName) : localGroups.push(domainName)
                } else if (domainName.search('Site & Tools') === -1) {
                    addedRepo(structure, domainName.replace(groupsRegex.exec(domainName)[0], ''), domainName, trimRepoProperties(repository));
                }
            });
            return structure;
        }

        function getLocation(location) {
            var version = 'v0'

            var group = ''

            var deferred = $q.defer();

            var urlAux = location.path();
            var res = urlAux.split("/");
            var version_aux = res[res.length-1];
            // comprobar si el ultimo split no es una version
            if( /^[a-z][a-z]*/.test(version_aux)==true) {
              // url sin modificar
              var url = location.path();
              // añadir grupo (products, people...)
              group = res[res.length-2];
            } else {
              // url sin la version anterior
              var url2 = urlAux.slice(0, (res[res.length-1].length)*-1);
              // añadir grupo (products, people...)
              group = res[res.length-3];
              // eliminar ultima barra
              url = url2.slice(0, url2.length-1);
            }

            var location_ = url;

            var lastWordOfPath = ""

            var stop = 0

            function putLetterIn(letter) { lastWordOfPath = lastWordOfPath + letter }

            function weHaveNotArrivedAtLastSlash() { if (stop == 0) { return true; } else { return false } }
            function weAreInSlash(index) { if (location_[index] == '/') { return true; } else { return false; } }

            function checkIfWeAreInLastSlash(index) {
                if (weAreInSlash(index) && weHaveNotArrivedAtLastSlash()) { stop = 1; }
            }

            for (var i = 0; i < location_.length; i++) {
                checkIfWeAreInLastSlash(location_.length - 1 - i)
                if (weHaveNotArrivedAtLastSlash()) {
                    putLetterIn(location_[location_.length - 1 - i])
                }
            }

            lastWordOfPath = lastWordOfPath.split('').reverse().join('')


            var notApplyFor = lastWordOfPath

            if (notApplyFor !== "api") {
              lastWordOfPath = lastWordOfPath.charAt(0).toUpperCase() + lastWordOfPath.slice(1).toLowerCase();
              $http({
                method: "POST",
                url: '/script.google.com',
                data: {'parameter':{'api':lastWordOfPath}}
              }).then(function (data) {
                  deferred.resolve(data.data);
              });

            }

            return deferred.promise
        }

        return {
            getLocation: getLocation,
            repoTags: repoTags
        }
    }

    angular
        .module('passbeta')
        .factory('ApiService', ApiService);
})();
