(function() {
  'use strict';

  /** @ngInject */
  function BlogController($scope, BlogService) {
    var vm = this

    BlogService.getPosts()
      .then(function(data) {
        vm.posts = data.posts.reverse();
      });
  }

  angular
    .module('passbeta')
    .controller('BlogController', BlogController);
})();
