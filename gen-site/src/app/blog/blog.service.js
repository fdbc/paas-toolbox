(function() {
  'use strict';

  /** @ngInject */
  function BlogService($http, $q) {
    function getPosts() {
      var deferred = $q.defer();

        $http({
            method: "GET",
            url: '/bitbucket/projects/BPC/repos/paas-toolbox/raw/gen-site/src/app/blog/post.json'
        }).then(function(data) {
            deferred.resolve(data.data);
        });
        return deferred.promise;
    }

    return {
      getPosts: getPosts
    }
  }

  angular
    .module('passbeta')
    .factory('BlogService', BlogService);
})();
