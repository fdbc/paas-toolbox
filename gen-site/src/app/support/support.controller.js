(function() {
  'use strict';

  /** @ngInject */
  function SupportController($location, $anchorScroll) {
    var vm = this;

    vm.scrollTo = function(id) {
      $location.hash(id);
      $anchorScroll();
    }
  }

  angular
    .module('passbeta')
    .controller('SupportController', SupportController);
})();
