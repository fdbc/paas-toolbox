(function() {
  'use strict';

  /** @ngInject */
  function DashboardController($location, $anchorScroll) {

    var vm = this;

    vm.selectTab = function(tab) {
      var lis = $("#tabsMenu li");
      var articles = $("#Articles article");
      var size = 0;
      //Compruba que haya el mismo numero de tabs que de articulos
      if(lis.size() == articles.size()){
        size = lis.size();
      }
      //recorremos el array para ocultar todos menos el seleccionado
      for(var i = 0; i < size; i++){
        if(lis[i].getAttribute("data-show") == tab){
          lis[i].classList.add('activo');
          articles[i].style.display = 'inline';
          switch (tab) {
            case "1":
              drawChartMexico();
              break;
            case "2":
              drawChartColombia();
              break;
            case "3":
              drawChartPeru();
              break;
            case "4":
              drawChartChile();
              break;
            case "5":
              drawChartArgentina();
              break;
            case "6":
              drawChartSpain();
              break;
            case "7":
              drawChartUSA();
              break;
            case "8":
              drawChartVenezuela();
              break;
            case "9":
              drawChartUruguay();
              break;
            case "10":
              drawChartParaguay();
              break;
            case "11":
              drawChartTurkey();
              break;
            default:
          }
        }else{
          lis[i].classList.remove('activo');
          articles[i].style.display = 'none';
        }
      }
    }
    // Load the Visualization API and the corechart package.
     google.charts.load('current', {'packages':['corechart', 'bar']});

     // Set a callback to run when the Google Visualization API is loaded.
     google.charts.setOnLoadCallback(drawChart);

     // Callback that creates and populates a data table,
     // instantiates the pie chart, passes in the data and
     // draws it.
     function drawChart() {

       // Create the data table.
  // -----------------------------  Datos de los gráficos de Global Catalog  -----------------------------

       var dataGlobalCatalog1 = google.visualization.arrayToDataTable([
        ["Element", "Services", { role: "style" } ],
        ["Accounts", 43, "#89D1F3"],
        ["Loans", 42, "#009EE5"],
        ["Cards", 41, "#094FA4"],
        ["Customers", 36, "#86C82D"],
        ["PFM", 34, "#89D1F3"],
        ["ContactsBook", 22, "#009EE5"],
        ["Transfers", 22, "#094FA4"],
        ["Businesses", 21, "#86C82D"],
        ["Payments", 19, "#89D1F3"],
        ["Deposits", 16, "009EE5"],
        ["Others", 109, "#094FA4"]
      ]);

      var viewGlobalCatalog1 = new google.visualization.DataView(dataGlobalCatalog1);
      viewGlobalCatalog1.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

       // Set chart options
       var optionsGlobalCatalog1 = {
                      'width':600,
                      'height':450,
                      legend: {position: 'none'}
                    };

       // Instantiate and draw our chart, passing in some options.
       var chartGlobalCatalog1 = new google.visualization.BarChart(document.getElementById('globalCatalog1'));
       chartGlobalCatalog1.draw(viewGlobalCatalog1, optionsGlobalCatalog1);

       var dataGlobalCatalog2 = google.visualization.arrayToDataTable([
        ["Country", "#Servicios", { role: "style" } ],
        ["Global API Catalog", 405, "#89D1F3"],
        ["Mexico", 81, "#89D1F3"],
        ["Spain", 12, "#86C82D"],
        ["Colombia", 127, "#009EE5"],
        ["Peru", 48, "#094FA4"],
        ["Chile", 73, "#86C82D"],
        ["USA", 51, "#094FA4"],
        ["Venezuela", 14, "#86C82D"],
        ["Paraguay", 0, "#009EE5"],
        ["Uruguay", 0, "#009EE5"],
        ["Argentina", 0, "#009EE5"],
        ["Turkey", 0, "#89D1F3"]
      ]);

      var viewGlobalCatalog2 = new google.visualization.DataView(dataGlobalCatalog2);
      viewGlobalCatalog2.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var optionsGlobalCatalog2 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        'titleTextStyle': {'fontSize': 20},
        'showColorCode': true,
      };
      var chartGlobalCatalog2 = new google.visualization.BarChart(document.getElementById("globalCatalog2"));
      chartGlobalCatalog2.draw(viewGlobalCatalog2, optionsGlobalCatalog2);

      var dataGlobalCatalog3 = google.visualization.arrayToDataTable([
        ["Country", "#Servicios", { role: "style" } ],
        ["Global API Catalog", 405, "#89D1F3"],
        ["Mexico", 2, "#89D1F3"],
        ["Spain", 8, "#86C82D"],
        ["Colombia", 19, "#009EE5"],
        ["Peru", 21, "#094FA4"],
        ["Chile", 0, "#86C82D"],
        ["USA", 0, "#094FA4"],
        ["Venezuela", 0, "#86C82D"],
        ["Paraguay", 0, "#009EE5"],
        ["Uruguay", 0, "#009EE5"],
        ["Argentina", 0, "#009EE5"],
        ["Turkey", 0, "#89D1F3"]  
      ]);

      var viewGlobalCatalog3 = new google.visualization.DataView(dataGlobalCatalog3);
      viewGlobalCatalog3.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var optionsGlobalCatalog3 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        'titleTextStyle': {'fontSize': 20},
        'showColorCode': true
      };
      var chartGlobalCatalog3 = new google.visualization.BarChart(document.getElementById("globalCatalog3"));
      chartGlobalCatalog3.draw(viewGlobalCatalog3, optionsGlobalCatalog3);

      var dataGlobalCatalog4 = google.visualization.arrayToDataTable([
        ["Country", "#Servicios", { role: "style" } ],
        ["Global API Catalog", 405, "#89D1F3"],
        ["Mexico", 0, "#89D1F3"],
        ["Spain", 0, "#86C82D"],
        ["Colombia", 0, "#009EE5"],
        ["Peru", 0, "#094FA4"],
        ["Chile", 0, "#86C82D"],
        ["USA", 0, "#094FA4"],
        ["Venezuela", 0, "#86C82D"],
        ["Paraguay", 0, "#009EE5"],
        ["Uruguay", 0, "#009EE5"],
        ["Argentina", 0, "#009EE5"],
        ["Turkey", 0, "#89D1F3"]  
      ]);

      var viewGlobalCatalog4 = new google.visualization.DataView(dataGlobalCatalog4);
      viewGlobalCatalog4.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var optionsGlobalCatalog4 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        'titleTextStyle': {'fontSize': 20},
        'showColorCode': true
      };
      var chartGlobalCatalog4 = new google.visualization.BarChart(document.getElementById("globalCatalog4"));
      chartGlobalCatalog4.draw(viewGlobalCatalog4, optionsGlobalCatalog4);

      var dataGlobalCatalog5 = google.visualization.arrayToDataTable([
        ["API", "Services", { role: "style" } ],
        ["Global API Catalog", 405, "#89D1F3"],
        ["accounts", 0, "#009EE5"],
        ["cards", 0, "#094FA4"],
        ["others", 0, "#86C82D"]
      ]);

      var viewGlobalCatalog5 = new google.visualization.DataView(dataGlobalCatalog5);
      viewGlobalCatalog5.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var optionsGlobalCatalog5 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        'titleTextStyle': {'fontSize': 20},
        'showColorCode': true
      };
      var chartGlobalCatalog5 = new google.visualization.BarChart(document.getElementById("globalCatalog5"));
      chartGlobalCatalog5.draw(viewGlobalCatalog5, optionsGlobalCatalog5);


      var dataGlobalCatalog6 = google.visualization.arrayToDataTable([
       ["Country", "#Servicios", { role: "style" } ],
       ["Global API Catalog", 405, "#89D1F3"],
       ["Mexico", 25, "#89D1F3"],
       ["Spain", 5, "#86C82D"],
       ["Colombia", 0, "#009EE5"],
       ["Peru", 9, "#094FA4"],
       ["Chile", 25, "#86C82D"],
       ["USA", 11, "#094FA4"],
       ["Venezuela", 0, "#86C82D"],
       ["Paraguay", 0, "#009EE5"],
       ["Uruguay", 34, "#009EE5"],
       ["Argentina", 2, "#009EE5"],
       ["Turkey", 0, "#89D1F3"]
      ]);

      var viewGlobalCatalog6 = new google.visualization.DataView(dataGlobalCatalog6);
      viewGlobalCatalog6.setColumns([0, 1,
                      { calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation" },
                      2]);

      var optionsGlobalCatalog6 = {
       width: 600,
       height: 450,
       bar: {groupWidth: "95%"},
       legend: { position: "none" },
       'titleTextStyle': {'fontSize': 20},
       'showColorCode': true
      };
      var chartGlobalCatalog6 = new google.visualization.BarChart(document.getElementById("globalCatalog6"));
      chartGlobalCatalog6.draw(viewGlobalCatalog6, optionsGlobalCatalog6);


      var dataGlobalCatalog7 = google.visualization.arrayToDataTable([
       ["Country", "#Servicios", { role: "style" } ],
       ["Global API Catalog", 405, "#89D1F3"],
       ["Mexico", 0, "#89D1F3"],
       ["Spain", 0, "#86C82D"],
       ["Colombia", 0, "#009EE5"],
       ["Peru", 0, "#094FA4"],
       ["Chile", 0, "#86C82D"],
       ["USA", 0, "#094FA4"],
       ["Venezuela", 0, "#86C82D"],
       ["Paraguay", 0, "#009EE5"],
       ["Uruguay", 0, "#009EE5"],
       ["Argentina", 0, "#009EE5"],
       ["Turkey", 0, "#89D1F3"]
      ]);

      var viewGlobalCatalog7 = new google.visualization.DataView(dataGlobalCatalog7);
      viewGlobalCatalog7.setColumns([0, 1,
                      { calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation" },
                      2]);

      var optionsGlobalCatalog7 = {
       width: 600,
       height: 450,
       bar: {groupWidth: "95%"},
       legend: { position: "none" },
       'titleTextStyle': {'fontSize': 20},
       'showColorCode': true
      };
      var chartGlobalCatalog7 = new google.visualization.BarChart(document.getElementById("globalCatalog7"));
      chartGlobalCatalog7.draw(viewGlobalCatalog7, optionsGlobalCatalog7);

        /*****************contributions*******************/

        var dataAcumulado = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 4],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 3],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 9],
          [new Date(2016, 7, 15), 2],
          [new Date(2016, 7, 22), 5],
          [new Date(2016, 7, 29), 5],
          [new Date(2016, 8, 5), 4],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 2],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 1],
          [new Date(2016, 9, 24), 2],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 1],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 3],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 1],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 3],
          [new Date(2017, 0, 16), 1]

        ]);

        var optionsAcumulado = {
          height: 450,
          legend: { position: "none" },
          vAxis: {format:'0', gridlines: {count: 3}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartAcumulado = new google.visualization.AreaChart(document.getElementById("acumulado"));
        chartAcumulado.draw(dataAcumulado, optionsAcumulado);

        var dataPersona1 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 9],
          [new Date(2016, 7, 15), 1],
          [new Date(2016, 7, 22), 1],
          [new Date(2016, 7, 29), 1],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 1],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 1],
          [new Date(2016, 9, 24), 1],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 0],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona1 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona1 = new google.visualization.AreaChart(document.getElementById("persona1"));
        chartPersona1.draw(dataPersona1, optionsPersona1);

        var dataPersona2 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 1],
          [new Date(2016, 7, 22), 3],
          [new Date(2016, 7, 29), 3],
          [new Date(2016, 8, 5), 3],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 0],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona2 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona2 = new google.visualization.AreaChart(document.getElementById("persona2"));
        chartPersona2.draw(dataPersona2, optionsPersona2);

        var dataPersona3 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 3],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 3],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 0],
          [new Date(2016, 7, 29), 0],
          [new Date(2016, 8, 5), 1],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 0],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona3 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona3 = new google.visualization.AreaChart(document.getElementById("persona3"));
        chartPersona3.draw(dataPersona3, optionsPersona3);

        var dataPersona4 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 1],
          [new Date(2016, 7, 29), 1],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 0],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona4 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona4 = new google.visualization.AreaChart(document.getElementById("persona4"));
        chartPersona4.draw(dataPersona4, optionsPersona4);

        var dataPersona5 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 0],
          [new Date(2016, 7, 29), 0],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 1],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 0],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona5 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona5 = new google.visualization.AreaChart(document.getElementById("persona5"));
        chartPersona5.draw(dataPersona5, optionsPersona5);

        var dataPersona6 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 1],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 0],
          [new Date(2016, 7, 29), 0],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 0],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona6 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona6 = new google.visualization.AreaChart(document.getElementById("persona6"));
        chartPersona6.draw(dataPersona6, optionsPersona6);

        var dataPersona7 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 0],
          [new Date(2016, 7, 29), 0],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 1],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 0],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona7 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona7 = new google.visualization.AreaChart(document.getElementById("persona7"));
        chartPersona7.draw(dataPersona7, optionsPersona7);

        var dataPersona8 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 0],
          [new Date(2016, 7, 29), 0],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 1],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 0],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona8 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona8 = new google.visualization.AreaChart(document.getElementById("persona8"));
        chartPersona8.draw(dataPersona8, optionsPersona8);

        var dataPersona9 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 0],
          [new Date(2016, 7, 29), 0],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 1],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 2],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 0],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona9 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona9 = new google.visualization.AreaChart(document.getElementById("persona9"));
        chartPersona9.draw(dataPersona9, optionsPersona9);

        var dataPersona10 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 0],
          [new Date(2016, 7, 29), 0],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 1],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 0],
          [new Date(2017, 0, 16), 1]
        ]);
        var optionsPersona10 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona10 = new google.visualization.AreaChart(document.getElementById("persona10"));
        chartPersona10.draw(dataPersona10, optionsPersona10);

        var dataPersona11 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 0],
          [new Date(2016, 7, 29), 0],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 1],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona11 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona11 = new google.visualization.AreaChart(document.getElementById("persona11"));
        chartPersona11.draw(dataPersona11, optionsPersona11);

        var dataPersona12 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 0],
          [new Date(2016, 7, 29), 0],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 1],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona12 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona12 = new google.visualization.AreaChart(document.getElementById("persona12"));
        chartPersona12.draw(dataPersona12, optionsPersona12);

        var dataPersona13 = google.visualization.arrayToDataTable([
          ["Semana", "Pull Requests"],
          [new Date(2016, 5, 6), 0],
          [new Date(2016, 5, 13), 0],
          [new Date(2016, 5, 20), 0],
          [new Date(2016, 5, 27), 0],
          [new Date(2016, 6, 4), 0],
          [new Date(2016, 6, 11), 0],
          [new Date(2016, 6, 18), 0],
          [new Date(2016, 6, 25), 0],
          [new Date(2016, 7, 1), 0],
          [new Date(2016, 7, 8), 0],
          [new Date(2016, 7, 15), 0],
          [new Date(2016, 7, 22), 0],
          [new Date(2016, 7, 29), 0],
          [new Date(2016, 8, 5), 0],
          [new Date(2016, 8, 12), 0],
          [new Date(2016, 8, 19), 0],
          [new Date(2016, 8, 26), 0],
          [new Date(2016, 9, 3), 0],
          [new Date(2016, 9, 10), 0],
          [new Date(2016, 9, 17), 0],
          [new Date(2016, 9, 24), 0],
          [new Date(2016, 9, 31), 0],
          [new Date(2016, 10, 7), 0],
          [new Date(2016, 10, 14), 0],
          [new Date(2016, 10, 21), 0],
          [new Date(2016, 10, 28), 0],
          [new Date(2016, 11, 5), 0],
          [new Date(2016, 11, 12), 0],
          [new Date(2016, 11, 19), 0],
          [new Date(2016, 11, 26), 0],
          [new Date(2017, 0, 2), 0],
          [new Date(2017, 0, 9), 1],
          [new Date(2017, 0, 16), 0]
        ]);
        var optionsPersona13 = {
          width: 485,
          bar: {groupWidth: 50},
          vAxis: {format:'0', gridlines: {count: 2}},
          hAxis: {slantedText: true, slantedTextAngle: 45},
          legend: { position: "none" },
          colors: ['#93dd1c'],
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPersona13 = new google.visualization.AreaChart(document.getElementById("persona13"));
        chartPersona13.draw(dataPersona13, optionsPersona13);

     }
     function drawChartMexico() {

             var dataMexico1 = google.visualization.arrayToDataTable([
               ["Element", "Services", { role: "style" } ],
               ["accounts", 0, "#89D1F3"],
               ["cards", 0, "#009EE5"],
               ["transactions", 0, "#094FA4"],
               ["customers", 0, "#86C82D"],
               ["others", 0, "#89D1F3"]
            ]);

            var viewMexico1 = new google.visualization.DataView(dataMexico1);
            viewMexico1.setColumns([0, 1,
                             { calc: "stringify",
                               sourceColumn: 1,
                               type: "string",
                               role: "annotation" },
                             2]);

             var optionsMexico1 = {
                            'width':600,
                            'height':450,
                            legend:{position:"none"},
                            backgroundColor: { fill:'transparent' }
                          };

             var chartMexico1 = new google.visualization.BarChart(document.getElementById('mexico1'));
             chartMexico1.draw(viewMexico1, optionsMexico1);

             var dataMexico2 = google.visualization.arrayToDataTable([
              ["Country", "Services", { role: "style" } ],
              ["Global API Catalog", 0, "#89D1F3"],
              ["Local", 444, "#009EE5"],
              ["Open Platform", 0, "#094FA4"]
            ]);

            var viewMexico2 = new google.visualization.DataView(dataMexico2);
            viewMexico2.setColumns([0, 1,
                             { calc: "stringify",
                               sourceColumn: 1,
                               type: "string",
                               role: "annotation" },
                             2]);

            var optionsMexico2 = {
              width: 600,
              height: 450,
              bar: {groupWidth: "95%"},
              legend: { position: "none" },
              'titleTextStyle': {'fontSize': 20},
              'showColorCode': true
            };
            var chartMexico2 = new google.visualization.BarChart(document.getElementById("mexico2"));
            chartMexico2.draw(viewMexico2, optionsMexico2);

            var dataMexico3 = google.visualization.arrayToDataTable([
              ["Country", "Services", { role: "style" } ],
              ["Global API Catalog", 0, "#89D1F3"],
              ["Local", 207, "#009EE5"],
              ["Open Platform", 0, "#094FA4"]
            ]);

            var viewMexico3 = new google.visualization.DataView(dataMexico3);
            viewMexico3.setColumns([0, 1,
                             { calc: "stringify",
                               sourceColumn: 1,
                               type: "string",
                               role: "annotation" },
                             2]);

            var optionsMexico3 = {
              width: 600,
              height: 450,
              bar: {groupWidth: "95%"},
              legend: { position: "none" },
              'titleTextStyle': {'fontSize': 20},
              'showColorCode': true
            };
            var chartMexico3 = new google.visualization.BarChart(document.getElementById("mexico3"));
            chartMexico3.draw(viewMexico3, optionsMexico3);

            var dataMexico4 = google.visualization.arrayToDataTable([
                 ['API', 'Local', { role: "style" }],
                 ['accounts', 27, "#89D1F3"],
                 ['customers', 15, "#009EE5"],
                 ['loans', 12, "#094FA4" ],
                 ['transfers', 7, "#86C82D"],
                 ['cards', 5, "#89D1F3"],
                 ['notifications', 4, "#009EE5"],
                 ['others', 374, "#094FA4"]
               ]);

            var optionsMexico4 = {
                 width: 600,
                 height: 450,
                 legend: {position: 'none'}
               };

            var chartMexico4 = new google.visualization.BarChart(document.getElementById('mexico4'));
            chartMexico4.draw(dataMexico4, optionsMexico4);

            var dataMexico5 = google.visualization.arrayToDataTable([
              ["Element", "Services", { role: "style" } ],
              ["accounts", 7, "#89D1F3"],
              ["cards", 6, "#009EE5"],
              ["deposits", 5, "#094FA4"],
              ["transfers", 3,  "#86C82D"],
              ["loans", 2, "#89D1F3"],
              ["customers", 1, "#009EE5"]
           ]);

           var viewMexico5 = new google.visualization.DataView(dataMexico5);
           viewMexico5.setColumns([0, 1,
                            { calc: "stringify",
                              sourceColumn: 1,
                              type: "string",
                              role: "annotation" },
                            2]);

            var optionsMexico5 = {
              width: 600,
              height: 450,
              legend: { position: "none" }};

            var chartMexico5 = new google.visualization.BarChart(document.getElementById('mexico5'));
            chartMexico5.draw(viewMexico5, optionsMexico5);

            var dataMexico6 = google.visualization.arrayToDataTable([
              ["Country", "Services", { role: "style" } ],
              ["Global API Catalog", 24, "#89D1F3"],
              ["Local", 493, "#009EE5"],
              ["Open Platform", 0, "#094FA4"]
            ]);

            var viewMexico6 = new google.visualization.DataView(dataMexico6);
            viewMexico6.setColumns([0, 1,
                            { calc: "stringify",
                              sourceColumn: 1,
                              type: "string",
                              role: "annotation" },
                            2]);

            var optionsMexico6 = {
             width: 600,
             height: 450,
             bar: {groupWidth: "95%"},
             legend: { position: "none" },
             'titleTextStyle': {'fontSize': 20},
             'showColorCode': true
            };
            var chartMexico6 = new google.visualization.BarChart(document.getElementById("mexico6"));
            chartMexico6.draw(viewMexico6, optionsMexico6);

            var dataMexico7 = google.visualization.arrayToDataTable([
              ["Country", "Services", { role: "style" } ],
              ["Global API Catalog", 0, "#89D1F3"],
              ["Local", 60, "#009EE5"],
              ["Open Platform", 0, "#094FA4"]
            ]);

            var viewMexico7 = new google.visualization.DataView(dataMexico7);
            viewMexico7.setColumns([0, 1,
                             { calc: "stringify",
                               sourceColumn: 1,
                               type: "string",
                               role: "annotation" },
                             2]);

            var optionsMexico7 = {

              width: 600,
              height: 450,
              bar: {groupWidth: "95%"},
              legend: { position: "none" },
              'titleTextStyle': {'fontSize': 20},
              'showColorCode': true
            };
            var chartMexico7 = new google.visualization.BarChart(document.getElementById("mexico7"));
            chartMexico7.draw(viewMexico7, optionsMexico7);


           var dataMexico10 = google.visualization.arrayToDataTable([
                ['API', 'Local', { role: "style" }],
                ['accounts', 8,"#89D1F3"],
                ['customers', 21, "#009EE5"],
                ['cards', 25, "#094FA4"],
                ['loans', 17, "#86C82D"],
                ['transfers', 15, "#89D1F3"],
                ['others', 407, "#009EE5"]
              ]);

           var optionsMexico10 = {
                width: 600,
                height: 450,
                legend: {position: 'none'}
              };

           var chartMexico10 = new google.visualization.BarChart(document.getElementById('mexico10'));
           chartMexico10.draw(dataMexico10, optionsMexico10);

           var dataMexicoConsumptions1 = google.visualization.arrayToDataTable([
            ["APP", "Invocations", { role: "style" } ],
            ["BMovilPrivada", 63.1, "#89D1F3"],
            ["APX", 32.2, "#009EE5"],
            ["SendPublica", 22.4, "#094FA4"],
            ["Personas", 8.8,"#86C82D"],
            ["BMovilPub", 1.1,"#89D1F3"],
            ["SendPrivada", 0.8,"#009EE5"],
            ["GCC_SAC", 0.4,"#094FA4"],
            ["PaymentPri", 0.2,"#86C82D"],
            ["GCC_IVR_Pub", 0.1,"#89D1F3"],
            ["SavePrivada", 0.1,"#009EE5"]
          ]);

          var viewMexicoConsumptions1 = new google.visualization.DataView(dataMexicoConsumptions1);
          viewMexicoConsumptions1.setColumns([0, 1,
                           { calc: "stringify",
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation" },
                           2]);

          var optionsMexicoConsumptions1 = {
            width: 600,
            height: 450,
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
            'titleTextStyle': {'fontSize': 20},
            hAxis: {slantedText: true, slantedTextAngle: 45},
            chartArea: {bottom:125},
            'showColorCode': true
          };
          var chartMexicoConsumptions1 = new google.visualization.ColumnChart(document.getElementById("mexicoConsumptions1"));
          chartMexicoConsumptions1.draw(viewMexicoConsumptions1, optionsMexicoConsumptions1);

          var dataMexicoConsumptions2 = google.visualization.arrayToDataTable([
           ["Services", "Invocations", { role: "style" } ],
           ["validateOptToken", 32.1, "#89D1F3"],
           ["createTicket", 29.7, "#009EE5"],
           ["getState", 11.5, "#094FA4"],
           ["createTicketMobile", 11.0, "#86C82D"],
           ["proxyServiceAuth", 10.7, "#89D1F3"],
           ["getFinancialDashboard", 10.6, "#009EE5"],
           ["listCampaigns", 10.6, "#89D1F3"],
           ["getSpeiAccount", 9.4, "#009EE5"],
           ["createUserWithoutSession", 1.7, "#094FA4"],
           ["deleteTicket", 0.2, "#86C82D"]
          ]);

          var viewMexicoConsumptions2 = new google.visualization.DataView(dataMexicoConsumptions2);
          viewMexicoConsumptions2.setColumns([0, 1,
                          { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                          2]);

          var optionsMexicoConsumptions2 = {
           width: 600,
           height: 450,
           bar: {groupWidth: "95%"},
           legend: { position: "none" },
           hAxis: {slantedText: true, slantedTextAngle: 45},
           chartArea: {bottom:150},
           'titleTextStyle': {'fontSize': 20},
           'showColorCode': true
          };
          var chartMexicoConsumptions2 = new google.visualization.ColumnChart(document.getElementById("mexicoConsumptions2"));
          chartMexicoConsumptions2.draw(viewMexicoConsumptions2, optionsMexicoConsumptions2);

      }
     function drawChartColombia() {
         var dataColombia1 = google.visualization.arrayToDataTable([
           ["Element", "Services", { role: "style" } ],
           ["cards", 15, "#89D1F3"],
           ["accounts", 1, "#009EE5"],
           ["customers", 1, "#094FA4"]
        ]);

        var viewColombia1 = new google.visualization.DataView(dataColombia1);
        viewColombia1.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

         var optionsColombia1 = {'width':600,
         'height':450,
         legend:{position:"none"}};

         var chartColombia1 = new google.visualization.BarChart(document.getElementById('colombia1'));
         chartColombia1.draw(viewColombia1, optionsColombia1);

         var dataColombia2 = google.visualization.arrayToDataTable([
           ["Country", "Services", { role: "style" } ],
           ["Global API Catalog", 17, "#89D1F3"],
           ["Local", 155, "#009EE5"],
           ["Open Platform", 0, "#094FA4"]
        ]);

        var viewColombia2 = new google.visualization.DataView(dataColombia2);
        viewColombia2.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

        var optionsColombia2 = {

          width: 600,
          height: 450,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartColombia2 = new google.visualization.BarChart(document.getElementById("colombia2"));
        chartColombia2.draw(viewColombia2, optionsColombia2);

        var dataColombia3 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 15, "#89D1F3"],
          ["Local", 115, "#009EE5"],
          ["Open Platform", 0, "#094FA4"]
        ]);

        var viewColombia3 = new google.visualization.DataView(dataColombia3);
        viewColombia3.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

        var optionsColombia3 = {

          width: 600,
          height: 450,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartColombia3 = new google.visualization.BarChart(document.getElementById("colombia3"));
        chartColombia3.draw(viewColombia3, optionsColombia3);

        var dataColombia4 = google.visualization.arrayToDataTable([
          ["Element", "Services", { role: "style" } ],
          ["cards", 20, "#89D1F3"],
          ["loans", 14, "#009EE5"],
          ["accounts", 12, "#094FA4"],
          ["deposits", 9, "#86C82D"],
          ["customers", 9, "#89D1F3"],
          ["investmentFunds", 7, "#009EE5"],
          ["others", 84, "#094FA4"]
        ]);
        var viewColombia4 = new google.visualization.DataView(dataColombia4);
          viewColombia4.setColumns([0, 1,
                          { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                          2]);

        var optionsColombia4 = {
             width: 600,
             height: 450,
             bar: {groupWidth: "95%"},
             legend: {position: 'none'}};

        var chartColombia4 = new google.visualization.BarChart(document.getElementById('colombia4'));
        chartColombia4.draw(dataColombia4, optionsColombia4);

        var dataColombia5 = google.visualization.arrayToDataTable([
          ["Element", "Services", { role: "style" } ],
          ["accounts", 22, "#89D1F3"],
          ["cards", 22, "#009EE5"],
          ["transfers", 20, "#094FA4"],
          ["customers", 19, "#86C82D"],
          ["loans", 13, "#89D1F3"],
          ["investmentFunds", 16, "#009EE5"],
          ["deposits", 12, "#094FA4"],
          ["transactions", 3, "#86C82D"],
          ["others", 4, "#89D1F3"]
        ]);

        var viewColombia5 = new google.visualization.DataView(dataColombia5);
        viewColombia5.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

        var optionsColombia5 = {'width':600,
        'height':450,
        legend:{position:"none"}};

        var chartColombia5 = new google.visualization.BarChart(document.getElementById('colombia5'));
        chartColombia5.draw(viewColombia5, optionsColombia5);

        var dataColombia6 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 131, "#89D1F3"],
          ["Local", 235, "#009EE5"],
          ["Open Platform", 0, "#094FA4"]
       ]);

       var viewColombia6 = new google.visualization.DataView(dataColombia6);
       viewColombia6.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsColombia6 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartColombia6 = new google.visualization.BarChart(document.getElementById("colombia6"));
       chartColombia6.draw(viewColombia6, optionsColombia6);

       var dataColombia7 = google.visualization.arrayToDataTable([
         ["Country", "Services", { role: "style" } ],
         ["Global API Catalog", 2, "#89D1F3"],
         ["Local", 6, "#009EE5"],
         ["Open Platform", 0, "#094FA4"]
       ]);

       var viewColombia7 = new google.visualization.DataView(dataColombia7);
       viewColombia7.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsColombia7 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartColombia7 = new google.visualization.BarChart(document.getElementById("colombia7"));
       chartColombia7.draw(viewColombia7, optionsColombia7);

       var dataColombia10 = google.visualization.arrayToDataTable([
            ['API', 'Local', { role: "style" }],
            ['loans', 26, "#89D1F3"],
            ['cards', 20, "#009EE5"],
            ['customers', 15,"#094FA4" ],
            ['accounts', 12, "#86C82D"],
            ['payments', 9,"#89D1F3" ],
            ['deposits', 9, "#009EE5"],
            ['investmentFunds', 7, "#094FA4"],
            ['transfers', 3, "#86C82D"],
            ['others', 28, "#89D1F3"]
          ]);

       var optionsColombia10 = {
            width: 600,
            height: 450,
            legend: {position: 'none'}
          };

       var chartColombia10 = new google.visualization.BarChart(document.getElementById('colombia10'));
       chartColombia10.draw(dataColombia10, optionsColombia10);

       var dataColombia8 = google.visualization.arrayToDataTable([
         ["Element", "Services", { role: "style" } ],
         ["cards", 10, "#89D1F3"],
         ["accounts", 1, "#009EE5"],
         ["customers", 1, "#094FA4"]
      ]);

      var viewColombia8 = new google.visualization.DataView(dataColombia8);
      viewColombia8.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

       var optionsColombia8 = {'width':600,
       'height':450,
       legend:{position:"none"}};

       var chartColombia8 = new google.visualization.BarChart(document.getElementById('colombia8'));
       chartColombia8.draw(viewColombia8, optionsColombia8);

       var dataColombia9 = google.visualization.arrayToDataTable([
         ["Element", "Services", { role: "style" } ],
         ["cards", 5, "#89D1F3"],
         ["accounts", 0, "#009EE5"],
         ["customers", 0, "#094FA4"]
      ]);

      var viewColombia9 = new google.visualization.DataView(dataColombia9);
      viewColombia9.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

       var optionsColombia9 = {'width':600,
       'height':450,
       legend:{position:"none"}};

       var chartColombia9 = new google.visualization.BarChart(document.getElementById('colombia9'));
       chartColombia9.draw(viewColombia9, optionsColombia9);

       var dataColombiaConsumptions1 = google.visualization.arrayToDataTable([
        ["APP", "Invocations", { role: "style" } ],
        ["BNet", 1.6, "#89D1F3"],
        ["Wallet", 0.4, "#009EE5"]
       ]);

       var viewColombiaConsumptions1 = new google.visualization.DataView(dataColombiaConsumptions1);
       viewColombiaConsumptions1.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

       var optionsColombiaConsumptions1 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        'titleTextStyle': {'fontSize': 20},
        hAxis: {slantedText: true, slantedTextAngle: 45},
        chartArea: {bottom:125},
        'showColorCode': true
       };
       var chartColombiaConsumptions1 = new google.visualization.ColumnChart(document.getElementById("colombiaConsumptions1"));
       chartColombiaConsumptions1.draw(viewColombiaConsumptions1, optionsColombiaConsumptions1);

       var dataColombiaConsumptions2 = google.visualization.arrayToDataTable([
       ["Services", "Invocations", { role: "style" } ],
       ["getExtractGlobalBalance", 301.3, "#89D1F3"],
       ["getCustomer", 277.7, "#009EE5"],
       ["createTicket", 192.9, "#094FA4"],
       ["listOffer", 149.7, "#86C82D"],
       ["getCard", 95.3, "#89D1F3"],
       ["getNotification", 88.7, "#009EE5"],
       ["listCards", 79.7, "#094FA4"],
       ["listAccounts", 77.5, "#86C82D"],
       ["getCustomer", 66.0, "#89D1F3"],
       ["listCardTransactions", 64.3, "#009EE5"]
       ]);

       var viewColombiaConsumptions2 = new google.visualization.DataView(dataColombiaConsumptions2);
       viewColombiaConsumptions2.setColumns([0, 1,
                      { calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation" },
                      2]);

       var optionsColombiaConsumptions2 = {
       width: 600,
       height: 450,
       bar: {groupWidth: "95%"},
       legend: { position: "none" },
       hAxis: {slantedText: true, slantedTextAngle: 45},
       chartArea: {bottom:150},
       'titleTextStyle': {'fontSize': 20},
       'showColorCode': true
       };
       var chartColombiaConsumptions2 = new google.visualization.ColumnChart(document.getElementById("colombiaConsumptions2"));
       chartColombiaConsumptions2.draw(viewColombiaConsumptions2, optionsColombiaConsumptions2);


    }
     function drawChartPeru() {
         var dataPeru1 = google.visualization.arrayToDataTable([
           ["Element", "Services", { role: "style" } ],
           ["customers", 0, "#89D1F3"],
           ["accounts", 0, "#009EE5"],
           ["catalogs", 0, "#094FA4"],
           ["others", 0, "#86C82D"]
        ]);

        var viewPeru1 = new google.visualization.DataView(dataPeru1);
        viewPeru1.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

         var optionsPeru1 = {'width':600,
         'height':450,
         legend:{position:"none"}};

         var chartPeru1 = new google.visualization.BarChart(document.getElementById('peru1'));
         chartPeru1.draw(viewPeru1, optionsPeru1);

         var dataPeru2 = google.visualization.arrayToDataTable([
           ["Country", "Services", { role: "style" } ],
           ["Global API Catalog", 0, "#89D1F3"],
           ["Local", 127, "#009EE5"],
           ["Open Platform", 0, "#094FA4"]
        ]);

        var viewPeru2 = new google.visualization.DataView(dataPeru2);
        viewPeru2.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

        var optionsPeru2 = {

          width: 600,
          height: 450,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
          hAxis: {viewWindow: {max:130}},
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPeru2 = new google.visualization.BarChart(document.getElementById("peru2"));
        chartPeru2.draw(viewPeru2, optionsPeru2);

        var dataPeru3 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 0, "#89D1F3"],
          ["Local", 117, "#009EE5"],
          ["Open Platform", 0, "#094FA4"]
        ]);

        var viewPeru3 = new google.visualization.DataView(dataPeru3);
        viewPeru3.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

        var optionsPeru3 = {

          width: 600,
          height: 450,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
          hAxis: {viewWindow: {max:130}},
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartPeru3 = new google.visualization.BarChart(document.getElementById("peru3"));
        chartPeru3.draw(viewPeru3, optionsPeru3);

        var dataPeru4 = google.visualization.arrayToDataTable([
          ['API', 'Local', { role: "style" } ],
          ['transfers', 19, "#89D1F3"],
          ['payments', 13, "#009EE5"],
          ['customers', 10, "#094FA4"],
          ['cards', 9, "#86C82D"],
          ['loans', 9, "#89D1F3"],
          ['contactBooks', 6, "#009EE5"],
          ['accounts', 5, "#094FA4"],
          ['investmentFunds', 5, "#86C82D"],
          ['deposits', 1, "#89D1F3"],
          ['others', 50, "#009EE5"]
        ]);

        var optionsPeru4 = {
             width: 600,
             height: 450,
             legend: {position: 'none'}};

        var chartPeru4 = new google.visualization.BarChart(document.getElementById('peru4'));
        chartPeru4.draw(dataPeru4, optionsPeru4);

        var dataPeru5 = google.visualization.arrayToDataTable([
          ["Element", "Services", { role: "style" } ],
          ["customers", 14, "#89D1F3"],
          ["cards", 2, "#009EE5"],
          ["catalogs", 1, "#094FA4"]
       ]);

       var viewPeru5 = new google.visualization.DataView(dataPeru5);
       viewPeru5.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

        var optionsPeru5 = {'width':600,
        'height':450,
        legend:{position:"none"}};

        var chartPeru5 = new google.visualization.BarChart(document.getElementById('peru5'));
        chartPeru5.draw(viewPeru5, optionsPeru5);

        var dataPeru6 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 17, "#89D1F3"],
          ["Local", 30, "#009EE5"],
          ["Open Platform", 0, "#094FA4"]
       ]);

       var viewPeru6 = new google.visualization.DataView(dataPeru6);
       viewPeru6.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsPeru6 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         hAxis: {gridlines: {count: 4}},
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartPeru6 = new google.visualization.BarChart(document.getElementById("peru6"));
       chartPeru6.draw(viewPeru6, optionsPeru6);

       var dataPeru7 = google.visualization.arrayToDataTable([
         ["Country", "Services", { role: "style" } ],
         ["Global API Catalog", 0, "#89D1F3"],
         ["Local", 8, "#009EE5"],
         ["Open Platform", 0, "#094FA4"]
       ]);

       var viewPeru7 = new google.visualization.DataView(dataPeru7);
       viewPeru7.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsPeru7 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartPeru7 = new google.visualization.BarChart(document.getElementById("peru7"));
       chartPeru7.draw(viewPeru7, optionsPeru7);

       var dataPeru10 = google.visualization.arrayToDataTable([
            ['API', 'Local', { role: "style" }],
            ['cards', 1, "#89D1F3"],
            ['accounts', 1, "#009EE5"],
            ['others', 28, "#094FA4"],

          ]);

       var optionsPeru10 = {
            width: 600,
            height: 450,
            legend: {position: 'none'}
          };

       var chartPeru10 = new google.visualization.BarChart(document.getElementById('peru10'));
       chartPeru10.draw(dataPeru10, optionsPeru10);
       var dataPeruConsumptions1 = google.visualization.arrayToDataTable([
        ["APP", "Invocations", { role: "style" } ],
        ["BNet Privada", 25.4, "#89D1F3"],
        ["Pic", 1.8, "#009EE5"],
        ["Others", 0.58, "#094FA4"]
      ]);

      var viewPeruConsumptions1 = new google.visualization.DataView(dataPeruConsumptions1);
      viewPeruConsumptions1.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var optionsPeruConsumptions1 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        'titleTextStyle': {'fontSize': 20},
        hAxis: {slantedText: true, slantedTextAngle: 45},
        chartArea: {bottom:125},
        'showColorCode': true
      };
      var chartPeruConsumptions1 = new google.visualization.ColumnChart(document.getElementById("peruConsumptions1"));
      chartPeruConsumptions1.draw(viewPeruConsumptions1, optionsPeruConsumptions1);

      var dataPeruConsumptions2 = google.visualization.arrayToDataTable([
       ["Services", "Invocations", { role: "style" } ],
       ["listFrequentOperations", 3.9, "#89D1F3"],
       ["getFinancialDashboard", 3.1, "#009EE5"],
       ["listExchangeRateValues", 2.4, "#094FA4"],
       ["createTicket", 1.9, "#86C82D"],
       ["getUserDashboard", 1.8, "#89D1F3"],
       ["listCommercialCampaigns", 1.8, "#009EE5"],
       ["getAccount", 1.7, "#094FA4"],
       ["listAccountTransactions", 1.5, "#86C82D"],
       ["deleteTicket", 0.9, "#89D1F3"],
       ["createTicketMobile", 0.7, "#009EE5"]
      ]);

      var viewPeruConsumptions2 = new google.visualization.DataView(dataPeruConsumptions2);
      viewPeruConsumptions2.setColumns([0, 1,
                      { calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation" },
                      2]);

      var optionsPeruConsumptions2 = {
       width: 600,
       height: 450,
       bar: {groupWidth: "95%"},
       legend: { position: "none" },
       hAxis: {slantedText: true, slantedTextAngle: 45},
       chartArea: {bottom:150},
       'titleTextStyle': {'fontSize': 20},
       'showColorCode': true
      };
      var chartPeruConsumptions2 = new google.visualization.ColumnChart(document.getElementById("peruConsumptions2"));
      chartPeruConsumptions2.draw(viewPeruConsumptions2, optionsPeruConsumptions2);

     }
     function drawChartChile() {
           var dataChile1 = google.visualization.arrayToDataTable([
             ["Element", "Services", { role: "style" } ],
             ["accounts", 0, "#89D1F3"],
             ["cards", 0, "#009EE5"],
             ["transactions", 0, "#094FA4"],
             ["customers", 0, "#86C82D"],
             ["others", 0, "#89D1F3"]
          ]);

          var viewChile1 = new google.visualization.DataView(dataChile1);
          viewChile1.setColumns([0, 1,
                           { calc: "stringify",
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation" },
                           2]);

           var optionsChile1 = {'width':600,
           'height':450,
           legend:{position:"none"}};

           var chartChile1 = new google.visualization.BarChart(document.getElementById('chile1'));
           chartChile1.draw(viewChile1, optionsChile1);

           var dataChile2 = google.visualization.arrayToDataTable([
             ["Country", "Services", { role: "style" } ],
             ["Global API Catalog", 0, "#89D1F3"],
             ["Local", 191, "#009EE5"],
             ["Open Platform", 0, "#094FA4"]
          ]);

          var viewChile2 = new google.visualization.DataView(dataChile2);
          viewChile2.setColumns([0, 1,
                           { calc: "stringify",
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation" },
                           2]);

          var optionsChile2 = {

            width: 600,
            height: 450,
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
            'titleTextStyle': {'fontSize': 20},
            'showColorCode': true
          };
          var chartChile2 = new google.visualization.BarChart(document.getElementById("chile2"));
          chartChile2.draw(viewChile2, optionsChile2);

          var dataChile3 = google.visualization.arrayToDataTable([
            ["Country", "Services", { role: "style" } ],
            ["Global API Catalog", 0, "#89D1F3"],
            ["Local", 172, "#009EE5"],
            ["Open Platform", 0, "#094FA4"]
          ]);

          var viewChile3 = new google.visualization.DataView(dataChile3);
          viewChile3.setColumns([0, 1,
                           { calc: "stringify",
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation" },
                           2]);

          var optionsChile3 = {

            width: 600,
            height: 450,
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
            'titleTextStyle': {'fontSize': 20},
            'showColorCode': true
          };
          var chartChile3 = new google.visualization.BarChart(document.getElementById("chile3"));
          chartChile3.draw(viewChile3, optionsChile3);

          var dataChile4 = google.visualization.arrayToDataTable([
            ["API", "Local", { role: "style" }  ],
            ['customers', 27, "#89D1F3"],
            ['loans', 9, "#009EE5"],
            ['accounts', 7, "#094FA4"],
            ['investmentFunds', 10, "#86C82D"],
            ['others', 138, "#89D1F3"]
             ]);
          var viewChile4 = new google.visualization.DataView(dataChile4);
          viewChile4.setColumns([0, 1,
                          { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                          2]);
          var optionsChile4 = {
               width: 600,
               height: 450,
               legend: {position: 'none'}
             };

          var chartChile4 = new google.visualization.BarChart(document.getElementById('chile4'));
          chartChile4.draw(viewChile4, optionsChile4);

          var dataChile5 = google.visualization.arrayToDataTable([
            ["Element", "Services", { role: "style" } ],
            ["accounts", 4, "#89D1F3"],
            ["cards", 8, "#009EE5"],
            ["catalogs", 2, "#094FA4"],
            ["contactsBook", 8, "#86C82D"],
            ["investmentFunds", 6, "#89D1F3"],
            ["loans", 4, "#009EE5"],
            ["transfers", 1, "#094FA4"],
            ["mobile", 3, "#86C82D"],
            ["devices", 5, "#89D1F3"]
          ]);

          var viewChile5 = new google.visualization.DataView(dataChile5);
          viewChile5.setColumns([0, 1,
                          { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                          2]);

          var optionsChile5 = {'width':600,
          'height':450,
          legend:{position:"none"}};

          var chartChile5 = new google.visualization.BarChart(document.getElementById('chile5'));
          chartChile5.draw(viewChile5, optionsChile5);

          var dataChile6 = google.visualization.arrayToDataTable([
            ["Country", "Services", { role: "style" } ],
            ["Global API Catalog", 41, "#89D1F3"],
            ["Local", 80, "#009EE5"],
            ["Open Platform", 0, "#094FA4"]
          ]);

          var viewChile6 = new google.visualization.DataView(dataChile6);
          viewChile6.setColumns([0, 1,
                          { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                          2]);

          var optionsChile6 = {

           width: 600,
           height: 450,
           bar: {groupWidth: "95%"},
           legend: { position: "none" },
           'titleTextStyle': {'fontSize': 20},
           'showColorCode': true
          };
          var chartChile6 = new google.visualization.BarChart(document.getElementById("chile6"));
          chartChile6.draw(viewChile6, optionsChile6);

          var dataChile7 = google.visualization.arrayToDataTable([
            ["Country", "Services", { role: "style" } ],
            ["Global API Catalog", 0, "#89D1F3"],
            ["Local", 47, "#009EE5"],
            ["Open Platform", 0, "#094FA4"]
          ]);

          var viewChile7 = new google.visualization.DataView(dataChile7);
          viewChile7.setColumns([0, 1,
                           { calc: "stringify",
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation" },
                           2]);

          var optionsChile7 = {

            width: 600,
            height: 450,
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
            'titleTextStyle': {'fontSize': 20},
            'showColorCode': true
          };
          var chartChile7 = new google.visualization.BarChart(document.getElementById("chile7"));
          chartChile7.draw(viewChile7, optionsChile7);


          var dataChile10 = google.visualization.arrayToDataTable([
               ['API', 'Local', { role: "style" }],
               ['customers', 1, "#89D1F3"],
               ['loans', 7, "#009EE5"],
               ['accounts', 4, "#094FA4"],
               ['others', 68, "#86C82D"]
             ]);

          var optionsChile10 = {
               width: 600,
               height: 450,
               legend: {position: 'none'}
             };

          var chartChile10 = new google.visualization.BarChart(document.getElementById('chile10'));
          chartChile10.draw(dataChile10, optionsChile10);


           var dataChileConsumptions1 = google.visualization.arrayToDataTable([
            ["APP", "Invocations", { role: "style" } ],
            ["BNet", 53.0, "#89D1F3"],
            ["BMovil", 23.8, "#009EE5"],
            ["Branches", 9.5, "#094FA4"],
            ["Others", 0.8, "#86C82D"]
          ]);

          var viewChileConsumptions1 = new google.visualization.DataView(dataChileConsumptions1);
          viewChileConsumptions1.setColumns([0, 1,
                           { calc: "stringify",
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation" },
                           2]);

          var optionsChileConsumptions1 = {
            width: 600,
            height: 450,
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
            'titleTextStyle': {'fontSize': 20},
            hAxis: {slantedText: true, slantedTextAngle: 45},
            chartArea: {bottom:125},
            'showColorCode': true
          };
          var chartChileConsumptions1 = new google.visualization.ColumnChart(document.getElementById("chileConsumptions1"));
          chartChileConsumptions1.draw(viewChileConsumptions1, optionsChileConsumptions1);

          var dataChileConsumptions2 = google.visualization.arrayToDataTable([
           ["Services", "Invocations", { role: "style" } ],
           ["getProducts", 12.6, "#89D1F3"],
           ["getExecutive", 7.6, "#009EE5"],
           ["getCustomer", 6.9, "#094FA4"],
           ["createTicket", 4.8, "#86C82D"],
           ["getAccountMovements", 4.8, "#89D1F3"],
           ["getHoldings", 4.5, "#009EE5"],
           ["getCustomerOffer", 4.4, "#094FA4"],
           ["getAccountBalance", 3.5, "#86C82D"],
           ["getEnrollSoftToken", 3.4, "#89D1F3"],
           ["createLogWebExecutionUser", 3.3, "#009EE5"]
          ]);

          var viewChileConsumptions2 = new google.visualization.DataView(dataChileConsumptions2);
          viewChileConsumptions2.setColumns([0, 1,
                          { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                          2]);

          var optionsChileConsumptions2 = {
           width: 600,
           height: 450,
           bar: {groupWidth: "95%"},
           legend: { position: "none" },
           hAxis: {slantedText: true, slantedTextAngle: 45},
           chartArea: {bottom:150},
           'titleTextStyle': {'fontSize': 20},
           'showColorCode': true
          };
          var chartChileConsumptions2 = new google.visualization.ColumnChart(document.getElementById("chileConsumptions2"));
          chartChileConsumptions2.draw(viewChileConsumptions2, optionsChileConsumptions2);
     }
     function drawChartArgentina() {
        var dataArgentina1 = google.visualization.arrayToDataTable([
          ["Element", "Services", { role: "style" } ],
          ["accounts", 0, "#89D1F3"],
          ["cards", 0, "#009EE5"],
          ["transactions", 0, "#094FA4"],
          ["customers", 0, "#86C82D"],
          ["others", 0, "#89D1F3"]
       ]);

        var viewArgentina1 = new google.visualization.DataView(dataArgentina1);
        viewArgentina1.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

         var optionsArgentina1 = {'width':600,
         'height':450,
         legend:{position:"none"}};

         var chartArgentina1 = new google.visualization.BarChart(document.getElementById('argentina1'));
         chartArgentina1.draw(viewArgentina1, optionsArgentina1);

         var dataArgentina2 = google.visualization.arrayToDataTable([
           ["Country", "Services", { role: "style" } ],
           ["Global API Catalog", 0, "#89D1F3"],
           ["Local", 429, "#009EE5"],
           ["Open Platform", 0, "#094FA4"]
        ]);

        var viewArgentina2 = new google.visualization.DataView(dataArgentina2);
        viewArgentina2.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

        var optionsArgentina2 = {

          width: 600,
          height: 450,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartArgentina2 = new google.visualization.BarChart(document.getElementById("argentina2"));
        chartArgentina2.draw(viewArgentina2, optionsArgentina2);

        var dataArgentina3 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 0, "#89D1F3"],
          ["Local", 248, "#009EE5"],
          ["Open Platform", 0, "#094FA4"]
        ]);

        var viewArgentina3 = new google.visualization.DataView(dataArgentina3);
        viewArgentina3.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

        var optionsArgentina3 = {

          width: 600,
          height: 450,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartArgentina3 = new google.visualization.BarChart(document.getElementById("argentina3"));
        chartArgentina3.draw(viewArgentina3, optionsArgentina3);

        var dataArgentina4 = google.visualization.arrayToDataTable([
          ["Element", "Services", { role: "style" } ],
          ["loans", 150, "#89D1F3"],
          ["employees", 58, "#009EE5"],
          ["catalogs", 33, "#094FA4"],
          ["products", 30, "#86C82D"],
          ["security", 22, "#89D1F3"],
          ["customers", 15, "#009EE5"],
          ["documents", 15, "#094FA4"],
          ["devices", 9, "#86C82D"],
          ["contactsBook", 6, "#89D1F3"],
          ["promotions", 6, "#009EE5"],
          ["others", 85, "#094FA4"]
        ]);
        var viewArgentina4 = new google.visualization.DataView(dataArgentina4);
        viewArgentina3.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);
        var optionsArgentina4 = {
             width: 600,
             height: 450,

             legend: {position: 'none'}};

        var chartArgentina4 = new google.visualization.BarChart(document.getElementById('argentina4'));
        chartArgentina4.draw(viewArgentina4, optionsArgentina4);

        var dataArgentina5 = google.visualization.arrayToDataTable([
          ["Element", "Services", { role: "style" } ],
          ["accounts", 0, "#89D1F3"],
          ["cards", 0, "#009EE5"],
          ["transactions", 0, "#094FA4"],
          ["customers", 0, "#86C82D"],
          ["others", 0, "#89D1F3"]
        ]);

        var viewArgentina5 = new google.visualization.DataView(dataArgentina5);
        viewArgentina5.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

         var optionsArgentina5 = {'width':600,
         'height':450,
         legend:{position:"none"}};

         var chartArgentina5 = new google.visualization.BarChart(document.getElementById('argentina5'));
         chartArgentina5.draw(viewArgentina5, optionsArgentina5);

         var dataArgentina6 = google.visualization.arrayToDataTable([
           ["Country", "Services", { role: "style" } ],
           ["Global API Catalog", 0, "#89D1F3"],
           ["Local", 59, "#009EE5"],
           ["Open Platform", 0, "#094FA4"]
        ]);

        var viewArgentina6 = new google.visualization.DataView(dataArgentina6);
        viewArgentina6.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

        var optionsArgentina6 = {

          width: 600,
          height: 450,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartArgentina6 = new google.visualization.BarChart(document.getElementById("argentina6"));
        chartArgentina6.draw(viewArgentina6, optionsArgentina6);

        var dataArgentina7 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 0, "#89D1F3"],
          ["Local", 57, "#009EE5"],
          ["Open Platform", 0, "#094FA4"]
        ]);

        var viewArgentina7 = new google.visualization.DataView(dataArgentina7);
        viewArgentina7.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

        var optionsArgentina7 = {

          width: 600,
          height: 450,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
        };
        var chartArgentina7 = new google.visualization.BarChart(document.getElementById("argentina7"));
        chartArgentina7.draw(viewArgentina7, optionsArgentina7);

        var dataArgentina10 = google.visualization.arrayToDataTable([
             ['API', 'Local', { role: "style" }],
             ['employees', 8, "#89D1F3"],
             ['catalogs', 26, "#009EE5"],
             ['customers', 12, "#094FA4"],
             ['documents', 1, "#86C82D"],
             ['devices', 2, "#89D1F3"],
             ['contactsBook', 2, "#009EE5"],
             ['others', 8, "#094FA4"]
           ]);

        var optionsArgentina10 = {
             width: 600,
             height: 450,
             legend: {position: 'none'}
           };

        var chartArgentina10 = new google.visualization.BarChart(document.getElementById('argentina10'));
        chartArgentina10.draw(dataArgentina10, optionsArgentina10);
      }
     function drawChartSpain() {
       var dataSpain1 = google.visualization.arrayToDataTable([
         ["Element", "Services", { role: "style" } ],
         ["accounts", 0, "#89D1F3"],
         ["cards", 0, "#009EE5"],
         ["transactions", 0, "#094FA4"],
         ["customers", 0, "#86C82D"],
         ["others", 0, "#89D1F3"]
      ]);

       var viewSpain1 = new google.visualization.DataView(dataSpain1);
       viewSpain1.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

        var optionsSpain1 = {'width':600,
        'height':450,
        legend:{position:"none"}};

        var chartSpain1 = new google.visualization.BarChart(document.getElementById('spain1'));
        chartSpain1.draw(viewSpain1, optionsSpain1);

        var dataSpain2 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 0, "#89D1F3"],
          ["Local", 1547, "#009EE5"],
          ["Open Platform", 68, "#094FA4"]
       ]);

       var viewSpain2 = new google.visualization.DataView(dataSpain2);
       viewSpain2.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsSpain2 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartSpain2 = new google.visualization.BarChart(document.getElementById("spain2"));
       chartSpain2.draw(viewSpain2, optionsSpain2);

       var dataSpain3 = google.visualization.arrayToDataTable([
         ["Country", "Services", { role: "style" } ],
         ["Global API Catalog", 0, "#89D1F3"],
         ["Local", 1110, "#009EE5"],
         ["Open Platform", 41, "#094FA4"]
       ]);

       var viewSpain3 = new google.visualization.DataView(dataSpain3);
       viewSpain3.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsSpain3 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartSpain3 = new google.visualization.BarChart(document.getElementById("spain3"));
       chartSpain3.draw(viewSpain3, optionsSpain3);

       var dataSpain4 = google.visualization.arrayToDataTable([
         ['API', 'Local', { role: "style" }],
         ['cards', 53, "#89D1F3"],
         ['customer', 43, "#009EE5"],
         ['accounts', 37, "#094FA4"],
         ['transfers', 21, "#86C82D"],
         ['investmentFunds', 21, "#89D1F3"],
         ['devices', 9, "#009EE5"],
         ['loans', 7, "#094FA4"],
         ['deposits', 6, "#86C82D"],
         ['others', 1350, "#89D1F3"]
       ]);

       var optionsSpain4 = {
            width: 600,
            height: 450,
            colors:['#89D1F3','#009EE5','#094FA4'],
            legend: {position: 'none'}};

       var chartSpain4 = new google.visualization.BarChart(document.getElementById('spain4'));
       chartSpain4.draw(dataSpain4, optionsSpain4);


         var dataSpain5 = google.visualization.arrayToDataTable([
           ["Element", "Services", { role: "style" } ],
           ["accounts", 0, "#89D1F3"],
           ["cards", 0, "#009EE5"],
           ["transactions", 0, "#094FA4"],
           ["customers", 0, "#86C82D"],
           ["others", 0, "#89D1F3"]
         ]);

         var viewSpain5 = new google.visualization.DataView(dataSpain5);
         viewSpain5.setColumns([0, 1,
                          { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                          2]);

          var optionsSpain5 = {'width':600,
          'height':450,
          legend:{position:"none"}};

          var chartSpain5 = new google.visualization.BarChart(document.getElementById('spain5'));
          chartSpain5.draw(viewSpain5, optionsSpain5);

          var dataSpain6 = google.visualization.arrayToDataTable([
            ["Country", "Services", { role: "style" } ],
            ["Global API Catalog", 0, "#89D1F3"],
            ["Local", 435, "#009EE5"],
            ["Open Platform", 7, "#094FA4"]
         ]);

         var viewSpain6 = new google.visualization.DataView(dataSpain6);
         viewSpain6.setColumns([0, 1,
                          { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                          2]);

         var optionsSpain6 = {

           width: 600,
           height: 450,
           bar: {groupWidth: "95%"},
           legend: { position: "none" },
           'titleTextStyle': {'fontSize': 20},
           'showColorCode': true
         };
         var chartSpain6 = new google.visualization.BarChart(document.getElementById("spain6"));
         chartSpain6.draw(viewSpain6, optionsSpain6);


           var dataSpain7 = google.visualization.arrayToDataTable([
             ["Country", "Services", { role: "style" } ],
             ["Global API Catalog", 0, "#89D1F3"],
             ["Local", 745, "#009EE5"],
             ["Open Platform", 41, "#094FA4"]
           ]);

           var viewSpain7 = new google.visualization.DataView(dataSpain7);
           viewSpain7.setColumns([0, 1,
                            { calc: "stringify",
                              sourceColumn: 1,
                              type: "string",
                              role: "annotation" },
                            2]);

           var optionsSpain7 = {

             width: 600,
             height: 450,
             bar: {groupWidth: "95%"},
             legend: { position: "none" },
             'titleTextStyle': {'fontSize': 20},
             'showColorCode': true
           };
           var chartSpain7 = new google.visualization.BarChart(document.getElementById("spain7"));
           chartSpain7.draw(viewSpain7, optionsSpain7);

           var dataSpain10 = google.visualization.arrayToDataTable([
                ['API', 'Local', { role: "style" }],
                ['loans', 9, "#89D1F3"],
                ['cards', 6, "#009EE5"],
                ['accounts', 6, "094FA4#"],
                ['customers', 6, "#86C82D"],
                ['investmentFunds', 3, "#89D1F3"],
                ['others', 405, "#86C82D"]
              ]);

           var optionsSpain10 = {
                width: 600,
                height: 450,
                legend: {position: 'none'}
              };

           var chartSpain10 = new google.visualization.BarChart(document.getElementById('spain10'));
           chartSpain10.draw(dataSpain10, optionsSpain10);

           var dataSpain11 = google.visualization.arrayToDataTable([
             ['API', 'Local', { role: "style" }],
             ['customers', 2, "#89D1F3"],
             ['loans', 1, "#009EE5"],
             ['others', 4, "094FA4#"]
             ]);

           var optionsSpain11 = {
                width: 600,
                height: 450,
                colors:['#89D1F3','#009EE5','#094FA4'],
                legend: {position: 'none'}};

           var chartSpain11 = new google.visualization.BarChart(document.getElementById('spain11'));
           chartSpain11.draw(dataSpain11, optionsSpain11);

           var dataSpain12 = google.visualization.arrayToDataTable([
             ['API', 'Local', { role: "style" }],
             ['notifications', 12, "#89D1F3"],
             ['payments', 8, "#009EE5"],
             ["cards", 3, "#094FA4"],
             ["accounts", 3, "#86C82D"],
             ["customers", 2, "#89D1F3"],
             ["loans", 2, "#009EE5"],
             ['others', 38, "#094FA4"]
             ]);

           var optionsSpain12 = {
                width: 600,
                height: 450,
                colors:['#89D1F3','#009EE5','#094FA4'],
                legend: {position: 'none'}};

           var chartSpain12 = new google.visualization.BarChart(document.getElementById('spain12'));
           chartSpain12.draw(dataSpain12, optionsSpain12);

           var dataSpainConsumptions1 = google.visualization.arrayToDataTable([
            ["APP", "Invocations", { role: "style" } ],
            ["BBVA Net", 287.9, "#89D1F3"],
            ["Mobile Android", 146.5, "#009EE5"],
            ["Branches", 57.4, "#094FA4"],
            ["Mobile iOS", 51.6, "#86C82D"],
            ["APX", 49.9, "#89D1F3"],
            ["Mobile multiplatform", 24.4, "#009EE5"],
            ["Public Consumption", 19.1, "#094FA4"],
            ["ATM", 9.6, "#86C82D"],
            ["Branches (lite access)", 6.5, "#89D1F3"],
            ["BBVA Wallet Android", 5.3, "#009EE5"]
          ]);

          var viewSpainConsumptions1 = new google.visualization.DataView(dataSpainConsumptions1);
          viewSpainConsumptions1.setColumns([0, 1,
                           { calc: "stringify",
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation" },
                           2]);

          var optionsSpainConsumptions1 = {
            width: 600,
            height: 450,
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
            'titleTextStyle': {'fontSize': 20},
            hAxis: {slantedText: true, slantedTextAngle: 45},
            chartArea: {bottom:125},
            'showColorCode': true
          };
          var chartSpainConsumptions1 = new google.visualization.ColumnChart(document.getElementById("spainConsumptions1"));
          chartSpainConsumptions1.draw(viewSpainConsumptions1, optionsSpainConsumptions1);

          var dataSpainConsumptions2 = google.visualization.arrayToDataTable([
           ["Services", "Invocations", { role: "style" } ],
           ["grantingTicket", 82.3, "#89D1F3"],
           ["getGlobalPosition", 57.3, "#009EE5"],
           ["getContextualData", 51.4, "#094FA4"],
           ["send_CRM_event", 47.7, "#86C82D"],
           ["getMovementsByCategory", 39.3, "#89D1F3"],
           ["getClientNotification", 38.5, "#009EE5"],
           ["getLoanLimit", 37.5, "#094FA4"],
           ["getAccountMovements", 33.3, "#86C82D"],
           ["listClientContract", 22.4, "#89D1F3"],
           ["getVirtualTicket", 17.4, "#009EE5"]
          ]);

          var viewSpainConsumptions2 = new google.visualization.DataView(dataSpainConsumptions2);
          viewSpainConsumptions2.setColumns([0, 1,
                          { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                          2]);

          var optionsSpainConsumptions2 = {
           width: 600,
           height: 450,
           bar: {groupWidth: "95%"},
           legend: { position: "none" },
           hAxis: {slantedText: true, slantedTextAngle: 45},
           chartArea: {bottom:150},
           'titleTextStyle': {'fontSize': 20},
           'showColorCode': true
          };
          var chartSpainConsumptions2 = new google.visualization.ColumnChart(document.getElementById("spainConsumptions2"));
          chartSpainConsumptions2.draw(viewSpainConsumptions2, optionsSpainConsumptions2);

          var dataSpainOPConsumptions1 = google.visualization.arrayToDataTable([
           ["APP", "Invocations", { role: "style" } ],
           ["afi", 544.2, "#89D1F3"],
           ["geoblink Android", 15.5, "#009EE5"],
           ["itelligent", 11.6, "#094FA4"],
           ["others", 1.2, "#86C82D"]
         ]);

         var viewSpainOPConsumptions1 = new google.visualization.DataView(dataSpainOPConsumptions1);
         viewSpainOPConsumptions1.setColumns([0, 1,
                          { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                          2]);

         var optionsSpainOPConsumptions1 = {
           width: 600,
           height: 450,
           bar: {groupWidth: "95%"},
           legend: { position: "none" },
           'titleTextStyle': {'fontSize': 20},
           hAxis: {slantedText: true, slantedTextAngle: 45},
           chartArea: {bottom:125},
           'showColorCode': true
         };
         var chartSpainOPConsumptions1 = new google.visualization.ColumnChart(document.getElementById("spainOPConsumptions1"));
         chartSpainOPConsumptions1.draw(viewSpainOPConsumptions1, optionsSpainOPConsumptions1);

         var dataSpainOPConsumptions2 = google.visualization.arrayToDataTable([
          ["Services", "Invocations", { role: "style" } ],
          ["getGenderDistribution", 337.9, "#89D1F3"],
          ["getBasicStats", 154.9, "#009EE5"],
          ["getCustomerZipCodes", 31.9, "#094FA4"],
          ["getCardsCube", 19.4, "#86C82D"],
          ["others", 27.9, "#89D1F3"]
         ]);

         var viewSpainOPConsumptions2 = new google.visualization.DataView(dataSpainOPConsumptions2);
         viewSpainOPConsumptions2.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

         var optionsSpainOPConsumptions2 = {
          width: 600,
          height: 450,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
          hAxis: {slantedText: true, slantedTextAngle: 45},
          chartArea: {bottom:150},
          'titleTextStyle': {'fontSize': 20},
          'showColorCode': true
         };
         var chartSpainOPConsumptions2 = new google.visualization.ColumnChart(document.getElementById("spainOPConsumptions2"));
         chartSpainOPConsumptions2.draw(viewSpainOPConsumptions2, optionsSpainOPConsumptions2);

     }
     function drawChartUSA() {
       var dataUSA1 = google.visualization.arrayToDataTable([
         ["Element", "Services", { role: "style" } ],
         ["accounts", 0, "#89D1F3"],
         ["cards", 0, "#009EE5"],
         ["transactions", 0, "#094FA4"],
         ["customers", 0, "#86C82D"],
         ["others", 0, "#89D1F3"]
      ]);

       var viewUSA1 = new google.visualization.DataView(dataUSA1);
       viewUSA1.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

        var optionsUSA1 = {'width':600,
        'height':450,
        legend:{position:"none"}};

        var chartUSA1 = new google.visualization.BarChart(document.getElementById('usa1'));
        chartUSA1.draw(viewUSA1, optionsUSA1);

        var dataUSA2 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 0, "#89D1F3"],
          ["Local", 0, "#009EE5"],
          ["Open Platform", 207, "#094FA4"]
       ]);

       var viewUSA2 = new google.visualization.DataView(dataUSA2);
       viewUSA2.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsUSA2 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartUSA2 = new google.visualization.BarChart(document.getElementById("usa2"));
       chartUSA2.draw(viewUSA2, optionsUSA2);

       var dataUSA3 = google.visualization.arrayToDataTable([
         ["Country", "Services", { role: "style" } ],
         ["Global API Catalog", 0, "#89D1F3"],
         ["Local", 0, "#009EE5"],
         ["Open Platform", 0, "#094FA4"]
       ]);

       var viewUSA3 = new google.visualization.DataView(dataUSA3);
       viewUSA3.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsUSA3 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartUSA3 = new google.visualization.BarChart(document.getElementById("usa3"));
       chartUSA3.draw(viewUSA3, optionsUSA3);

       var dataUSA4 = google.visualization.arrayToDataTable([
         ['API', 'Local', { role: "style" }],
         ['payments', 199, "#89D1F3"],
         ['cards', 103, "#009EE5"],
         ['customers', 72,"#094FA4"],
         ['loans', 60, "#86C82D"],
         ['accounts', 35, "#89D1F3"],
         ['others', 678, "#009EE5"]
       ]);

       var optionsUSA4 = {
            width: 600,
            height: 450,
            legend: {position: 'none'}};

       var chartUSA4 = new google.visualization.BarChart(document.getElementById('usa4'));
       chartUSA4.draw(dataUSA4, optionsUSA4);

       var dataUSA5 = google.visualization.arrayToDataTable([
         ["Element", "Services", { role: "style" } ],
         ["accounts", 4, "#89D1F3"],
         ["cards", 4, "#009EE5"]
       ]);

       var viewUSA5 = new google.visualization.DataView(dataUSA5);
       viewUSA5.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

        var optionsUSA5 = {'width':600,
        'height':450,
        legend:{position:"none"}};

        var chartUSA5 = new google.visualization.BarChart(document.getElementById('usa5'));
        chartUSA5.draw(viewUSA5, optionsUSA5);

        var dataUSA6 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 8, "#89D1F3"],
          ["Local", 0, "#009EE5"],
          ["Open Platform", 7, "#094FA4"]
       ]);

       var viewUSA6 = new google.visualization.DataView(dataUSA6);
       viewUSA6.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsUSA6 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartUSA6 = new google.visualization.BarChart(document.getElementById("usa6"));
       chartUSA6.draw(viewUSA6, optionsUSA6);

       var dataUSA7 = google.visualization.arrayToDataTable([
         ["Country", "Services", { role: "style" } ],
         ["Global API Catalog", 0, "#89D1F3"],
         ["Local", 0, "#009EE5"],
         ["Open Platform", 0, "#094FA4"]
       ]);

       var viewUSA7 = new google.visualization.DataView(dataUSA7);
       viewUSA7.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsUSA7 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartUSA7 = new google.visualization.BarChart(document.getElementById("usa7"));
       chartUSA7.draw(viewUSA7, optionsUSA7);

       var dataUSA10 = google.visualization.arrayToDataTable([
            ['API', 'Local', { role: "style" }],
            ["payments", 178, "#89D1F3"],
            ["loans", 78, "#009EE5"],
            ["cards", 63, "#094FA4"],
            ["customers", 51, "#86C82D"],
            ['accounts', 35, "#89D1F3"],
            ["others", 699, "#009EE5"]
          ]);

        var viewUSA10 = new google.visualization.DataView(dataUSA10);
        viewUSA10.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

       var optionsUSA10 = {
            width: 600,
            height: 450,
            legend: {position: 'none'}
          };

       var chartUSA10 = new google.visualization.BarChart(document.getElementById('usa10'));
       chartUSA10.draw(dataUSA10, optionsUSA10);


       var dataUSA13 = google.visualization.arrayToDataTable([
            ['API', 'Local', { role: "style" }],
            ["others", 7, "#094FA4"]

          ]);
       var viewUSA13 = new google.visualization.DataView(dataUSA13);
       viewUSA13.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);
       var optionsUSA13 = {
            width: 600,
            height: 450,
            legend: {position: 'none'}
          };

       var chartUSA13 = new google.visualization.BarChart(document.getElementById('usa13'));
       chartUSA13.draw(dataUSA13, optionsUSA13);


       var dataUSA14 = google.visualization.arrayToDataTable([
            ['API', 'Local', { role: "style" }],
            ['accounts', 51, "#89D1F3"],
            ["cards", 25, "#009EE5"],
            ["customers", 18, "#094FA4"],
            ["payments", 17, "#86C82D"],
            ["others", 96, "#89D1F3"],
          ]);
       var viewUSA14 = new google.visualization.DataView(dataUSA14);
       viewUSA14.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);
       var optionsUSA14 = {
            width: 600,
            height: 450,
            legend: {position: 'none'}
          };

       var chartUSA14 = new google.visualization.BarChart(document.getElementById('usa14'));
       chartUSA14.draw(dataUSA14, optionsUSA14);

       var dataUsaConsumptions1 = google.visualization.arrayToDataTable([
        ["APP", "Invocations", { role: "style" } ],
        ["Simple", 4.5, "#89D1F3"],
        ["Dwolla", 0.05, "#009EE5"],
        ["Idology", 0.01, "#094FA4"],
        ["others", 0.001, "#86C82D"]
       ]);

       var viewUsaConsumptions1 = new google.visualization.DataView(dataUsaConsumptions1);
       viewUsaConsumptions1.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

       var optionsUsaConsumptions1 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        hAxis: {slantedText: true, slantedTextAngle: 45},
        chartArea: {bottom:150},
        'titleTextStyle': {'fontSize': 20},
        'showColorCode': true
       };
       var chartUsaConsumptions1 = new google.visualization.ColumnChart(document.getElementById("usaConsumptions1"));
       chartUsaConsumptions1.draw(viewUsaConsumptions1, optionsUsaConsumptions1);

       var dataUsaConsumptions2 = google.visualization.arrayToDataTable([
        ["Services", "Invocations", { role: "style" } ],
        ["getAccounts", 1.3, "#89D1F3"],
        ["postVerifyCustomerIdentity", 0.3, "#009EE5"],
        ["postCustomerRisk", 0.3, "#094FA4"],
        ["getCards", 0.2, "#86C82D"],
        ["postCustomer", 0.2, "#89D1F3"],
        ["getFiles", 0.2, "#009EE5"],
        ["postCard", 0.1, "#094FA4"]
       ]);

       var viewUsaConsumptions2 = new google.visualization.DataView(dataUsaConsumptions2);
       viewUsaConsumptions2.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

       var optionsUsaConsumptions2 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        hAxis: {slantedText: true, slantedTextAngle: 45},
        chartArea: {bottom:150},
        'titleTextStyle': {'fontSize': 20},
        'showColorCode': true
       };
       var chartUsaConsumptions2 = new google.visualization.ColumnChart(document.getElementById("usaConsumptions2"));
       chartUsaConsumptions2.draw(viewUsaConsumptions2, optionsUsaConsumptions2);

     }
     function drawChartVenezuela() {
       var dataVenezuela1 = google.visualization.arrayToDataTable([
         ["Element", "Services", { role: "style" } ],
         ["accounts", 0, "#89D1F3"],
         ["cards", 0, "#009EE5"],
         ["transactions", 0, "#094FA4"],
         ["customers", 0, "#86C82D"],
         ["others", 0, "#89D1F3"]
      ]);

       var viewVenezuela1 = new google.visualization.DataView(dataVenezuela1);
       viewVenezuela1.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

        var optionsVenezuela1 = {'width':600,
        'height':450,
        legend:{position:"none"}};

        var chartVenezuela1 = new google.visualization.BarChart(document.getElementById('venezuela1'));
        chartVenezuela1.draw(viewVenezuela1, optionsVenezuela1);

        var dataVenezuela2 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 0, "#89D1F3"],
          ["Local", 0, "#009EE5"],
          ["Open Platform", 0, "#094FA4"]
       ]);

       var viewVenezuela2 = new google.visualization.DataView(dataVenezuela2);
       viewVenezuela2.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsVenezuela2 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartVenezuela2 = new google.visualization.BarChart(document.getElementById("venezuela2"));
       chartVenezuela2.draw(viewVenezuela2, optionsVenezuela2);

       var dataVenezuela3 = google.visualization.arrayToDataTable([
         ["Country", "Services", { role: "style" } ],
         ["Global API Catalog", 0, "#89D1F3"],
         ["Local", 4, "#009EE5"],
         ["Open Platform", 0, "#094FA4"]
       ]);

       var viewVenezuela3 = new google.visualization.DataView(dataVenezuela3);
       viewVenezuela3.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsVenezuela3 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartVenezuela3 = new google.visualization.BarChart(document.getElementById("venezuela3"));
       chartVenezuela3.draw(viewVenezuela3, optionsVenezuela3);

       var dataVenezuela4 = google.visualization.arrayToDataTable([
         ['API', 'Global Catalog', 'Local', 'Open PLatform'],
         ['accounts', 0, 0, 0]
          ]);

       var optionsVenezuela4 = {
            width: 600,
            height: 450,
            colors:['#89D1F3','#009EE5','#094FA4'],
            legend: {position: 'none'}};

       var chartVenezuela4 = new google.charts.Bar(document.getElementById('venezuela4'));
       chartVenezuela4.draw(dataVenezuela4, google.charts.Bar.convertOptions(optionsVenezuela4));

       var dataVenezuela5 = google.visualization.arrayToDataTable([
         ["Element", "Services", { role: "style" } ],
         ["cards", 1, "#89D1F3"]
       ]);

       var viewVenezuela5 = new google.visualization.DataView(dataVenezuela5);
       viewVenezuela5.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

        var optionsVenezuela5 = {'width':600,
        'height':450,
        legend:{position:"none"}};

        var chartVenezuela5 = new google.visualization.BarChart(document.getElementById('venezuela5'));
        chartVenezuela5.draw(viewVenezuela5, optionsVenezuela5);

        var dataVenezuela6 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 1, "#89D1F3"],
          ["Local", 82, "#009EE5"],
          ["Open Platform", 0, "#094FA4"]
       ]);

       var viewVenezuela6 = new google.visualization.DataView(dataVenezuela6);
       viewVenezuela6.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsVenezuela6 = {
         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartVenezuela6 = new google.visualization.BarChart(document.getElementById("venezuela6"));
       chartVenezuela6.draw(viewVenezuela6, optionsVenezuela6);

       var dataVenezuela7 = google.visualization.arrayToDataTable([
         ["Country", "Services", { role: "style" } ],
         ["Global API Catalog", 0, "#89D1F3"],
         ["Local", 1, "#009EE5"],
         ["Open Platform", 0, "#094FA4"]
       ]);

       var viewVenezuela7 = new google.visualization.DataView(dataVenezuela7);
       viewVenezuela7.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsVenezuela7 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartVenezuela7 = new google.visualization.BarChart(document.getElementById("venezuela7"));
       chartVenezuela7.draw(viewVenezuela7, optionsVenezuela7);

       var dataVenezuela10 = google.visualization.arrayToDataTable([
            ['API', 'Local', { role: "style" }],
            ['accounts', 10, "#89D1F3"],
            ['cards', 2, "#009EE5"],
            ['others', 70, "#094FA4"],
          ]);

       var optionsVenezuela10 = {
            width: 600,
            height: 450,
            legend: {position: 'none'}
          };

       var chartVenezuela10 = new google.visualization.BarChart(document.getElementById('venezuela10'));
       chartVenezuela10.draw(dataVenezuela10, optionsVenezuela10);


        var dataVenezuelaConsumptions1 = google.visualization.arrayToDataTable([
         ["APP", "Invocations", { role: "style" } ],
         ["Internet(personal)", 38.6, "#89D1F3"],
         ["Mobile", 15.3, "#009EE5"],
         ["Branches", 5.4, "#094FA4"],
         ["Others", 5.2, "#86C82D"]
       ]);

       var viewVenezuelaConsumptions1 = new google.visualization.DataView(dataVenezuelaConsumptions1);
       viewVenezuelaConsumptions1.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsVenezuelaConsumptions1 = {
         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         hAxis: {slantedText: true, slantedTextAngle: 45},
         chartArea: {bottom:125},
         'showColorCode': true
       };
       var chartVenezuelaConsumptions1 = new google.visualization.ColumnChart(document.getElementById("venezuelaConsumptions1"));
       chartVenezuelaConsumptions1.draw(viewVenezuelaConsumptions1, optionsVenezuelaConsumptions1);

       var dataVenezuelaConsumptions2 = google.visualization.arrayToDataTable([
        ["Services", "Invocations", { role: "style" } ],
        ["GET customers", 8.6, "#89D1F3"],
        ["GET products", 8.5, "#009EE5"],
        ["GET executive", 5.3, "#094FA4"],
        ["POST createTicket", 3.3, "#86C82D"],
        ["GET accountMovements", 3.3, "#89D1F3"],
        ["GET holdings", 3.2, "#009EE5"],
        ["GET customerOffer", 3, "#094FA4"],
        ["GET accountBalance", 2.4, "#86C82D"],
        ["GET isActive", 1.9, "#89D1F3"],
        ["GET proactiveMessages", 1.6, "#009EE5"]
       ]);

       var viewVenezuelaConsumptions2 = new google.visualization.DataView(dataVenezuelaConsumptions2);
       viewVenezuelaConsumptions2.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

       var optionsVenezuelaConsumptions2 = {
        width: 600,
        height: 450,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        hAxis: {slantedText: true, slantedTextAngle: 45},
        chartArea: {bottom:150},
        'titleTextStyle': {'fontSize': 20},
        'showColorCode': true
       };
       var chartVenezuelaConsumptions2 = new google.visualization.ColumnChart(document.getElementById("venezuelaConsumptions2"));
       chartVenezuelaConsumptions2.draw(viewVenezuelaConsumptions2, optionsVenezuelaConsumptions2);
     }
     function drawChartUruguay() {

           var dataUruguay1 = google.visualization.arrayToDataTable([
             ["Element", "Services", { role: "style" } ],
             ["accounts", 0, "#89D1F3"],
             ["cards", 0, "#009EE5"],
             ["transactions", 0, "#094FA4"],
             ["customers", 0, "#86C82D"],
             ["others", 0, "#89D1F3"]
          ]);

           var viewUruguay1 = new google.visualization.DataView(dataUruguay1);
           viewUruguay1.setColumns([0, 1,
                            { calc: "stringify",
                              sourceColumn: 1,
                              type: "string",
                              role: "annotation" },
                            2]);

            var optionsUruguay1 = {'width':600,
            'height':450,
            legend:{position:"none"}};

            var chartUruguay1 = new google.visualization.BarChart(document.getElementById('uruguay1'));
            chartUruguay1.draw(viewUruguay1, optionsUruguay1);

            var dataUruguay2 = google.visualization.arrayToDataTable([
              ["Country", "Services", { role: "style" } ],
              ["Global API Catalog", 0, "#89D1F3"],
              ["Local", 70, "#009EE5"],
              ["Open Platform", 0, "#094FA4"]
           ]);

           var viewUruguay2 = new google.visualization.DataView(dataUruguay2);
           viewUruguay2.setColumns([0, 1,
                            { calc: "stringify",
                              sourceColumn: 1,
                              type: "string",
                              role: "annotation" },
                            2]);

           var optionsUruguay2 = {

             width: 600,
             height: 450,
             bar: {groupWidth: "95%"},
             legend: { position: "none" },
             'titleTextStyle': {'fontSize': 20},
             'showColorCode': true
           };
           var chartUruguay2 = new google.visualization.BarChart(document.getElementById("uruguay2"));
           chartUruguay2.draw(viewUruguay2, optionsUruguay2);

           var dataUruguay3 = google.visualization.arrayToDataTable([
             ["Country", "Services", { role: "style" } ],
             ["Global API Catalog", 0, "#89D1F3"],
             ["Local", 0, "#009EE5"],
             ["Open Platform", 0, "#094FA4"]
           ]);

           var viewUruguay3 = new google.visualization.DataView(dataUruguay3);
           viewUruguay3.setColumns([0, 1,
                            { calc: "stringify",
                              sourceColumn: 1,
                              type: "string",
                              role: "annotation" },
                            2]);

           var optionsUruguay3 = {

             width: 600,
             height: 450,
             bar: {groupWidth: "95%"},
             legend: { position: "none" },
             'titleTextStyle': {'fontSize': 20},
             'showColorCode': true
           };
           var chartUruguay3 = new google.visualization.BarChart(document.getElementById("uruguay3"));
           chartUruguay3.draw(viewUruguay3, optionsUruguay3);

           var dataUruguay4 = google.visualization.arrayToDataTable([
             ["API", "Local", { role: "style" } ],
             ['customers', 15, "#89D1F3"],
             ['cards', 10, "#009EE5"],
             ['accounts', 9, "#094FA4"],
             ['loans', 2, "#86C82D"],
             ['others', 34, "#89D1F3"]
              ]);
           var viewUruguay4 = new google.visualization.DataView(dataUruguay4);
           viewUruguay4.setColumns([0, 1,
                           { calc: "stringify",
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation" },
                           2]);
           var optionsUruguay4 = {
                width: 600,
                height: 450,
                legend: {position: 'none'},
                'titleTextStyle': {'fontSize': 20},
                'showColorCode': true
              };

           var chartUruguay4 = new google.visualization.BarChart(document.getElementById('uruguay4'));
           chartUruguay4.draw(viewUruguay4, optionsUruguay4);

           var dataUruguay5 = google.visualization.arrayToDataTable([
             ["Element", "Services", { role: "style" } ],
             ["accounts", 0, "#89D1F3"],
             ["cards", 0, "#009EE5"],
             ["transactions", 0, "#094FA4"],
             ["customers", 0, "#86C82D"],
             ["others", 0, "#89D1F3"]
          ]);

           var viewUruguay5 = new google.visualization.DataView(dataUruguay5);
           viewUruguay5.setColumns([0, 1,
                            { calc: "stringify",
                              sourceColumn: 1,
                              type: "string",
                              role: "annotation" },
                            2]);

            var optionsUruguay5 = {'width':600,
            'height':450,
            legend:{position:"none"}};

            var chartUruguay5 = new google.visualization.BarChart(document.getElementById('uruguay5'));
            chartUruguay5.draw(viewUruguay5, optionsUruguay5);

            var dataUruguay6 = google.visualization.arrayToDataTable([
              ["Country", "Services", { role: "style" } ],
              ["Global API Catalog", 0, "#89D1F3"],
              ["Local", 13, "#009EE5"],
              ["Open Platform", 0, "#094FA4"]
           ]);

           var viewUruguay6 = new google.visualization.DataView(dataUruguay6);
           viewUruguay6.setColumns([0, 1,
                            { calc: "stringify",
                              sourceColumn: 1,
                              type: "string",
                              role: "annotation" },
                            2]);

           var optionsUruguay6 = {

             width: 600,
             height: 450,
             bar: {groupWidth: "95%"},
             legend: { position: "none" },
             'titleTextStyle': {'fontSize': 20},
             'showColorCode': true
           };
           var chartUruguay6 = new google.visualization.BarChart(document.getElementById("uruguay6"));
           chartUruguay6.draw(viewUruguay6, optionsUruguay6);

           var dataUruguay7 = google.visualization.arrayToDataTable([
             ["Country", "Services", { role: "style" } ],
             ["Global API Catalog", 0, "#89D1F3"],
             ["Local", 0, "#009EE5"],
             ["Open Platform", 0, "#094FA4"]
           ]);

           var viewUruguay7 = new google.visualization.DataView(dataUruguay7);
           viewUruguay7.setColumns([0, 1,
                            { calc: "stringify",
                              sourceColumn: 1,
                              type: "string",
                              role: "annotation" },
                            2]);

           var optionsUruguay7 = {

             width: 600,
             height: 450,
             bar: {groupWidth: "95%"},
             legend: { position: "none" },
             'titleTextStyle': {'fontSize': 20},
             'showColorCode': true
           };
           var chartUruguay7 = new google.visualization.BarChart(document.getElementById("uruguay7"));
           chartUruguay7.draw(viewUruguay7, optionsUruguay7);

           var dataUruguay10 = google.visualization.arrayToDataTable([
                ['API', 'Local', { role: "style" }],
                ['cards', 1, "#89D1F3"],
              ]);

           var optionsUruguay10 = {
                width: 600,
                height: 450,
                legend: {position: 'none'}
              };

           var chartUruguay10 = new google.visualization.BarChart(document.getElementById('uruguay10'));
           chartUruguay10.draw(dataUruguay10, optionsUruguay10);


                  var dataUruguayConsumptions1 = google.visualization.arrayToDataTable([
                   ["APP", "Invocations", { role: "style" } ],
                   ["Internet(personal)", 38.6, "#89D1F3"],
                   ["Mobile", 15.3, "#009EE5"],
                   ["Branches", 5.4, "#094FA4"],
                   ["Others", 5.2, "#86C82D"]
                 ]);

                 var viewUruguayConsumptions1 = new google.visualization.DataView(dataUruguayConsumptions1);
                 viewUruguayConsumptions1.setColumns([0, 1,
                                  { calc: "stringify",
                                    sourceColumn: 1,
                                    type: "string",
                                    role: "annotation" },
                                  2]);

                 var optionsUruguayConsumptions1 = {
                   width: 600,
                   height: 450,
                   bar: {groupWidth: "95%"},
                   legend: { position: "none" },
                   'titleTextStyle': {'fontSize': 20},
                   hAxis: {slantedText: true, slantedTextAngle: 45},
                   chartArea: {bottom:125},
                   'showColorCode': true
                 };
                 var chartUruguayConsumptions1 = new google.visualization.ColumnChart(document.getElementById("uruguayConsumptions1"));
                 chartUruguayConsumptions1.draw(viewUruguayConsumptions1, optionsUruguayConsumptions1);

                 var dataUruguayConsumptions2 = google.visualization.arrayToDataTable([
                  ["Services", "Invocations", { role: "style" } ],
                  ["GET customers", 8.6, "#89D1F3"],
                  ["GET products", 8.5, "#009EE5"],
                  ["GET executive", 5.3, "#094FA4"],
                  ["POST createTicket", 3.3, "#86C82D"],
                  ["GET accountMovements", 3.3, "#89D1F3"],
                  ["GET holdings", 3.2, "#009EE5"],
                  ["GET customerOffer", 3, "#094FA4"],
                  ["GET accountBalance", 2.4, "#86C82D"],
                  ["GET isActive", 1.9, "#89D1F3"],
                  ["GET proactiveMessages", 1.6, "#009EE5"]
                 ]);

                 var viewUruguayConsumptions2 = new google.visualization.DataView(dataUruguayConsumptions2);
                 viewUruguayConsumptions2.setColumns([0, 1,
                                 { calc: "stringify",
                                   sourceColumn: 1,
                                   type: "string",
                                   role: "annotation" },
                                 2]);

                 var optionsUruguayConsumptions2 = {
                  width: 600,
                  height: 450,
                  bar: {groupWidth: "95%"},
                  legend: { position: "none" },
                  hAxis: {slantedText: true, slantedTextAngle: 45},
                  chartArea: {bottom:150},
                  'titleTextStyle': {'fontSize': 20},
                  'showColorCode': true
                 };
                 var chartUruguayConsumptions2 = new google.visualization.ColumnChart(document.getElementById("uruguayConsumptions2"));
                 chartUruguayConsumptions2.draw(viewUruguayConsumptions2, optionsUruguayConsumptions2);
     }
     function drawChartParaguay() {
       var dataParaguay1 = google.visualization.arrayToDataTable([
         ["Element", "Services", { role: "style" } ],
         ["accounts", 0, "#89D1F3"],
         ["cards", 0, "#009EE5"],
         ["transactions", 0, "#094FA4"],
         ["customers", 0, "#86C82D"],
         ["others", 0, "#89D1F3"]
      ]);

       var viewParaguay1 = new google.visualization.DataView(dataParaguay1);
       viewParaguay1.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

        var optionsParaguay1 = {'width':600,
        'height':450,
        legend:{position:"none"}};

        var chartParaguay1 = new google.visualization.BarChart(document.getElementById('paraguay1'));
        chartParaguay1.draw(viewParaguay1, optionsParaguay1);

        var dataParaguay2 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 0, "#89D1F3"],
          ["Local", 266, "#009EE5"],
          ["Open Platform", 0, "#094FA4"]
       ]);

       var viewParaguay2 = new google.visualization.DataView(dataParaguay2);
       viewParaguay2.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsParaguay2 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartParaguay2 = new google.visualization.BarChart(document.getElementById("paraguay2"));
       chartParaguay2.draw(viewParaguay2, optionsParaguay2);

       var dataParaguay3 = google.visualization.arrayToDataTable([
         ["Country", "Services", { role: "style" } ],
         ["Global API Catalog", 0, "#89D1F3"],
         ["Local", 0, "#009EE5"],
         ["Open Platform", 0, "#094FA4"]
       ]);

       var viewParaguay3 = new google.visualization.DataView(dataParaguay3);
       viewParaguay3.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsParaguay3 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartParaguay3 = new google.visualization.BarChart(document.getElementById("paraguay3"));
       chartParaguay3.draw(viewParaguay3, optionsParaguay3);

       var dataParaguay4 = google.visualization.arrayToDataTable([
         ['API', 'Global Catalog', 'Local', 'Open PLatform'],
         ['accounts', 0, 0, 0]
          ]);

       var optionsParaguay4 = {
            width: 600,
            height: 450,
            colors:['#89D1F3','#009EE5','#094FA4'],
            legend: {position: 'none'}};

       var chartParaguay4 = new google.charts.Bar(document.getElementById('paraguay4'));
       chartParaguay4.draw(dataParaguay4, google.charts.Bar.convertOptions(optionsParaguay4));

        var dataParaguay5 = google.visualization.arrayToDataTable([
          ["Element", "Services", { role: "style" } ],
          ["accounts", 0, "#89D1F3"],
          ["cards", 0, "#009EE5"],
          ["transactions", 0, "#094FA4"],
          ["customers", 0, "#86C82D"],
          ["others", 0, "#89D1F3"]
       ]);

        var viewParaguay5 = new google.visualization.DataView(dataParaguay5);
        viewParaguay5.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

         var optionsParaguay5 = {'width':600,
         'height':450,
         legend:{position:"none"}};

         var chartParaguay5 = new google.visualization.BarChart(document.getElementById('paraguay5'));
         chartParaguay5.draw(viewParaguay5, optionsParaguay5);

         var dataParaguay6 = google.visualization.arrayToDataTable([
           ["Country", "Services", { role: "style" } ],
           ["Global API Catalog", 0, "#89D1F3"],
           ["Local", 0, "#86C82D"],
           ["Open Platform", 0, "#89D1F3"]
        ]);

        var viewParaguay6 = new google.visualization.DataView(dataParaguay6);
        viewParaguay6.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

          var optionsParaguay6 = {

            width: 600,
            height: 450,
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
            'titleTextStyle': {'fontSize': 20},
            'showColorCode': true
          };
          var chartParaguay6 = new google.visualization.BarChart(document.getElementById("paraguay6"));
          chartParaguay6.draw(viewParaguay6, optionsParaguay6);

          var dataParaguay7 = google.visualization.arrayToDataTable([
            ["Country", "Services", { role: "style" } ],
            ["Global API Catalog", 0, "#89D1F3"],
            ["Local", 0, "#009EE5"],
            ["Open Platform", 0, "#094FA4"]
          ]);

          var viewParaguay7 = new google.visualization.DataView(dataParaguay7);
          viewParaguay7.setColumns([0, 1,
                           { calc: "stringify",
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation" },
                           2]);

          var optionsParaguay7 = {

            width: 600,
            height: 450,
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
            'titleTextStyle': {'fontSize': 20},
            'showColorCode': true
          };
          var chartParaguay7 = new google.visualization.BarChart(document.getElementById("paraguay7"));
          chartParaguay7.draw(viewParaguay7, optionsParaguay7);

          var dataParaguay10 = google.visualization.arrayToDataTable([
               ['API', 'Local', { role: "style" }],
               ['cards', 1, "#89D1F3"],
             ]);

          var optionsParaguay10 = {
               width: 600,
               height: 450,
               legend: {position: 'none'}
             };

          var chartParaguay10 = new google.visualization.BarChart(document.getElementById('paraguay10'));
          chartParaguay10.draw(dataParaguay10, optionsParaguay10);


                 var dataParaguayConsumptions1 = google.visualization.arrayToDataTable([
                  ["APP", "Invocations", { role: "style" } ],
                  ["Web", 2.5, "#89D1F3"],
                  ["Mobile", 1.7, "#009EE5"],
                  ["Others", 0.6, "#094FA4"]
                ]);

                var viewParaguayConsumptions1 = new google.visualization.DataView(dataParaguayConsumptions1);
                viewParaguayConsumptions1.setColumns([0, 1,
                                 { calc: "stringify",
                                   sourceColumn: 1,
                                   type: "string",
                                   role: "annotation" },
                                 2]);

                var optionsParaguayConsumptions1 = {
                  width: 600,
                  height: 450,
                  bar: {groupWidth: "95%"},
                  legend: { position: "none" },
                  'titleTextStyle': {'fontSize': 20},
                  hAxis: {slantedText: true, slantedTextAngle: 45},
                  chartArea: {bottom:125},
                  'showColorCode': true
                };
                var chartParaguayConsumptions1 = new google.visualization.ColumnChart(document.getElementById("paraguayConsumptions1"));
                chartParaguayConsumptions1.draw(viewParaguayConsumptions1, optionsParaguayConsumptions1);

                var dataParaguayConsumptions2 = google.visualization.arrayToDataTable([
                 ["Services", "Invocations", { role: "style" } ],
                 ["getGlobalPosition", 738.2, "#89D1F3"],
                 ["login", 444.9, "#009EE5"],
                 ["checkPreApproveAvailable", 431.7, "#094FA4"],
                 ["logout", 326.5, "#86C82D"],
                 ["getAuthorizationHistory", 244.2, "#89D1F3"],
                 ["getAccountMovements", 238.9, "#009EE5"],
                 ["getAccountDetail", 206.5, "#094FA4"],
                 ["getAddres", 181.2, "#86C82D"],
                 ["getFavOperations", 150.0, "#89D1F3"],
                 ["getConfigPopUp", 145.1, "#009EE5"]
                ]);

                var viewParaguayConsumptions2 = new google.visualization.DataView(dataParaguayConsumptions2);
                viewParaguayConsumptions2.setColumns([0, 1,
                                { calc: "stringify",
                                  sourceColumn: 1,
                                  type: "string",
                                  role: "annotation" },
                                2]);

                var optionsParaguayConsumptions2 = {
                 width: 600,
                 height: 450,
                 bar: {groupWidth: "95%"},
                 legend: { position: "none" },
                 hAxis: {slantedText: true, slantedTextAngle: 45},
                 chartArea: {bottom:150},
                 'titleTextStyle': {'fontSize': 20},
                 'showColorCode': true
                };
                var chartParaguayConsumptions2 = new google.visualization.ColumnChart(document.getElementById("paraguayConsumptions2"));
                chartParaguayConsumptions2.draw(viewParaguayConsumptions2, optionsParaguayConsumptions2);
     }
     function drawChartTurkey() {
       var dataTurkey1 = google.visualization.arrayToDataTable([
         ["Element", "Services", { role: "style" } ],
         ["accounts", 0, "#89D1F3"],
         ["cards", 0, "#009EE5"],
         ["transactions", 0, "#094FA4"],
         ["customers", 0, "#86C82D"],
         ["others", 0, "#89D1F3"]
      ]);

       var viewTurkey1 = new google.visualization.DataView(dataTurkey1);
       viewTurkey1.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

        var optionsTurkey1 = {'width':600,
        'height':450,
        legend:{position:"none"}};

        var chartTurkey1 = new google.visualization.BarChart(document.getElementById('turkey1'));
        chartTurkey1.draw(viewTurkey1, optionsTurkey1);

        var dataTurkey2 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 0, "#89D1F3"],
          ["Local", 13389, "#009EE5"],
          ["Open Platform", 0, "#89D1F3"]
       ]);

       var viewTurkey2 = new google.visualization.DataView(dataTurkey2);
       viewTurkey2.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsTurkey2 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartTurkey2 = new google.visualization.BarChart(document.getElementById("turkey2"));
       chartTurkey2.draw(viewTurkey2, optionsTurkey2);

       var dataTurkey3 = google.visualization.arrayToDataTable([
         ["Country", "Services", { role: "style" } ],
         ["Global API Catalog", 0, "#89D1F3"],
         ["Local", 0, "#86C82D"],
         ["Open Platform", 0, "#89D1F3"]
       ]);

       var viewTurkey3 = new google.visualization.DataView(dataTurkey3);
       viewTurkey3.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsTurkey3 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartTurkey3 = new google.visualization.BarChart(document.getElementById("turkey3"));
       chartTurkey3.draw(viewTurkey3, optionsTurkey3);

       var dataTurkey4 = google.visualization.arrayToDataTable([
         ['API', 'Global Catalog', 'Local', 'Open PLatform'],
         ['accounts', 0, 0, 0]
          ]);

       var optionsTurkey4 = {
            width: 600,
            height: 450,
            colors:['#89D1F3','#009EE5','#094FA4'],
            legend: {position: 'none'}};

       var chartTurkey4 = new google.charts.Bar(document.getElementById('turkey4'));
       chartTurkey4.draw(dataTurkey4, google.charts.Bar.convertOptions(optionsTurkey4));


       var dataTurkey5 = google.visualization.arrayToDataTable([
         ["Element", "Services", { role: "style" } ],
         ["transfers", 1, "#89D1F3"],
         ["others", 3, "#009EE5"]
      ]);

       var viewTurkey5 = new google.visualization.DataView(dataTurkey5);
       viewTurkey5.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

        var optionsTurkey5 = {'width':600,
        'height':450,
        legend:{position:"none"}};

        var chartTurkey5 = new google.visualization.BarChart(document.getElementById('turkey5'));
        chartTurkey5.draw(viewTurkey5, optionsTurkey5);

        var dataTurkey6 = google.visualization.arrayToDataTable([
          ["Country", "Services", { role: "style" } ],
          ["Global API Catalog", 0, "#89D1F3"],
          ["Local", 1739, "#009EE5"],
          ["Open Platform", 0, "#89D1F3"]
       ]);

       var viewTurkey6 = new google.visualization.DataView(dataTurkey6);
       viewTurkey6.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsTurkey6 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartTurkey6 = new google.visualization.BarChart(document.getElementById("turkey6"));
       chartTurkey6.draw(viewTurkey6, optionsTurkey6);

       var dataTurkey7 = google.visualization.arrayToDataTable([
         ["Country", "Services", { role: "style" } ],
         ["Global API Catalog", 0, "#89D1F3"],
         ["Local", 0, "#009EE5"],
         ["Open Platform", 0, "#094FA4"]
       ]);

       var viewTurkey7 = new google.visualization.DataView(dataTurkey7);
       viewTurkey7.setColumns([0, 1,
                        { calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation" },
                        2]);

       var optionsTurkey7 = {

         width: 600,
         height: 450,
         bar: {groupWidth: "95%"},
         legend: { position: "none" },
         'titleTextStyle': {'fontSize': 20},
         'showColorCode': true
       };
       var chartTurkey7 = new google.visualization.BarChart(document.getElementById("turkey7"));
       chartTurkey7.draw(viewTurkey7, optionsTurkey7);

       var dataTurkey10 = google.visualization.arrayToDataTable([
            ['API', 'Local', { role: "style" }],
            ['transfers', 1, "#89D1F3"],
            ["others", 2, "#009EE5"],
          ]);

       var optionsTurkey10 = {
            width: 600,
            height: 450,
            legend: {position: 'none'}
          };

       var chartTurkey10 = new google.visualization.BarChart(document.getElementById('turkey10'));
       chartTurkey10.draw(dataTurkey10, optionsTurkey10);


              var dataTurkeyConsumptions1 = google.visualization.arrayToDataTable([
               ["APP", "Invocations", { role: "style" } ],
               ["Internet(personal)", 38.6, "#89D1F3"],
               ["Mobile", 15.3, "#009EE5"],
               ["Branches", 5.4, "#094FA4"],
               ["Others", 5.2, "#86C82D"]
             ]);

             var viewTurkeyConsumptions1 = new google.visualization.DataView(dataTurkeyConsumptions1);
             viewTurkeyConsumptions1.setColumns([0, 1,
                              { calc: "stringify",
                                sourceColumn: 1,
                                type: "string",
                                role: "annotation" },
                              2]);

             var optionsTurkeyConsumptions1 = {
               width: 600,
               height: 450,
               bar: {groupWidth: "95%"},
               legend: { position: "none" },
               'titleTextStyle': {'fontSize': 20},
               hAxis: {slantedText: true, slantedTextAngle: 45},
               chartArea: {bottom:125},
               'showColorCode': true
             };
             var chartTurkeyConsumptions1 = new google.visualization.ColumnChart(document.getElementById("turkeyConsumptions1"));
             chartTurkeyConsumptions1.draw(viewTurkeyConsumptions1, optionsTurkeyConsumptions1);

             var dataTurkeyConsumptions2 = google.visualization.arrayToDataTable([
              ["Services", "Invocations", { role: "style" } ],
              ["GET customers", 8.6, "#89D1F3"],
              ["GET products", 8.5, "#009EE5"],
              ["GET executive", 5.3, "#094FA4"],
              ["POST createTicket", 3.3, "#86C82D"],
              ["GET accountMovements", 3.3, "#89D1F3"],
              ["GET holdings", 3.2, "#009EE5"],
              ["GET customerOffer", 3, "#094FA4"],
              ["GET accountBalance", 2.4, "#86C82D"],
              ["GET isActive", 1.9, "#89D1F3"],
              ["GET proactiveMessages", 1.6, "#009EE5"]
             ]);

             var viewTurkeyConsumptions2 = new google.visualization.DataView(dataTurkeyConsumptions2);
             viewTurkeyConsumptions2.setColumns([0, 1,
                             { calc: "stringify",
                               sourceColumn: 1,
                               type: "string",
                               role: "annotation" },
                             2]);

             var optionsTurkeyConsumptions2 = {
              width: 600,
              height: 450,
              bar: {groupWidth: "95%"},
              legend: { position: "none" },
              hAxis: {slantedText: true, slantedTextAngle: 45},
              chartArea: {bottom:150},
              'titleTextStyle': {'fontSize': 20},
              'showColorCode': true
             };
             var chartTurkeyConsumptions2 = new google.visualization.ColumnChart(document.getElementById("turkeyConsumptions2"));
             chartTurkeyConsumptions2.draw(viewTurkeyConsumptions2, optionsTurkeyConsumptions2);
     }
  }

  angular
    .module('passbeta')
    .controller('DashboardController', DashboardController);
})();
