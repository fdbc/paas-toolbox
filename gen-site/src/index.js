(function () {
    'use strict';
    /** @ngInject */

    loadViews();

    var element = angular.element('#login-container'),
        labelUserName = element.find('.input label[for="name"]'),
        labelPassword = element.find('.input label[for="pass"]'),
        inputUserName = element.find('.input input#name'),
        inputPassword = element.find('.input input#pass'),
        loginButton = element.find('.footer-login button'),
        helpMessage = element.find('.mail');

    function unlisten(element, event, listener) {
        element.off(event, listener);
    }

    function listen(element, event, listener) {
        element.on(event, listener);
        return function () {
            unlisten(element, event, listener);
        }
    }

    function addLabelAnimation(event) {
        switch (event.type) {
            case 'click':
                angular.element(event.target).addClass('animate');
                break;
            case 'focusout':
                if (event.type === 'focusout' && event.target.value.length === 0) {
                    angular.element(event.target).parent().find('label').removeClass('animate');
                }
                break;
            case 'focusin':
                angular.element(event.target).parent().find('label').addClass('animate');
                break;
        }
    }

    function removeLabelAnimation(event) {
        var inputs = angular.element(event.target).find('.input input');
        angular.forEach(inputs, function (input) {
            if (input.value.length === 0) {
                angular.element(input).parent().find('label').removeClass('animate');
            }
        });
    }

    function showHelpMessage(msg) {
        angular.element('.message').find('p').text(msg)
        angular.element('.message').addClass('show');
        angular.element('.message').find('p').removeClass('error');
        angular.element('.message').find('p').addClass('help');
        angular.element('.login-card').addClass('error');
    }

    function showErrorMessage(msg) {
        angular.element('.message').find('p').text(msg)
        angular.element('.message').addClass('show');
        angular.element('.message').find('p').removeClass('help');
        angular.element('.message').find('p').addClass('error');
        angular.element('.login-card').addClass('error');
    }

    function prepareBody() {
        var html = angular.element('html'),
            body = angular.element('body');
        html.addClass('ng-cloak');
    }

    function loadViews() {
        var httpRequest = new XMLHttpRequest(),
            uriRegex = /([^:\/\s]+)$/g,
            initInjector = angular.injector(["ng"]),
            $http = initInjector.get("$http"),
            $q = initInjector.get("$q"),
            promises = [],
            structure,
            views = {},
            urlBB = "/bitbucket/projects/BPC/repos/paas-toolbox/raw/gen-site/src/structure.json";

        if (window.location.hostname.includes('-dot-')) {
          // When uploads new versions to google apps and doesn't deploy it, it provides an url to
          // test the new version, it has the -dot- prefix, for testing purposes it will points to
          // develop version of structure.json
          urlBB = "/bitbucket/projects/BPC/repos/paas-toolbox/raw/gen-site/src/structure.json?at=refs%2Fheads%2Fdevelop"
        }


        httpRequest.open('GET', urlBB, true);
        httpRequest.send(null);

        httpRequest.onreadystatechange = function () {
            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                prepareBody();
                document.body.innerHTML =
                    '<paas-header></paas-header>' +
                    '<div ng-view autoscroll=\"true\"></div>';

                structure = JSON.parse(httpRequest.response);

                angular.forEach(structure['domain-groups'], function (domain) {
                    angular.forEach(domain.sections, function (section) {
                        angular.forEach(section.domains, function (repo) {
                            if (repo['repository-url'].search('commons') === -1) {
                                var deferred = $q.defer();
                                $http({
                                    method: "GET",
                                    url: repo['repository-url'] + '/raw/html/view.html?at=refs%2Fheads%2Fdevelop',
                                }).then(function (view) {
                                    deferred.resolve(views[repo['repository-url'].match(uriRegex)[0].toLowerCase() + '.html'] = view.data);
                                });
                                promises.push(deferred.promise);
                            }
                        });
                    });
                });
                $q.all(promises).then(function () {
                    angular.module("config", [])
                        .constant('Structure', views);
                    loadSite();
                }).catch(function(){
                })
            }
        };
    }

    function loadSite() {
        prepareBody();
        document.body.innerHTML =
            '<paas-header></paas-header>' +
            '<div ng-view autoscroll=\"true\"></div>';

        angular.element(document).ready(function () {
            angular.bootstrap(document, ['passbeta', 'config']);
        });
    }

    element.on('$destroy', function () {
        unlistenLabelUserName();
        unlistenLabelPassword();
        unlistenInputUserName();
        unlistenInputPassword();
        unlistenLoginContainer();
        unlistenLoginButton();
        unlistenHelpMessage();
        unlistenEnterInputUserName();
        unlistenEnterInputPassword();
    });
})()

function toggle(element, oldUnionPropertyName, oldSelectedType) {
  oldUnionPropertyName = oldUnionPropertyName ? oldUnionPropertyName : ''
  oldSelectedType = oldSelectedType ? oldSelectedType : ''
  var toggledElements = element.parentNode.getElementsByClassName('toggled')
  for (var i = 0; i < toggledElements.length; i++) {
    if(toggledElements [i] !== element){
      toggle(toggledElements[i])
    }
  }
  var selectedType = element.innerText.trim()
  var unionPropertyName = getParentProperty(element)
  if(!isARamlBuiltInType(selectedType)){
    var propertiesParent = element.parentNode.parentNode.parentNode
    var propertiesList = propertiesParent.querySelectorAll('span[class*="'+unionPropertyName+'.'+selectedType+'"]');
    if (propertiesList.length === 0) {
      propertiesList = propertiesParent.querySelectorAll('span[class*="'+oldUnionPropertyName+'.'+oldSelectedType+'"]')
    }
    var innerUnionList = []
    for (var i = 0; i < propertiesList.length; i++){
      var subElement = propertiesList[i].parentNode.parentNode
      toggleStyle(subElement, 'display', 'none', excludeElements=innerUnionList)
      if (isAUnionElement(subElement)) {
        innerUnionList = innerUnionList.concat(getExcludedDisplayNames(subElement))
      }
    }
    toggleStyle(element, 'borderStyle', 'inherit', [], 'toggled')
  }
}

function getParentProperty(element){
  var parentPropertyElements = element.parentElement.getElementsByClassName('display-name')
  return parentPropertyElements[parentPropertyElements.length - 1].innerText
}

function toggleStyle(element, styleType, styleValue, excludeElements, toggleClass, secondStyleValue){
  if (!excludeElements.find(isTypeOrSubtipe, getDisplayName(element))) {
    secondStyleValue = secondStyleValue ? secondStyleValue : ''
    if (toggleClass) {
      element.classList.toggle(toggleClass)
    }
    if (element.style[styleType] === styleValue) {
      element.style[styleType] = secondStyleValue
    } else {
      element.style[styleType] = styleValue
    }
  }
}

function isARamlBuiltInType(type) {
  const RamlBuiltInTypes = ['any', 'object', 'array', 'union', 'number', 'boolean', 'string', 'date-only', 'datetime', 'file', 'integer', 'nil']
  return RamlBuiltInTypes.includes(type)
}

function isAUnionElement(element) {
  return element.children ? element.children[0].children.length > 2 : false
}

function getExcludedDisplayNames(element){
  var displayName = getDisplayName(element) + '.'
  var types = element.getElementsByClassName('type')
  var excludedDysplayNames = []
  for(var i=0; i<types.length; i++){
    excludedDysplayNames.push(displayName + types[i].innerText)
  }
  return excludedDysplayNames
}

function getDisplayName(element){
  return element.getElementsByClassName("display-name").length > 0 ? element.getElementsByClassName("display-name")[0].innerText : ''
}

function isTypeOrSubtipe(element){
  return this.includes(element)
}
