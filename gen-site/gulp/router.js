'use strict';

const path = require('path'),
    gulp = require('gulp'),
    fs = require('fs-extra'),
    _ = require('lodash'),
    genApiCatalog = require('../../gen-api-catalog/app.js'),
    request = require('request');

var countries = ["/es-", "/mx-", "/cl-", "/pe-", "/ve-", "/co-", "/us-"];

function nameCountryMatched(repo){

  var findCountry = countries.reduce(function(res,country){

    if (repo.indexOf(country) != -1){
      res.push(country);
    }
    return res;
  },[]);

  if (findCountry.length == 1){
    return findCountry[0].substring(1,3);
  }
}


let header = " (function () {" +
                "'use strict';" +
                "function routeConfig($routeProvider) {" +
                    "$routeProvider" +
                        ".when('/', {" +
                            "templateUrl: 'app/home/home.html'," +
                            "controller: 'HomeController'," +
                            "controllerAs: 'home'," +
                            "homeTab: 'home'" +
                        "})" +
                        ".when('/learn', {" +
                            "templateUrl: 'app/learn/learn.html'," +
                            "controller: 'LearnController'," +
                            "controllerAs: 'learn'" +
                        "})" +
                        ".when('/team', {" +
                            "templateUrl: 'app/team/team.html'," +
                            "controller: 'TeamController'," +
                            "controllerAs: 'team'" +
                        "})" +
                        ".when('/dashboard', {" +
                            "templateUrl: 'app/dashboard/dashboard.html'," +
                            "controller: 'DashboardController'," +
                            "controllerAs: 'dashboard'" +
                        "})" +
                        ".when('/consumptions', {" +
                            "templateUrl: 'app/invocations/invocations.html'," +
                            "controller: 'InvocationsController'," +
                            "controllerAs: 'invocations'" +
                        "})" +
                        ".when('/apiConsumerGuide', {" +
                            "templateUrl: 'app/apiConsumer/apiConsumer.html'," +
                            "controller: 'ApiConsumerController'," +
                            "controllerAs: 'apiConsumer'" +
                        "})" +
                        ".when('/apiDeveloperGuide', {" +
                            "templateUrl: 'app/apiDeveloper/apiDeveloper.html'," +
                            "controller: 'ApiDeveloperController'," +
                            "controllerAs: 'apiDeveloper'" +
                        "})" +
                        ".when('/support', {" +
                            "templateUrl: 'app/support/support.html'," +
                            "controller: 'SupportController'," +
                            "controllerAs: 'support'" +
                        "})" +
                        ".when('/blog', {" +
                            "templateUrl: 'app/blog/blog.html'," +
                            "controller: 'BlogController'," +
                            "controllerAs: 'blog'" +
                        "})" +
                        ".when('/api/traits', {" +
                            "templateUrl: 'app/api/traits/traits.html'," +
                            "controller: 'ApiController'," +
                            "controllerAs: 'api'" +
                        "})" +
                        ".when('/api/resourceTypes', {" +
                            "templateUrl: 'app/api/resourceTypes/resourceTypes.html'," +
                            "controller: 'ApiController'," +
                            "controllerAs: 'api'" +
                        "})",
    footer = ".otherwise({" +
                "redirectTo: '/'" +
            "});" +
        "}" +
    "angular" +
        ".module('passbeta')" +
        ".config(routeConfig);" +
    "})();";

function createTagRouter(structure) {
    let url,
        uriRegex = /([^:\/\s]+)$/g,
        routes = "",
        headerTag = "(function () {" +
                    "'use strict';" +
                    "function routeConfig($routeProvider) {" +
                        "$routeProvider";

        _.forEach(structure['domain-groups'], function (domain) {
            _.forEach(domain.sections, function (section) {
                url = "/";
                _.forEach(section.domains, function (repo) {

                  url = url + domain.title.replace('-', '/');

                  if (repo['repository-url'].indexOf("GLAPI") == -1){
                    var endURL = repo['repository-url'].split("/")
                    endURL = endURL[endURL.length-1];
                    var countryRegex = /-(\w*)-/g;
                    var country = countryRegex.exec(repo['repository-url'])[1];
                    url = url + "/" + country;
                  }

                  if(nameCountryMatched(repo['repository-url']) != null && nameCountryMatched(repo['repository-url']).length > 0){
                    url = url + '/' + nameCountryMatched(repo['repository-url']) + '/' + section.name;
                  }else{
                    url = url + '/' + section.name;
                  }

                    _.forEach(repo.tag, function (tag) {
                        routes = routes.concat(".when('" + url + '/' + repo['display-name'] + '/' + tag +"', {" +
                                                    "templateUrl: '" + repo['repository-url'].match(uriRegex)[0] + ".html'," +
                                                    "controller: 'ApiController'," +
                                                    "controllerAs: 'api'," +
                                                    "resolve: {" +
                                                        "template: function (IndexService, $templateCache) {" +
                                                            "return IndexService.getTagHtml('" + repo['repository-url'] + "', '" + tag + "')" +
                                                                ".then(function (data) {" +
                                                                    "$templateCache.put('" + repo['display-name'].toLowerCase() + "-api.html', data);" +
                                                                "})" +
                                                        "}" +
                                                    "}" +
                                                "})"
                        )
                    });

                  url = "/";

                });
            });
        });
        fs.outputFileSync('./src/app/api/api.route.js', headerTag + routes + footer);
}

gulp.task('router:dist', ['catalog'], function () {
    const structure = require('../../gen-site/src/structure.json');

    header = header + ".when('/api', {" +
                          "templateUrl: 'app/api/api.html'," +
                          "controller: 'ApiController'," +
                          "controllerAs: 'api'," +
                          "resolve: {" +
                              "template: function ($http, $templateCache) {" +
                                  "return $http({" +
                                  "method: \"GET\"," +
                                  "url: '/bitbucket/projects/BPC/repos/paas-toolbox/raw/gen-site/src/structure.json'" +
                                  "}).then(function (data) {" +
                                      "localStorage.setItem('catalog', JSON.stringify(data.data['domain-groups']));" +
                                  "});" +
                              "}" +
                          "}" +
                      "})" +
                      ".when('/api/statusCodes', {" +
                          "templateUrl: 'app/api/statusCodes/statusCodes.html'," +
                          "controller: 'ApiController'," +
                          "controllerAs: 'api'," +
                          "resolve: {" +
                              "template: function ($http, $templateCache) {" +
                                  "return $http({" +
                                  "method: \"GET\"," +
                                  "url: '/bitbucket/projects/BPC/repos/paas-toolbox/raw/gen-site/src/statusCodes.json'" +
                                  "}).then(function (data) {" +
                                      "localStorage.setItem('statusCodes', JSON.stringify(data.data.statusCodes));" +
                                  "});" +
                              "}" +
                          "}" +
                      "})" +
                      ".when('/api/errors', {" +
                          "templateUrl: 'app/api/errors/errors.html'," +
                          "controller: 'ApiController'," +
                          "controllerAs: 'api'," +
                          "resolve: {" +
                              "template: function ($http, $templateCache) {" +
                                  "return $http({" +
                                      "method: \"GET\"," +
                                      "url: '/bitbucket/projects/BPC/repos/paas-toolbox/raw/gen-site/src/errors.json'" +
                                  "}).then(function (data) {" +
                                      "localStorage.setItem('errors', JSON.stringify(data.data.functionalErrors));" +
                                  "});" +
                              "}" +
                          "}" +
                      "})";

    createTagRouter(structure);

    function createRoutes() {
        let url,
            uriRegex = /([^:\/\s]+)$/g,
            routes = "";

        _.forEach(structure['domain-groups'], function (domain) {
            _.forEach(domain.sections, function (section) {
                url = "/";
                _.forEach(section.domains, function (repo) {

                  url = url + domain.title.replace('-', '/');

                  if (repo['repository-url'].indexOf("GLAPI") == -1){
                    var endURL = repo['repository-url'].split("/")
                    endURL = endURL[endURL.length-1];
                    var countryRegex = /-(\w*)-/g;
                    var country = countryRegex.exec(repo['repository-url'])[1];
                    url = url + "/" + country;
                  }

                    if(nameCountryMatched(repo['repository-url']) != null && nameCountryMatched(repo['repository-url']).length > 0){
                      url = url + '/' + nameCountryMatched(repo['repository-url']) + '/' + section.name;
                    }else{
                      url = url + '/' + section.name;
                    }

                    routes = routes.concat(".when('" + url + '/' + repo['display-name'] + "', {" +
                                                "templateUrl: '" + repo['repository-url'].match(uriRegex)[0] + ".html'," +
                                                "controller: 'ApiController'," +
                                                "controllerAs: 'api'," +
                                                "resolve: {" +
                                                    "template: function (IndexService, $templateCache) {" +
                                                        "return IndexService.getHtml('" + repo['repository-url'] + "')" +
                                                            ".then(function (data) {" +
                                                                "$templateCache.put('" + repo['display-name'].toLowerCase() + "-api.html', data);" +
                                                            "})" +
                                                    "}" +
                                                "}" +
                                            "})"
                    )

                    url = "/";

                });
            });
        });
        return routes;
    }
    return fs.outputFileSync('./src/app/index.route.js', header + createRoutes() + footer);
});

gulp.task('router', ['catalog'], function () {
    const structure = require('../../gen-site/src/structure.json');

    header = header + ".when('/api', {" +
                          "templateUrl: 'app/api/api.html'," +
                          "controller: 'ApiController'," +
                          "controllerAs: 'api'," +
                          "resolve: {" +
                              "template: function ($http, $templateCache) {" +
                                  "return $http({" +
                                      "method: \"GET\"," +
                                      "url: 'http://localhost:3000/structure.json'," +
                                  "}).then(function (data) {" +
                                      "localStorage.setItem('catalog', JSON.stringify(data.data['domain-groups']));" +
                                  "});" +
                              "}" +
                          "}" +
                      "})" +
                      ".when('/api/statusCodes', {" +
                          "templateUrl: 'app/api/statusCodes/statusCodes.html'," +
                          "controller: 'ApiController'," +
                          "controllerAs: 'api'," +
                          "resolve: {" +
                              "template: function ($http, $templateCache) {" +
                                  "return $http({" +
                                      "method: \"GET\"," +
                                      "url: 'http://localhost:3000/statusCodes.json'," +
                                  "}).then(function (data) {" +
                                      "localStorage.setItem('statusCodes', JSON.stringify(data.data.statusCodes));" +
                                  "});" +
                              "}" +
                          "}" +
                      "})" +
                      ".when('/api/errors', {" +
                          "templateUrl: 'app/api/errors/errors.html'," +
                          "controller: 'ApiController'," +
                          "controllerAs: 'api'," +
                          "resolve: {" +
                              "template: function ($http, $templateCache) {" +
                                  "return $http({" +
                                      "method: \"GET\"," +
                                      "url: 'http://localhost:3000/errors.json'," +
                                  "}).then(function (data) {" +
                                      "localStorage.setItem('errors', JSON.stringify(data.data.functionalErrors));" +
                                  "});" +
                              "}" +
                          "}" +
                      "})";

    createTagRouter(structure)

    function createRoutes() {
        let url,
            uriRegex = /([^:\/\s]+)$/g,
            routes = "";

        _.forEach(structure['domain-groups'], function (domain) {
            _.forEach(domain.sections, function (section) {
                url = "/";
                _.forEach(section.domains, function (repo) {
                  url = url + domain.title.replace('-', '/');
                  if (repo['repository-url'].indexOf("GLAPI") == -1){
                    var endURL = repo['repository-url'].split("/")
                    endURL = endURL[endURL.length-1];
                    var countryRegex = /-(\w*)-/g;
                    var country = countryRegex.exec(repo['repository-url'])[1];
                    url = url + "/" + country;
                  }
                  if(nameCountryMatched(repo['repository-url']) != null && nameCountryMatched(repo['repository-url']).length > 0){
                    url = url + '/' + nameCountryMatched(repo['repository-url']) + '/' + section.name;
                  }else{
                    url = url + '/' + section.name;
                  }
                    routes = routes.concat(".when('" + url + '/' + repo['display-name'] + "', {" +
                                                "templateUrl: 'app/api/" + repo['repository-url'].match(uriRegex)[0] + "/" + repo['repository-url'].match(uriRegex)[0] + ".html'," +
                                                "controller: 'ApiController'," +
                                                "controllerAs: 'api'" +
                                            "})"
                    )
                  url = "/";
                });
            });
        });
        return routes;
    }
    return fs.outputFileSync('./src/app/index.route.js', header + createRoutes() + footer);
});
