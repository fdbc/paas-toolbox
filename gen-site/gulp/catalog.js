'use strict';

const gulp = require('gulp'),
    genApiCatalog = require('../../gen-api-catalog/app.js')

gulp.task('catalog', function () {
    return genApiCatalog.createApiCatalog();
});

gulp.task('catalog:dist', function () {
    return genApiCatalog.createApiCatalog();
});
