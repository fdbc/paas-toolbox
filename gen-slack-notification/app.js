var https = require('https'),
    Q = require('q');

function notify(token, channel, text, username, emoji){
  var deferred = Q.defer();
  channel = encodeURIComponent(channel);
  text = encodeURIComponent(text);
  username = encodeURIComponent(username);
  emoji = encodeURIComponent(emoji);
  var path = '/api/chat.postMessage?token='+token+'&channel='+channel+'&text='+text+'&username='+username+'&icon_emoji='+emoji;
  var options = {
    hostname: 'slack.com',
    port: 443,
    path: path,
    method: 'GET'
  };
  var req = https.request(options, function(response) {
    var data = [];
    response.on('data', function(chunk) {
      data.push(chunk);
    });
    response.on('end', function(){
      deferred.resolve(JSON.parse(data.join('')));
    });
  });
  req.end();
  req.on('error', function(e) {
    deferred.reject(e);
  });
  return deferred.promise;
}

function obtainChannel(token, channelName){
  var deferred = Q.defer();
  var path = '/api/channels.list?token='+token;
  var options = {
    hostname: 'slack.com',
    port: 443,
    path: path,
    method: 'GET'
  };
  var req = https.request(options, function(response) {
    var data = [];
    response.on('data', function(chunk) {
      data.push(chunk);
    });
    response.on('end', function() {
      var result = JSON.parse(data.join(''));
      if(result.ok){
        result.channels.forEach(function(channel){
          if(channel.name === channelName){
            deferred.resolve(channel.id);
          }
        });
        deferred.reject('No channel with name '+channelName+' found.');
      } else {
        deferred.reject(result.error);
      }
    });
  });
  req.end();
  req.on('error', function(e) {
    deferred.reject(e);
  });
  return deferred.promise;
}

module.exports = {
  notify:notify,
  obtainChannel:obtainChannel
};
