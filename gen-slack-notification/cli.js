#!/usr/bin/env node

'use strict';
var meow = require('meow'),
    app = require('./app.js'),
    log = require('../logger/logger.js'),
    Q = require('q');

var cli = meow({
    help: [
        'Usage',
        '  node cli.js -k <token> -c <channel_id> -t <text> -u <username> -i <emoji>',
        '',
        'Example',
        '  node cli.js -k xoxp-18304797494-18304047649-23766831655-5027460bf3 -c cards-v1 -t "This is the test" -u Bitbucket -i :desktop_computer:',
        '',
    ]
});

var token = cli.flags.k,
    channel = cli.flags.c,
    text = cli.flags.t,
    user = cli.flags.u,
    emoji = cli.flags.i;

if (!token) {
    log.error('token is not specified');
    cli.showHelp();
    process.exit(1);
}
if (!channel) {
    log.error('channel is not specified');
    cli.showHelp();
    process.exit(1);
}
if (!text) {
    log.error('text is not specified');
    cli.showHelp();
    process.exit(1);
}
if (!user) {
    log.error('user is not specified');
    cli.showHelp();
    process.exit(1);
}
if (!emoji) {
    log.error('emoji is not specified');
    cli.showHelp();
    process.exit(1);
}

function notificateSlack(channelId){
  var deferred = Q.defer();
  app.notify(token,channelId,text,user,emoji)
  .then(function(result){
    deferred.resolve(result);
  })
  .catch(function(err){
    deferred.reject(err);
  });
  return deferred.promise;
}

app.obtainChannel(token, channel)
  .then(notificateSlack)
  .then(function(response){
    log.info('Message "'+text+ '" was sent to #'+channel+ ' channel');
  }).catch(function(err){
    log.error(err);
  });
