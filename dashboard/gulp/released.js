(function() {
    'use strict';

    const gulp = require('gulp'),
        electron = require('electron-packager'),
        packageJson = require(__dirname + '/../package.json'),
        logger = require('../../logger/logger');

    let options = {
        'dir': __dirname + '/../',
        'out': __dirname + '/../released',
        'overwrite': true,
        'platform': 'darwin',
        'arch': 'x64',
        'app-version': packageJson.version,
        'name': packageJson.name,
        'version': '1.1.0',
        'icon': __dirname + '/../icon.icns',
        'app-bundle-id': packageJson.name,
        'app-category-type': 'Developer Tools',
        'helper-bundle-id': packageJson.name,
        // 'prune': true,
        'asar': false,
        'ignore': [
            '/node_modules/electron-prebuilt($|/)',
            '/node_modules/electron-packager($|/)',
            '/.git($|/)',
            '/node_modules/\\.bin($|/)'
        ]
    };

    gulp.task('released', ['build'], () => {
        electron(options, (err) => {
            if (err) return logger.error(err);
        });
    });
})();
