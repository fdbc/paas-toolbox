(function () {
    'use strict';

    /** @ngInject */
    function navBar($location) {
        return {
            restrict: 'E',
            templateUrl: 'app/components/header/header-toolbox.html',
            link: function (scope, element) {
                var body = angular.element(document.querySelector('body'));

                scope.$on('$routeChangeSuccess', function () {
                    if (location.hash === '#/login') {
                        element.addClass('hide');
                        body.addClass('login');
                        body.removeAttr('layout', 'column');
                    } else {
                        element.removeClass('hide');
                        body.removeClass('login');
                        body.attr('layout', 'column');
                    }
                });
            }
        };
    }

    angular
        .module('dashboard')
        .directive('headerToolbox', navBar);
})();
