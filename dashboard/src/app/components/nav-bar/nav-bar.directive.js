(function () {
    'use strict';

    /** @ngInject */
    function navBar($location) {
        return {
            restrict: 'E',
            templateUrl: 'app/components/nav-bar/nav-bar.html',
            link: function (scope, element) {
                var buttons = document.getElementsByClassName('menu-button')

                function unlisten(element, event, handler) {
                    element.off(event, handler);
                    element = null;
                }

                function listen(element, event, handler) {
                    element.on(event, handler);
                    return function () {
                        unlisten(element, event, handler);
                    }
                }

                function addActiveButton(button) {
                    angular.element(button).addClass('active');
                }

                function removeActiveButton(button) {
                    angular.element(button).removeClass('active');
                }

                scope.goTo = function (route) {
                    $location.path(route);
                }

                scope.$on('$routeChangeSuccess', function () {
                    if (location.hash === '#/login') {
                        element.addClass('hide');
                    } else {
                        element.removeClass('hide');
                    }
                });

                scope.$watch(function () {
                    return location.hash;
                }, function (newHash) {
                    angular.forEach(buttons, function (button) {
                        if (newHash.search(button.id) >= 0) {
                            addActiveButton(button);
                        } else {
                            removeActiveButton(button);
                        }
                    });
                });
            }
        };
    }

    angular
        .module('dashboard')
        .directive('navBar', navBar);
})();
