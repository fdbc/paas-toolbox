(function () {
    'use strict';

    function routeConfig($routeProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: 'app/login/login.html',
                controller: 'LoginController',
                controllerAs: 'login'
            })
            .when('/develop', {
                templateUrl: 'app/develop/develop.html',
                controller: 'DevelopController',
                controllerAs: 'develop'
            })
            .when('/versioned', {
                templateUrl: 'app/versioned/versioned.html',
                controller: 'VersionedController',
                controllerAs: 'versioned'
            })
            .when('/documentation', {
                templateUrl: 'app/documentation/documentation.html',
                controller: 'DocumentationController',
                controllerAs: 'documentation'
            })
            .otherwise({
                redirectTo: '/login'
            });
    }

    angular
        .module('dashboard')
        .config(routeConfig);

})();
