(function() {
    'use strict';

    /** @ngInject */
    function runBlock() {
    }

    angular
        .module('dashboard')
        .run(runBlock);
})();
