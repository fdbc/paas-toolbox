(function () {
    'use strict';

    /** @ngInject */
    function LoginController(LoginService) {
        var vm = this;

        vm.login = function () {
            LoginService.getStructure(vm.user, vm.password)
                .then(function(data) {
                    localStorage.setItem('catalog', JSON.stringify(data['domain-groups']));
                    localStorage.setItem('pth', btoa(vm.user + ':' + vm.password));
                    location.hash = "#/develop";
                });
        }
    }

    angular
        .module('dashboard')
        .controller('LoginController', LoginController);
})();
