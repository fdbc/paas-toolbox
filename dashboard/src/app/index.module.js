(function() {
    'use strict';

    angular
        .module('dashboard', [
            'ngAnimate',
            'ngSanitize',
            'ngMessages',
            'ngAria',
            'ngRoute',
            'ngMaterial',
            'jsonFormatter'
        ]);
})();
