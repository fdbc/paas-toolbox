(function() {
    'use strict';

    /** @ngInject */
    function DocumentationController() {}

    angular
        .module('dashboard')
        .controller('DocumentationController', DocumentationController);
})();
