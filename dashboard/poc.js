(function() {
    'use strict';
    const exec = require('child_process').exec;
    exec('gulp serve --gulpfile ./../gen-site/gulpfile.js', (err, stdout) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log(stdout);
    });
})();
