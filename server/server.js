const express = require('express');
const app = express();
const request = require('request');
const bodyParser = require('body-parser');
const credentials = require('./credentials');
const fs = require('fs');
const google = require('googleapis');
const script = google.script('v1');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

let credentials_auth = "";
let setProxy = false;
let proxyData = "";

fs.readFile('../.proxy', 'utf8', function (err,data) {
  if (!err) {
    proxyData = data;
    setProxy = true;
  }
});


let authorization = "Basic Ym90LWNhdGFsb2ctcmVhZDpHZHRabkVMWA==";
const host = 'https://globaldevtools.bbva.com';


app.all('/*', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "localhost");
  res.header("Access-Control-Allow-Methods", "GET");
  res.header("Access-Control-Allow-Headers", "*");

  next();
});

function proxyService(options, callback){
    request(options, (err, response, body) => {
        var res = {};
          if (!err) {
            res.body = body;
            res.status = response.statusCode;
          }else{
            res.body = err;
            res.status = 404;
          }
          return callback(res);
      });
}

app.get('/bitbucket/*', (req, res) => {
  req.body.url = host + req.url;
  req.body.headers = { "Authorization" : ""};
  req.body.headers.Authorization = authorization;
  if (setProxy) req.body.proxy = proxyData;
  proxyService(req.body, (response) => {
        res.status(response.status).send(response.body);
  })

});

app.post('/script.google.com', (req, res) => {
    script.scripts.run({
      resource: {
        function: 'doGet',
        parameters: req.body
      },
      scriptId: credentials_auth.script_id
    }, function (err, rescript) {
      if (err){
        res.status(404).send(err);
      }else {
        res.status(200).send(rescript.response.result);
      }
    });
});


app.listen(3003, () => {
  console.log('Server listening on port 3003');

  credentials.getCredentials((content) => {credentials_auth = content});

});
