const google = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const fs = require('fs');
const googleAuth = require('google-auth-library');

const CREDENTIALS = '.credentials/.credentials';

const credentials = {
  getCredentials(callback){
    loadCredentials((content) => callback(content));
  }
}

function loadCredentials(callback){
  fs.readFile(CREDENTIALS, (err, data) => {
    if (err) {
      console.log('Error loading credentials file: ' + err);
      return;
    }else{
      var content = JSON.parse(data);
      var clientSecret = content.client.client_secret;
      var clientId = content.client.client_id;
      var redirectUrl = content.client.redirect_uris[0];
      var script_id = content.script_id;
      var auth = new googleAuth();
      var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
      oauth2Client.credentials = content.token;

      google.options({ auth: oauth2Client });
      var response = {
        oauth2Client,
        script_id
      }

      callback(response);
    }

  });
}

module.exports = credentials;
