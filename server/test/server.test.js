var expect    = require("chai").expect;
var server = require("../server");
var request = require("request");

let host = "http://localhost:3002";

describe('Server endpoints test', () => {
  describe('get home method', () => {

    let url = host + "/";
    it('should return 200', (done) => {
      var options = {
        url: url,
        headers: {
          'Content-Type': 'application/json;charset=UTF-8'
        }
      };
      request.get(options, (err, res, body) => {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.equal('Use endpoint POST /bbproxy');
        done();
      });
    });

});
});
