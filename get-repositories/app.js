'use strict';

var fs = require('fs-extra'),
    exec = require('child_process').exec,
    log = require('../logger/logger.js'),
    Q = require('q');
var merge = require('../utils/deepmerge.js');


function downloadRepo(repository, outputFolder, branch){
  var deferred = Q.defer();
  var command = "git clone " + repository.replace('/bitbucket/projects','ssh://git@globaldevtools.bbva.com:7999').replace('/repos','') + '.git',
      commandBranch = command;
  if(branch) commandBranch += ' -b ' + branch;
    exec(commandBranch, { cwd: outputFolder }, function (error, stdout, stderr){
    if (error == null) return deferred.resolve(outputFolder + "/" + repository.split(/[/]/).pop().split('.')[0]);
    log.warning(branch + ' branch doesn\' exist in '+ repository+ '. Downloading master branch...');
    exec(command, { cwd: outputFolder }, function (error, stdout, stderr){
      if (error == null) return deferred.resolve(outputFolder + "/" + repository.split(/[/]/).pop().split('.')[0]);
      return deferred.reject(error);
    });
  });
  return deferred.promise;
}

function isTagged(domain){
  const isTagged = domain["repository-url"] && domain.tag.length > 0
  return isTagged
}

function isCommonsRepo(domain){
  const isCommonsRepo = domain["repository-url"] && domain["repository-url"].search('commons') >= 0
  return isCommonsRepo
}

function isRequestedRepo(domain, repo_name){
  const isRequestedRepo = !generateAll(repo_name) && domain["repository-url"] && domain["repository-url"].search(repo_name) !== -1
  return isRequestedRepo
}

function generateAll(repo_name){
  const generateAll = repo_name === ""
  return generateAll
}
function createRepos(outputFolder, branch, repo_name=""){
  var deferred = Q.defer();
  var promises = [];
  fs.emptyDir(outputFolder, function (err) {
    let json = getExtendedJson()
    json["domain-groups"].forEach(function(domaingroup) {
      domaingroup.sections.forEach(function(section) {
        section.domains.forEach(function(domain) {
          if((isTagged(domain) && generateAll(repo_name)) || isCommonsRepo(domain) || isRequestedRepo(domain, repo_name)){
            var promise = downloadRepo(domain["repository-url"], outputFolder, branch);
            promise.then(function(path){
              log.info('Repository created '+ path);
            }).catch(function(error){
              log.error('Error downloading repository:', error);
            });
            promises.push(promise);
          } else if (domain.tag.length === 0) {
            log.warning('Domain ' + domain['display-name'] + ' won\'t download because repository ' + domain["repository-url"] + ' is empty');
          } else {
            log.warning('Domain ' + domain['display-name'] + ' won\'t download because repository ' + domain["repository-url"] + ' doesn\'t exist');
          }
        });
      });
    });
    Q.all(promises).then(function(data){
      return deferred.resolve(data);
    });
  });
  return deferred.promise;
}

function getExtendedJson(structureBaseJsonPath, structureJsonPath, encoding){
  structureBaseJsonPath = structureBaseJsonPath ? structureBaseJsonPath : '../gen-site/src/structure.json'
  structureJsonPath = structureJsonPath ? structureJsonPath : '../gen-site/src/structure_base.json'
  encoding = encoding ? encoding : 'utf8'
  var json = JSON.parse(fs.readFileSync(structureBaseJsonPath, encoding));
  var baseJson = JSON.parse(fs.readFileSync(structureJsonPath, encoding));
  return merge(json, baseJson);
}

module.exports = {
  getExtendedJson:getExtendedJson,
  createRepos:createRepos
};
