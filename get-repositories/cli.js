#!/usr/bin/env node

'use strict';
var meow = require('meow'),
    log = require('../logger/logger.js'),
    app = require('./app.js');

const fs = require('fs');

var cli = meow({
    help: [
        'Usage',
        '  node cli.js -o <outputPath> -b <branch>',
        '  node cli.js -g',
        '      Generate extended structure.json in current path',
        '',
        'Example',
        '  node cli.js -o "../repositories"',
        '  node cli.js -o "../repositories" -b develop',
        '',
    ]
});

var path = cli.flags.o,
    branch = cli.flags.b;

let structurePath = cli.flags.structure
let structureBasePath = cli.flags.base
let generateJson = cli.flags.g

if (!generateJson) {
  if (!path) {
      log.error('path is not specified');
      cli.showHelp();
      process.exit(1);
  }

  if (!branch) {
      log.error('branch is not specified');
      cli.showHelp();
      process.exit(1);
  }

  app.createRepos(path, branch).then(function(done){
    log.info(done);
  }).catch(function(err){
    log.info(err);
  });
} else {
  fs.writeFile('structure.json', JSON.stringify(app.getExtendedJson()), (err) => {
    if (err) throw err;
    console.log('structure.json has been saved!');
  });
}
