#!/usr/bin/env node
'use strict';

var Q = require('q'),
    fs = require('fs-extra'),
    path = require('path'),
    exec = require('child_process').exec,
    validate = require('../linter-raml-myrules/lib/raml-validator.js'),
    log = require('../logger/logger.js'),
    genChangelog = require('../gen-changelog/app.js'),
    raml2view = require('../gen-raml2view/app.js');


function commiter(config) {
    function resolvePromiseFunction(promise) {
        return function (error, stdout, stderr) {
            if (error) log.error(error);
            if (stderr) log.warning(stderr);
            if (stdout) log.info(stdout);
            if (error === null) return promise.resolve();
            else promise.reject();
        };
    }
    function deferExec(execCommand) {
        var deferred = Q.defer();
        log.info('Executing: ' + execCommand);
        exec(execCommand, {cwd: config.path}, resolvePromiseFunction(deferred));
        return deferred.promise;
    }
    function validateAPIRAMLExist() {
        var defer = Q.defer();
        fs.exists(config.path+ '/api.raml', function (exist) {
            if (!exist) {
                log.error('Path to API repository doesn\'t exist.');
                defer.reject();
            } else {
                defer.resolve();
            }
        });
        return defer.promise;
    }
    function changeRAMLVersion() {
        var defer = Q.defer();
        var regex = /([vV]ersion: v?)(\d\.?\d*\.?\d*)/g;
        fs.readFile(config.path+ '/api.raml', 'utf8', function(err, data){
            if(err){
                log.error('Path to API repository doesn\'t exist.');
                defer.reject();
            }
            var result = data.replace(regex, function(match, g1){
                return g1+config.version;
            });
            fs.writeFile(config.path+ '/api.raml', result, 'utf8', function (err) {
                if (err) {
                    log.error('Error modifying RAML version');
                    defer.reject();
                }
                defer.resolve();
            });
        });
        return defer.promise;
    }
    function validateTag(){
      var defer = Q.defer();
      log.info('Validating Tag...');
      exec("git tag --contains " +config.version , {cwd: config.path }, function(error, stdout, stderr) {
        if(!error){
            log.error("Version "+ config.version + " already exist.");
            defer.reject();
        }else{
            defer.resolve();
        }
      });
      return defer.promise;
    }
    function checkoutDevelop() {
        return deferExec('git checkout develop');
    }
    function validateRAML(){
        log.info('Validating RAML...');
        var deferred = Q.defer();
        var validator = validate(config.path + '/api.raml');
        validator.parseApi()
            .then(
                function(){
                    validator.apiErrors.bind(validator);
                    return validator.apiErrors();
                })
            .then(
                function(){
                    deferred.resolve();
                },
                function(errors){
                    errors.forEach(function(x){
                        log.error(JSON.stringify({
                            code: x.code,
                            message: x.message,
                            path: x.path,
                            start: x.start,
                            end: x.end,
                            isWarning: x.isWarning
                        },null,2));
                    });
                    deferred.reject();
                });
        return deferred.promise;
    }
    function generateChangelog() {
        log.info('Generating changelog...');
        return genChangelog(config).then(function () {
            return Q.when();
        });
    }
    function generateDocumentation() {
        log.info('Generating documentation...');
        return raml2view(config.path);
    }
    function addChangelog() {
        return deferExec('git add .');
    }
    function commitChangelog() {
        return deferExec('git commit -m "docs(changelog): update CHANGELOG.md for version ' + config.version + '"');
    }
    function checkoutMaster() {
        return deferExec('git checkout master');
    }
    function mergeWithMaster() {
        return deferExec('git merge develop');
    }
    function addTag() {
        return deferExec('git tag -a ' + config.version + ' -m "version ' + config.version + '"');
    }
    function deferConfig(){
      var deferred = Q.defer();
      deferred.resolve(config);
      return deferred.promise;
    }

    return validateAPIRAMLExist()
        .then(validateTag)
        //.then(checkoutDevelop)
        .then(changeRAMLVersion)
        //.then(validateRAML)
        .then(generateChangelog)
        .then(generateDocumentation)
        .then(addChangelog)
        .then(commitChangelog)
        //.then(checkoutMaster)
        //.then(mergeWithMaster)
        .then(addTag)
        .then(deferConfig)
        .catch(function(err){
          log.error('There was an error in commit proccess.');
          if(err) log.error(err);
        });
}

module.exports = commiter;
