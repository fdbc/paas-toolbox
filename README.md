# Paas Toolbox

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

* Contribution guidelines: [CONTRIBUTING.md](../browse/CONTRIBUTING.md)

## Repo structure

* bbva-rules
* bump-version -> API versioning
* bump-version-commit -> tagging + raml validation + requires changelog editting
* bump-version-push -> push tag to master
* dashboard
* gen-api-catalog -> gets all repositories + generates structure.json (APIS structure for [https://apisbbva.bitbucket.io/#/api])
* gen-blog
* gen-changelog --> changelog generation
* gen-raml2view -> generates API html
* gen-site -> Server. `gulp serve` deploys local portal and ... deploys at [https://bitbucket.org/apisbbva/apisbbva.bitbucket.org]
* gen-slack-notification
* gen-types-from-md
* get-repositories -> clones all API repos in a local directory
* linter-raml-myrules
* logger


## First steps

Install all dependencies and needed libraries

```
$ npm install
```

Run API portal Server

```
$ npm run serve
```

### Generates all APIs html from bitbucket

```
$ npm run snapshot
```

### Generates all APIs html from branch
* -b : branch name
```
$ npm run snapshot -- -b master
```

### Generates one API from branch
*  -r : api slug
*  -b : branch name
```
$ npm run snapshot -- -r glapi-global-apis-products-accounts -b master
```

### Generates one API from local source
*  -r : api slug
*  -p : local path
```
$ npm run snapshot -- -r glapi-global-apis-products-accounts -p ../../glapi-global-apis-products-accounts
```

### Updates structure.json from bitbucket api repos (merge)
```
$ npm run gen-structure
```

More options available at [gen-raml2view/README.md](gen-raml2view/README.md)
