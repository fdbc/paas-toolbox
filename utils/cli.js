const assert = require('assert')
const fs = require('fs-extra')

const merge = require('./deepmerge.js')

const encoding = 'utf8'

const structure_file = './test/addAPI/structure.json'
const base_structure_file = './test/addAPI/structure_base.json'


const structure = JSON.parse(fs.readFileSync(structure_file, encoding));
const base_structure = JSON.parse(fs.readFileSync(base_structure_file, encoding));


const mergedStructure = merge(base_structure, structure);

fs.outputFileSync('./test/addAPI/generated_structure_result.json', JSON.stringify(mergedStructure, null, 4));
