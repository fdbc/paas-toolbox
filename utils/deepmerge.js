var isMergeableObject = require('./is-mergeable-object.js')

function emptyTarget(val) {
    return Array.isArray(val) ? [] : {}
}

function cloneIfNecessary(value, optionsArgument) {
    var clone = optionsArgument && optionsArgument.clone === true
    return (clone && isMergeableObject(value)) ? deepmerge(emptyTarget(value), value, optionsArgument) : value
}

function defaultArrayMerge(target, source, optionsArgument) {
    var destination = target.slice()
    source.forEach(function(e, i) {
        var index = existObjectInArray(e, target) !== -1 ? existObjectInArray(e, target) : i
        if (typeof destination[index] === 'undefined') {
            destination[index] = cloneIfNecessary(e, optionsArgument)
        } else if (existObjectInArray(e, target) === -1) {
            destination.push(cloneIfNecessary(e, optionsArgument))
        } else if (isMergeableObject(e)) {
            destination[index] = deepmerge(target[index], e, optionsArgument)
        } else if (target.indexOf(e) === -1) {
            destination.push(cloneIfNecessary(e, optionsArgument))
        }
    })
    return destination
}

function existObjectInArray(object, array){
  const propertiesLength = Object.keys(object).length
  for(element in array){
    var numberOfSameProperties = 0
    for(key in Object.keys(object)){
      if (array[element].hasOwnProperty(Object.keys(object)[key]) && object[Object.keys(object)[key]] === array[element][Object.keys(object)[key]]) {
        numberOfSameProperties = numberOfSameProperties + 1;
        if (numberOfSameProperties >= propertiesLength - 1) {
          return element
        }
      }
    }
  }

  return -1;
}

function isSameObject(){}

function mergeObject(target, source, optionsArgument) {
    var destination = {}
    if (isMergeableObject(target)) {
        Object.keys(target).forEach(function(key) {
            destination[key] = cloneIfNecessary(target[key], optionsArgument)
        })
    }
    Object.keys(source).forEach(function(key) {
        if (!isMergeableObject(source[key]) || !target[key]) {
            destination[key] = cloneIfNecessary(source[key], optionsArgument)
        } else {
            destination[key] = deepmerge(target[key], source[key], optionsArgument)
        }
    })
    return destination
}

function deepmerge(target, source, optionsArgument) {
    var sourceIsArray = Array.isArray(source)
    var targetIsArray = Array.isArray(target)
    var options = optionsArgument || { arrayMerge: defaultArrayMerge }
    var sourceAndTargetTypesMatch = sourceIsArray === targetIsArray

    if (!sourceAndTargetTypesMatch) {
        return cloneIfNecessary(source, optionsArgument)
    } else if (sourceIsArray) {
        var arrayMerge = options.arrayMerge || defaultArrayMerge
        return arrayMerge(target, source, optionsArgument)
    } else {
        return mergeObject(target, source, optionsArgument)
    }
}

deepmerge.all = function deepmergeAll(array, optionsArgument) {
    if (!Array.isArray(array) || array.length < 2) {
        throw new Error('first argument should be an array with at least two elements')
    }

    // we are sure there are at least 2 values, so it is safe to have no initial value
    return array.reduce(function(prev, next) {
        return deepmerge(prev, next, optionsArgument)
    })
}

module.exports = deepmerge
