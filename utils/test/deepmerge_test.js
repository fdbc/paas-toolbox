const assert = require('assert')
const fs = require('fs-extra')

const merge = require('../deepmerge.js')

const encoding = 'utf8'

describe('Structure deep merge', function() {
  describe('#same structure', function() {
    it('should return same structure', function() {
      const structure_file = './test/sameStructure/structure.json'

      const structure = JSON.parse(fs.readFileSync(structure_file, encoding));

      const mergedStructure = merge(structure, structure);

      assert.deepEqual(structure, mergedStructure);
    });
  });

  describe('#different structure', function() {
    it('should return expected structure with items from both structures', function() {
      const structure_file = './test/differentStructure/structure.json'
      const base_structure_file = './test/differentStructure/structure_base.json'
      const expected_structure_file = './test/differentStructure/structure_expected.json'

      const structure = JSON.parse(fs.readFileSync(structure_file, encoding));
      const base_structure = JSON.parse(fs.readFileSync(base_structure_file, encoding));
      const expected_structure = JSON.parse(fs.readFileSync(expected_structure_file, encoding));

      const mergedStructure = merge(base_structure, structure);

      fs.outputFileSync('./test/differentStructure/generated_structure_result.json', JSON.stringify(mergedStructure, null, 4));

      assert.deepEqual(expected_structure, mergedStructure);

    });
  });

  describe('#combined structure', function() {
    it('should return expected structure with combined items from both structures', function() {
      const structure_file = './test/combinedStructure/structure.json'
      const base_structure_file = './test/combinedStructure/structure_base.json'
      const expected_structure_file = './test/combinedStructure/structure_expected.json'

      const structure = JSON.parse(fs.readFileSync(structure_file, encoding));
      const base_structure = JSON.parse(fs.readFileSync(base_structure_file, encoding));
      const expected_structure = JSON.parse(fs.readFileSync(expected_structure_file, encoding));

      const mergedStructure = merge(base_structure, structure);

      fs.outputFileSync('./test/combinedStructure/generated_structure_result.json', JSON.stringify(mergedStructure, null, 4));

      assert.deepEqual(expected_structure, mergedStructure);

    });
  });

  describe('#different section', function() {
    it('should return expected structure with items from structures sections', function() {
      const structure_file = './test/differentSection/structure.json'
      const base_structure_file = './test/differentSection/structure_base.json'
      const expected_structure_file = './test/differentSection/structure_expected.json'

      const structure = JSON.parse(fs.readFileSync(structure_file, encoding));
      const base_structure = JSON.parse(fs.readFileSync(base_structure_file, encoding));
      const expected_structure = JSON.parse(fs.readFileSync(expected_structure_file, encoding));

      const mergedStructure = merge(base_structure, structure);

      fs.outputFileSync('./test/differentSection/generated_structure_result.json', JSON.stringify(mergedStructure, null, 4));

      assert.deepEqual(expected_structure, mergedStructure);

    });
  });

  describe('#combined section', function() {
    it('should return expected structure with combined items from sections', function() {
      const structure_file = './test/combineSection/structure.json'
      const base_structure_file = './test/combineSection/structure_base.json'
      const expected_structure_file = './test/combineSection/structure_expected.json'

      const structure = JSON.parse(fs.readFileSync(structure_file, encoding));
      const base_structure = JSON.parse(fs.readFileSync(base_structure_file, encoding));
      const expected_structure = JSON.parse(fs.readFileSync(expected_structure_file, encoding));

      const mergedStructure = merge(base_structure, structure);

      fs.outputFileSync('./test/combineSection/generated_structure_result.json', JSON.stringify(mergedStructure, null, 4));

      assert.deepEqual(expected_structure, mergedStructure);

    });
  });

  describe('#new API structure', function() {
    it('should return expected structure with new items from new API structure', function() {
      const structure_file = './test/addAPI/structure.json'
      const base_structure_file = './test/addAPI/structure_base.json'
      const expected_structure_file = './test/addAPI/structure_expected.json'

      const structure = JSON.parse(fs.readFileSync(structure_file, encoding));
      const base_structure = JSON.parse(fs.readFileSync(base_structure_file, encoding));
      const expected_structure = JSON.parse(fs.readFileSync(expected_structure_file, encoding));

      const mergedStructure = merge(base_structure, structure);

      fs.outputFileSync('./test/addAPI/generated_structure_result.json', JSON.stringify(mergedStructure, null, 4));

      assert.deepEqual(expected_structure, mergedStructure);

    });
  });
});
